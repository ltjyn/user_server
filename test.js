/*var http = require('http');

http.createServer(function (request, response) {
	response.writeHead(200, {'Content-Type': 'text/plain'});
	response.end('Hello World\n');
}).listen(8888);
console.log(typeof('aaa'));
console.log('Server running at http://127.0.0.1:8888/');*/

//  http://192.168.0.28:5556/login?m=login&token=c10630a9974cd1ad2d51a766321b949f&openid=oc43Pv-xQXH44xpURmjMfmnrmBQ8&headimgurl=http://wx.qlogo.cn/mmopen/9ibuJeOEOtKTgyynnoBNAOEhXRH18epUz3LzKOFGauLqc08gLk2a96Rmh6ASt4A0b2Rqib3b0FmNIb5ZPKjsy2JVpnZQeKKYhQ/0&nickname=auth

'use strict';

var api = require('./src/molten/yg_api.js'),
	querystring = require('querystring'),
	url = require('url');

// 初始化配置信息
api.init({
	'apiKey' : 'IL7SnndiPhUT9YeOAKshJDQBeZO6yC8U', // !!!!在商户平台中配置的游戏私钥, 这里只是个示例
	'gameId' : 133 // !!!!!!在商户平台中创建的游戏ID，这里只是个示例
});

var http = require('http');

var server = http.createServer(function (req, res) {

	var urlParams = url.parse(req.url);
	console.log('url', req.url);
	var requestArgs = querystring.parse(urlParams.query);
	console.log(requestArgs);

	// 校验订单并发货
	api.verifyDelivery(requestArgs, function (err) {
		if (err) {
			// 返回客户端表示发货失败
			console.log(err);
			res.end(err.Error);
		} else {
			// 检验订单是否重复，由于网络原因发货平台可能会重复通知
			// 发货相关逻辑 : 根据商户平台配置不同的产品id(productId), 发送不同的钻石

			// 发货成功后返回客户端表示发货成功
			res.end('SUCCESS');
		}
	});
}).listen(3000);