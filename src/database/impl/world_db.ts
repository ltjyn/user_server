import MysqlConnection = require('../mysql_connection');
import async = require('async');
import mysql = require('mysql');
import log = require('../../util/log');

export var TABLE_SPLIT_NUM = 10;
export var conn:MysqlConnection;

export var DIAMOND_LOG_TABLE_NAME = 'diamond_log';
export var PAYMENT_TABLE_NAME = 'payment';

export function init(config:any, callback:(err) => void):void {
    conn = new MysqlConnection();
    conn.startDb({
        name: 'world',
        connectionLimit: config.connectionLimit,
        host: config.hostname,
        user: config.user,
        password: config.password,
        database: config.database,
        charset: 'UTF8_GENERAL_CI',
        supportBigNumbers: true
    });

    var tables:{[tableName:string]:string} = {};
    var columns = [];
    var indexes = [];

    // 玩家分表
    var tableName = '';
    for (var i = 0; i < TABLE_SPLIT_NUM; i++) {
        tableName = 'player_info_' + i;
        tables[tableName] =
            "CREATE TABLE IF NOT EXISTS " + tableName + " (" +
            "uid 	        BIGINT(20) 	UNSIGNED 	NOT NULL," +
            "nickname 		VARCHAR(50) CHARACTER SET utf8 NOT NULL DEFAULT ''," +
            "headimgurl 	VARCHAR(200) CHARACTER SET utf8 NOT NULL DEFAULT ''," +
            "lastLoginTime 	INT      	UNSIGNED	NOT NULL	DEFAULT '0'," +
            "lastAliveTime 	INT      	UNSIGNED	NOT NULL	DEFAULT '0'," +
            "createTime	    INT 		UNSIGNED 	NOT NULL 	DEFAULT '0'," +
            "gold 			INT 	    UNSIGNED	NOT NULL	DEFAULT '0'," +
            "yxb 		    DOUBLE 		UNSIGNED	NOT NULL    DEFAULT '0'," +
            "totalYxb       DOUBLE      UNSIGNED    NOT NULL    DEFAULT '0'," +
            "totalRmbPay    INT         UNSIGNED    NOT NULL    DEFAULT '0'," +
            "rmbPay         INT         UNSIGNED    NOT NULL    DEFAULT '0'," +
            "isFirstPay     INT         UNSIGNED    NOT NULL    DEFAULT '0'," +
            "dayShare       INT         UNSIGNED    NOT NULL    DEFAULT '0'," +
            "shareAmount    INT         UNSIGNED    NOT NULL    DEFAULT '0'," +
            "lastShareTime  INT         UNSIGNED    NOT NULL    DEFAULT '0'," +
            "isAttention    INT         UNSIGNED    NOT NULL    DEFAULT '0'," +
            "isAttentionAward    INT         UNSIGNED    NOT NULL    DEFAULT '0'," +
            "channel        INT         UNSIGNED    NOT NULL    DEFAULT '0'," +
            "PRIMARY KEY (uid)" +
            ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        columns = columns.concat([
            [tableName, 'heroLv', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'weaponList', "VARCHAR(1024) CHARACTER SET utf8 NOT NULL DEFAULT ''"],
            [tableName, 'soldierList', "VARCHAR(1024) CHARACTER SET utf8 NOT NULL DEFAULT ''"],
            [tableName, 'skillList', "VARCHAR(1024) CHARACTER SET utf8 NOT NULL DEFAULT ''"],
            [tableName, 'achiAwardBit', "VARCHAR(1024) CHARACTER SET utf8 NOT NULL DEFAULT ''"],
            [tableName, 'killEnemy', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'killBox', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'soldierDie', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'heroBorn', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'weaponUp', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'useSkill', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'callPeri', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'buyYxb',   "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'lastItemDropTime', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'dayDrop', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'drawEffectCount', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'drawEffectTime', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'cdkeyAward', "VARCHAR(128) CHARACTER SET utf8 NOT NULL DEFAULT ''"],
            [tableName, 'awardMonthItemTime', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'buyMonthItemTime', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'highestfloorclear', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'sealFloor', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'maxFloor', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'giftDrop', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'lastGiftDropTime', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            //[tableName, 'isGetFirstPay', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'unlockCount', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'useDemonClickTime', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'cheatNum', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'lastCheatTime', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'floorCheatNum', "INT UNSIGNED NOT NULL DEFAULT '0'"],
            [tableName, 'floorLastCheatTime', "INT UNSIGNED NOT NULL DEFAULT '0'"]
            // add new update column here
        ]);

        indexes = indexes.concat([]);
    }

    tableName = DIAMOND_LOG_TABLE_NAME;
    tables[tableName] =
        "CREATE TABLE IF NOT EXISTS " + tableName + " (" +
        "id             INT(10)         UNSIGNED    NOT NULL AUTO_INCREMENT,"+
        "uid 	        BIGINT(20) 	    UNSIGNED 	NOT NULL," +
        "transId 		VARCHAR(64) CHARACTER SET utf8 NOT NULL DEFAULT ''," +
        "addGold     	INT             UNSIGNED    NOT NULL    DEFAULT '0'," +
        "time 	        INT      	    UNSIGNED	NOT NULL    DEFAULT '0'," +
        "reason         INT             UNSIGNED    NOT NULL    DEFAULT '0'," +
        "PRIMARY KEY (id)," +
        "KEY idx_time (uid)," +
        "KEY idx_trans_id (transId)"+
        ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

    //tableName = PAYMENT_TABLE_NAME;
    //tables[tableName] =
    //    "CREATE TABLE IF NOT EXISTS " + tableName + " (" +
    //    "id             INT(10)         UNSIGNED    NOT NULL AUTO_INCREMENT,"+
    //    "uid 	        BIGINT(20) 	    UNSIGNED 	NOT NULL," +
    //    "tradeNO 		VARCHAR(64)     CHARACTER SET utf8  NOT NULL  DEFAULT ''," +
    //    "pid     	    INT             UNSIGNED    NOT NULL    DEFAULT '0'," +
    //    "gameId 	    INT      	    UNSIGNED	NOT NULL    DEFAULT '0'," +
    //    "status         INT             UNSIGNED    NOT NULL    DEFAULT '0'," +
    //    "PRIMARY KEY (id)," +
    //    ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

    async.waterfall([
            (next) => {
                conn.createTables(tables, (err, info) => {
                    if (err) {
                        log.sError('check ' + info + ' failed');
                    }
                    next(err);
                });
            },
            (next) => {
                conn.addColumns(columns, (err, info) => {
                    if (err) {
                        log.sError('check ' + info + ' failed');
                    }
                    next(err);
                });
            },
            (next) => {
                conn.addIndexes(indexes, (err, info) => {
                    if (err) {
                        log.sError('check ' + info + ' failed');
                    }
                    next(err);
                });
            }
        ], (err) => {
            if (err) {
                callback(err);
                return;
            }
            log.sInfo('checked all tables, columns, indexes');
            callback(null);
        }
    );

    setInterval(() => {
        conn.execute('select 1', null, () => {
            log.sInfo('keep mysql alive');
        });
    }, 1000 * 3600);
}

export function getDBTime(callback:(err, dbTime:number) => void):void {
    var sql = 'select unix_timestamp() as dbTime';
    conn.execute(sql, null, (err, connection, result) => {
        if (err) {
            callback(err, 0);
            return;
        }

        callback(null, result[0].dbTime);
    });
}

export function shutDownDB(callback:(err) => void):void {
    conn.closeDb((err) => {
        callback(err);
    });
}