//import WorldDB = require('../database/impl/world_db');
import LoginDb = require('../database/impl/login_db');
import RedisMgr = require('../redis/redis_mgr');

class SDK {
    public static insert(currtime,headimgurl, nickname, openid, codeToken, cb:(uid)=>void) {
        if(headimgurl === null || typeof headimgurl === 'undefined'){
            headimgurl = '';
        }
        if(nickname === null || typeof nickname === 'undefined'){
            nickname = '';
        }
        LoginDb.conn.execute('insert into player set ?', {
            lastGetHeadimgurlTime:currtime,
            headimgurl: headimgurl,
            nickname: nickname,
            openid: openid,
            token: codeToken,
            channel:'vu'
        }, (err, conn,result)=> {
            if (err) {
                throw err;
            }

            var uid = result.insertId;
            RedisMgr.getInstance('accountTokenRedis').hmset(codeToken, {
                uid: uid,
                headimgurl: headimgurl,
                nickname: nickname,
                openid: openid,
                token: codeToken
            }, 86400, ()=> {
                cb(uid);
            });
        });
    }

    public static select(fields,tablename,condition,cb:(repalyJson)=>void){
        LoginDb.conn.execute('select '+ fields +' from '+ tablename +' where ?', condition, function (err, connection, result) {
            if (err) {
                throw err;
            }
            if (!result || result.length === 0) {
                cb(result);
            }
            else cb(result[0]);
        });
    }

    public static update(data,condition,cb:(repalyJson)=>void){
        LoginDb.conn.execute('update player set token=?,headimgurl=?,nickname=? where ?',[data.token,data.headimgurl,data.nickname,condition], (err, connection, result)=> {
            if (err)  return;
            if (!result || result.length === 0) {
                cb(result);
            }
            cb(result);
        });
    }

    public static genSign(data, gameid) {
        delete data['sign']; // 除去sign参数
        var content = this.strBuff(data);
        var crypto = require("crypto");
        var key = crypto.createHash("md5").update('as8df@#s2!%*&%'+gameid).digest("hex").toLowerCase();
        // console.log('string', key);
        content += ("&appkey=" + key);
        console.log('string', content);
        return crypto.createHash("md5").update(content).digest("hex").toUpperCase();
    }

    public static strBuff(data){
        var list = [];

        for (var key in data) {
            list.push(key);
        }
        list.sort();

        var content = "";
        for (var i in list) {
            var key = list[i];
            var value = data[key];
            if (value) {
                content += ((i == 0 ? "" : "&") + key + "=" + value);
            } else {
                content += ((i == 0 ? "" : "&") + key + "=");
            }
        }
        return content;
    }
}

export  = SDK;