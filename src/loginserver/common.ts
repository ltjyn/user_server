/*op sql */
import LoginDb = require('../database/impl/login_db');
import RedisMgr = require('../redis/redis_mgr');
import Time = require('../util/time');
import request = require("request");
import log = require('../util/log');
var crypto = require("crypto");

class COMMON {
    public static select(fields,tablename,condition,cb:(repalyJson)=>void){
        LoginDb.conn.execute('select '+ fields +' from '+ tablename +' where ?', condition, function (err, connection, result) {
            if (err) {
                throw err;
            }
            if (!result || result.length === 0) {
                cb(result);
            }else{
                cb(result[0]);
            }
        });
    }

    public static dbin(obj, cb:(uid)=>void) {
        var now = Time.realNow();
        LoginDb.conn.execute('insert into player set ?', {
            lastLogin:now,
            lastGetHeadimgurlTime:now,
            headimgurl: obj.headimgurl,
            nickname: obj.nickname,
            openid: obj.openid,
            token: obj.token,
            channel:obj.channel
        }, (err, conn,result)=> {
            if (err) {
                throw err;
            }

            var uid = result.insertId;
            RedisMgr.getInstance('accountTokenRedis').hmset(obj.token, {
                uid: uid,
                headimgurl: obj.headimgurl,
                nickname: obj.nickname,
                openid: obj.openid,
                token: obj.token
            }, 86400, ()=> {
                cb(uid);
            });
        });
    }

    public static update(data,condition,cb:(repalyJson)=>void){
        LoginDb.conn.execute('update player set token=?,headimgurl=?,nickname=?,lastLogin=? where ?',
            [data.token,data.headimgurl,data.nickname,data.lastLogin,condition], (err, connection, result)=> {
                if (err)  return;
                if (!result || result.length === 0) {
                    cb(result);
                }
                cb(result);
            });
    }

    public static noticeNode(verSignData,svrId,cb:(state)=>void){
        var mySign = this.genSign(verSignData, verSignData.gameid);
        var content = this.strBuff(verSignData);
        LoginDb.conn.execute('select connectIp from web_data where ?', {id: verSignData.gameid}, function (err, connection, result) {
            if (err)  throw err;
            if (!result || result.length === 0) {

            }else{
                var payUrl = 'http://' + result[0].connectIp + '/pay?m=index&' + content + '&svr_id=' + svrId + '&sign=' + mySign;
                log.sInfo('pay Url = ' + payUrl);
                request(payUrl,function (error, response, paybody) {
                    var res = {result:1};
                    if (!error && response.statusCode == 200) {
                        var paydata = JSON.parse(paybody);
                        if(paydata == 'SUCCESS') {
                            LoginDb.conn.execute('update wxpay set status=2 where trade_no="' + verSignData.trade_no + '"', {}, function (err, connection, result) {
                                if (err) throw err;
                                if(result.affectedRows){
                                    res.result =0 ;
                                    cb(res);
                                }else{
                                    cb(res);
                                }
                            });
                        }else {
                            cb(res);
                        }
                    }else {
                        cb(res);
                    }
                });
            }
        });
    }

    public static genSign(data, gameid) {
        delete data['sign']; // 除去sign参数
        var content = this.strBuff(data);
        var key = crypto.createHash("md5").update('as8df@#s2!%*&%'+gameid).digest("hex").toLowerCase();
        content += ("&appkey=" + key);
        // console.log('string', content);
        return crypto.createHash("md5").update(content).digest("hex").toUpperCase();
    }

    public static strBuff(data){
        var list = [];

        for (var key in data) {
            list.push(key);
        }
        list.sort();

        var content = "";
        for (var i in list) {
            var key = list[i];
            var value = data[key];
            if (value) {
                content += ((i == 0 ? "" : "&") + key + "=" + value);
            } else {
                content += ((i == 0 ? "" : "&") + key + "=");
            }
        }
        return content;
    }

    public static makeSig(method, url_path, params, secret){
        var mk = this.makeSource(method, url_path, params);
        var my_sign = crypto.createHmac('sha1', secret).update(mk).digest().toString('base64');
        return encodeURIComponent(my_sign);
    }

    public static makeSource(method, url_path, params){
        var strs = method.toUpperCase() + '&' + encodeURIComponent(url_path) + '&';
        params = this.objKeySort(params);
        var query_string=[];
        for (var key in params)
        {
            query_string.push(key + '=' + params[key]);
        }
        return strs + (encodeURIComponent(query_string.join('&')).replace('~', '%7E'));
    }
    //排序的函数
    public static objKeySort(obj) {
        var newkey = Object.keys(obj).sort();
        var newObj = {};
        for (var i = 0; i < newkey.length; i++) {//遍历newkey数组
            newObj[newkey[i]] = obj[newkey[i]];//向新创建的对象中按照排好的顺序依次增加键值对
        }
        return newObj;//返回排好序的新对象
    }

    public static getProductByid(zoneid,pid){
        var android = {
            5001:13683,
            5002:13684,
            5003:13685,
            5004:13686,
            5005:13687,
            5006:13688,
            5007:13689,
            5008:13690
        };
        var ios = {
            5001:13691,
            5002:13692,
            5003:13693,
            5004:13694,
            5005:13695,
            5006:13696,
            5007:13697,
            5008:13698
        };
        var prodect = {
            5001:6,
            5002:30,
            5003:68,
            5004:128,
            5005:328,
            5006:648,
            5007:33,
            5008:99
        };
        var price,itemid;
        if(zoneid == 1){
            itemid = android[pid];
            price = prodect[pid];
        }else if(zoneid == 2){
            itemid = ios[pid];
            price = prodect[pid];
        }else{
            price = 0;
        }
        var data = {
            itemid:itemid,
            price:price
        };
        return data;
    }

    public static getTradeNo(channel,uid){
        var now = Time.realNow();
        var str = channel+now.toString()+uid;
        return crypto.createHash("md5").update(str).digest("hex").toLowerCase();
    }

    public static md5tosign(str){
        return crypto.createHash("md5").update(str).digest("hex").toLowerCase();
    }

    public static TalkingData(tradeInfo,cb:(talkres)=>void){
        var url,data = [{
            "msgID":tradeInfo.pid,
            "gameVersion": tradeInfo.channel,
            "OS": "h5",
            "accountID": tradeInfo.uid,
            "level": 0,
            "gameServer":tradeInfo.svr_id,
            "orderID": tradeInfo.trade_no,
            "iapID": tradeInfo.product[3],
            "currencyAmount":Number(tradeInfo.product[1]),
            "currencyType": "CNY",
            "virtualCurrencyAmount": Number(tradeInfo.product[2]),
            "paymentType": "h5",
            "status": "success",
            "chargeTime": Date.now(),
            "mission": tradeInfo.product[0],
            "partner":tradeInfo.partner
        }];
        url = "http://115.159.98.176/htgame/talking.php";
        request.post({url:url,form:data}, function(error, response, body) {
            log.sInfo('productInfo = ' + JSON.stringify(body));
            body = JSON.parse(body);
            if (!error && response.statusCode == 200) {
                if(body.code == 100 ){
                    log.sInfo('productInfo = ' + JSON.stringify(body));
                    if(body.dataStatus[0].code == 1){
                        cb(0);
                    }else{
                        cb(body.dataStatus[0].code);
                    }
                }else{
                    cb(body.code);
                }
            }else{
                log.sError('nickname err = %s', body.data.name);
                cb(body.code);
            }
        });
    }

    public static  upimg(img){
        if(img != '' && typeof img != 'undefined'){
            var imgdata = {
                img:img
            }
            request.post({url:"http://127.0.0.1/cos/upimg.php",form:imgdata}, function(error, response, body) {
                console.log(JSON.stringify(body));
            });
        }
    }
}

export  = COMMON;