import log = require('../../util/log');
import LoginDb = require('../../database/impl/login_db');
import RedisMgr = require('../../redis/redis_mgr');
import request = require("request");
import common = require('../common');
import Time = require('../../util/time');
import url = require("request");

var chnInfo = require('../../config/data/channel.json');

//index 测试
export function index(invoke){
    var args = invoke.args;

    common.upimg(args['img']);
    return invoke.res.send(JSON.stringify(args));
/*
    var url = "http://115.159.98.176/cos/upimg.php";
    var data = {
        img:args['img']
     };
    request.post({url:url,form:data}, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            return invoke.res.send(JSON.stringify(body));
        }else{
            return invoke.res.send(JSON.stringify(response));
        }
    });
*/
}

//玩吧
export function wanba(invoke) {
    var args = invoke.args;
    var token = args['openkey'];
    var fuid = args['uid'];
    args['userip'] = args['IP'].replace('::ffff:', '');
    args['pf'] = 'wanba_ts';
    var chn = args['platform'];
    delete args['m'];
    delete args['IP'];
    delete args['v'];
    delete args['userinfo'];
    delete args['platform'];
    var openid = chn + '-' + args['openid'];
    var secret, gameid, channel = 'wanba';
    if (args['appid'] == '1106277868') {
        secret = 'I8s9338x7wWwNhlB';
        gameid = 30005;
    }
    secret += '&';
    var sig = common.makeSig('get', '/v3/user/get_info', args, secret);
    var str = '';
    for (var obj in args) {
        if (args[obj] == '')
            continue;
        str += obj + '=' + args[obj] + '&';
    }
    var url = 'https://api.urlshare.cn/v3/user/get_info?' + str + 'sig=' + sig;
    var now = Time.realNow();
    request(url, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var data = JSON.parse(body);
            if (data.error == 403 || typeof data.uid == 'undefind') {
                return invoke.res.json('errr');
            }
            if (data.ret == 0) {
                var currtime = Math.floor(Date.now() / 1000);
                common.select('userid,headimgurl,nickname,token', 'player', { openid: openid }, function (replyJson) {
                    token = token.toLowerCase();
                    try {
                        data.nickname = decodeURIComponent(data.nickname);
                    }
                    catch (e) {
                        log.sError('nickname err = %s', data.nickname);
                    }
                    if (typeof replyJson.userid == 'undefined') {
                        var indb = {
                            headimgurl: data.figureurl,
                            nickname: data.nickname,
                            openid: openid,
                            token: token,
                            channel: channel
                        };
                        common.dbin(indb, function (uid) {
                            if (typeof fuid != "undefined" && fuid != 0) {
                                common.select('userid', 'player', { userid: fuid }, function (getfuid) {
                                    LoginDb.conn.execute('insert ignore into offered set ?', { userid: getfuid.userid, offered: uid, gameid: gameid }, function () {
                                    });
                                });
                            }
                            LoginDb.conn.execute('insert ignore into daykeep set ?', { userid: uid, time: currtime, gameid: gameid, channel: channel }, function () {
                            });
                            return invoke.res.json({ token: token, uid: uid });
                        });
                    }
                    else {
                        data.token = token;
                        var updateData = {
                            token: token,
                            headimgurl: data.figureurl,
                            nickname: data.nickname
                        };
                        common.update(updateData, { userid: data.uid }, function (updateJson) {
                            LoginDb.conn.execute('insert ignore into daykeep set ?', { userid: replyJson.userid, time: currtime, gameid: gameid, channel: 'vu' }, function () {
                            });
                            RedisMgr.getInstance('accountTokenRedis').del(replyJson.token, function (del_token) {
                            });
                            RedisMgr.getInstance('accountTokenRedis').hmset(token, { headimgurl: data.figureurl, nickname: data.nickname, uid: replyJson.userid, token: token }, 86400, function () {
                                return invoke.res.json({ token: token });
                            });
                        });
                    }
                });
            }
            else {
                return invoke.res.json(data);
            }
        }
    });
}

export function pay(invoke) {
    var args = invoke.args;
    var now = Time.realNow();
    var secret, gameid, channel = 'wanba';
    if (args['appid'] == '1106277868') {
        secret = 'I8s9338x7wWwNhlB';
        gameid = 30005;
    }
    secret += '&';
    var ip = args['IP'].replace('::ffff:', '');
    var get_playzone_userinfo = {
        openid: args['openid'],
        zoneid: args['zoneid'],
        openkey: args['openkey'],
        appid: args['appid'],
        pf: args['pf'],
        format: 'json',
        userip: ip
    };
    var sig = common.makeSig('get', '/v3/user/get_playzone_userinfo', get_playzone_userinfo, secret);
    var str = '';
    for (var obj in get_playzone_userinfo) {
        if (get_playzone_userinfo[obj] == '')
            continue;
        str += obj + '=' + get_playzone_userinfo[obj] + '&';
    }
    var url = "https://api.urlshare.cn/v3/user/get_playzone_userinfo?" + str + 'sig=' + sig;
    log.sInfo('get_playzone_userinfo = ' + url);
    request(url, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var data = JSON.parse(body);
            if (data.code == 0) {

                var productInfo = common.getProductByid(args['zoneid'], args['pid']);
                log.sInfo('productInfo = ' + JSON.stringify(productInfo));
                if (productInfo.price > 0 && data.data[0].score >= productInfo.price) {
                    var billno = channel + common.getTradeNo(channel, args['uid']);
                    if(args['uid'] > args['srvId'] * Math.pow(10, 10)){
                        args['uid'] = args['uid'] % Math.pow(10, 10);
                    }
                    var orderInfo = {
                        trade_no: billno,
                        uid: args['uid'],
                        pid: args['pid'],
                        gameid: gameid,
                        create_order: now,
                        buy_time: now,
                        channel: channel,
                        param: JSON.stringify(invoke.args),
                        svr_id: args['srvId'],
                        status: 0
                    };
                    LoginDb.conn.execute('insert into wxpay set ?', orderInfo, function (err, conn, result) {
                        log.sInfo('insert into wxpay = ' + JSON.stringify(orderInfo));
                        if (err)
                            throw err;
                        if (result.affectedRows) {
                            var buy_playzone_item = {
                                billno: billno,
                                openid: args['openid'],
                                zoneid: args['zoneid'],
                                openkey: args['openkey'],
                                appid: args['appid'],
                                itemid: productInfo.itemid,
                                pf: args['pf'],
                                format: 'json',
                                count: 1,
                                userip: ip
                            };
                            var sign = common.makeSig('get', '/v3/user/buy_playzone_item', buy_playzone_item, secret);
                            var buystr='';
                            for (var objs in buy_playzone_item) {
                                if (buy_playzone_item[objs] == '')
                                    continue;
                                buystr += objs + '=' + buy_playzone_item[objs] + '&';
                            }
                            var buyUrl = 'https://api.urlshare.cn/v3/user/buy_playzone_item?' + buystr + 'sig=' + sign;
                            log.sInfo('buy_playzone_item = ' + buyUrl);
                            request(buyUrl, function (buyerr, res, buyresult) {
                                if (!buyerr && res.statusCode == 200) {
                                    buyresult = JSON.parse(buyresult);
                                    if (buyresult.code == 0) {
                                        LoginDb.conn.execute('update wxpay set status=1 where trade_no="' + billno + '"', {}, function (uperr, upconn, upresult) {
                                            if (uperr)  throw uperr;
                                            if (upresult.affectedRows) {
                                                var verSignData = {
                                                    trade_no: billno,
                                                    uid: args['uid'],
                                                    pid: args['pid'],
                                                    gameid: gameid,
                                                    time: now
                                                };
                                                log.sInfo('noticeNode = ' + JSON.stringify(verSignData));
                                                common.noticeNode(verSignData, args['srvId'],function(notice){
                                                    /*common.TalkingData(verSignData,function(talk){
                                                        invoke.res.json({ code: notice.result });
                                                    });*/
                                                });
                                            }
                                            else {
                                                return invoke.res.json({ code: 2018, pid: args['pid'], msg: '重复或已经支付成功' });
                                            }
                                        });
                                    }
                                    else {
                                        buyresult.pid = args['pid'];
                                        return invoke.res.send(buyresult);
                                    }
                                }
                                else {
                                    return invoke.res.json({ code: 1900, pid: args['pid'], msg: '网络错误' });
                                }
                            });
                        }
                    });
                }
                else {
                    return invoke.res.json({ code: 1004, pid: args['pid'], msg: '账户余额不足' });
                }
            }
            else {
                data.pid = args['pid'];
                return invoke.res.json(data);
            }
        }
    });
}
//越山SDK
export function ys(invoke) {
    var args = invoke.args;
    var now = Time.realNow();
    var url = "http://ys.jygame.net/ysgame/ysapi/getUserInfo.php";
    var token = args['Token'];
    var yspt = args['yspt'];
    var chnkey = 'ys'+yspt;
    log.sInfo('ys login = ' + JSON.stringify(args));
    if(isNaN(yspt) &&  typeof chnInfo[chnkey] == 'undefined')
    {
        return invoke.res.json({ token: ''});
    }
    var requestData = {
        token:token,
        time:now,
        appID:chnInfo[chnkey].appID
    };
    var str='',gameid=6;
    var obj = common.objKeySort(requestData);
    for(var k in  obj){
        str += k+'='+obj[k];
    }
    requestData['sign'] = common.md5tosign(str+chnInfo[chnkey].appSecret);
    request.post({url:url, form:requestData}, function(error, response, body) {
        body = JSON.parse(body);
        if (!error && response.statusCode == 200) {
            if(body.code == 0){
                var timeStamp = (new Date().setHours(0, 0, 0, 0)) / 1000;

                common.select('userid,headimgurl,nickname,token,lastLogin', 'player', { openid: body.data.id }, function (replyJson) {
                    try{
                        body.data.name = decodeURIComponent(body.data.name);
                    }catch(e){
                        log.sError('nickname err = %s', body.data.name);
                    }
                    var channel = chnkey;

                    if (typeof replyJson.userid == 'undefined') {
                        var indb = {
                            headimgurl: body.data.pic,
                            nickname: body.data.name,
                            openid: body.data.id,
                            token: token,
                            lastLogin:now,
                            channel: channel
                        };
                        common.dbin(indb, function (uid) {
                            LoginDb.conn.execute('insert ignore into daykeep set ?', { userid: uid, time: now, gameid: gameid, channel: channel }, function () {});
                            common.upimg(body.data.pic);
                            if(typeof args['fid'] != "undefined" && args['fid'] != 0){
                                common.select('userid','player',{ openid: args['fid'] },(getfuid)=> {
                                    if(getfuid.userid !=uid){
                                        LoginDb.conn.execute('insert ignore into offered set ?', {userid:getfuid.userid,offered:uid,gameid:gameid}, ()=>{});
                                        LoginDb.conn.execute('insert ignore into recall set ?', {uid:getfuid.userid,user:uid,gameid:gameid,time:timeStamp}, ()=>{});
                                    }
                                });
                            }
                            return invoke.res.json({ token: token});
                        });
                    }else {
                        if(typeof args['fid'] != "undefined" && args['fid'] != 0) {
                            common.select('userid', 'player', {openid: args['fid']}, (getfuid)=> {
                                if(getfuid.userid != replyJson.userid)
                                    LoginDb.conn.execute('insert ignore into recall set ?', {
                                        uid: getfuid.userid,
                                        user: replyJson.userid,
                                        gameid: gameid,
                                        time: timeStamp
                                    }, ()=> {});
                            });
                        }
                        LoginDb.conn.execute('insert ignore into daykeep set ?', { userid: replyJson.userid, time: now, gameid: gameid, channel: channel }, function () {});
                        if(now - replyJson.lastLogin > 86400){
                            LoginDb.conn.execute('update player set token=?,headimgurl=?,nickname=?,lastLogin=? where ?',
                                [token,body.data.pic,body.data.name,now,{userid:replyJson.userid}], (err, connection, result)=> {
                                    if (err)  return err;
                                    if (!result || result.length === 0) {
                                        return invoke.res.json({ token: replyJson.token });
                                    }
                                    if(result.affectedRows == 1){
                                        common.upimg(body.data.pic);
                                        RedisMgr.getInstance('accountTokenRedis').del(replyJson.token, function () {});
                                        RedisMgr.getInstance('accountTokenRedis').hmset(token, { headimgurl: body.data.pic, nickname: body.data.name, uid: replyJson.userid, token: token }, 86400, function () {
                                            return invoke.res.json({ token: token });
                                        });
                                    }
                                });

                        }else{
                            RedisMgr.getInstance('accountTokenRedis').hmget(token, ['uid'], function (error, reply) {
                                if(typeof reply['uid'] == 'object'){
                                    return invoke.res.json({ token: replyJson.token});
                                }else{
                                    RedisMgr.getInstance('accountTokenRedis').hmset(token, { headimgurl: body.data.pic, nickname: body.data.name, uid: replyJson.userid, token: token }, 86400, function () {
                                        return invoke.res.json({ token: replyJson.token});
                                    });
                                }
                            });
                        }
                    }
                });
            }else{
                return invoke.res.json({ token: ''});
            }
        }else{
            return invoke.res.json({ token: ''});
        }
    })
}

export function yspay(invoke){
    var now = Time.realNow();
    var args = invoke.args;
    log.sInfo('yspay post data = ' + JSON.stringify(args));
    var gameid=6;
    var arr,partner,ext = args['ext'];
    arr = ext.split("|");
    var chnkey = 'ys'+arr[0];
    partner = arr[1];
    var yssign,trans_id = chnkey + args['orderId'];

    if(isNaN(arr[0])){
        return invoke.res.json({ code: 1001 });
    }
    var str = '';
    yssign = args['sign'];
    delete args['m'];
    delete args['IP'];
    delete args['sign'];
    var obj = common.objKeySort(args);
    for(var k in  obj){
        str += k+'='+obj[k];
    }
    var sign = common.md5tosign(str + chnInfo[chnkey].appSecret);

    var product = {
        6001:['600元宝',6,60,'6元600元宝'],
        6002:['3300元宝',30,3300,'30元3300元宝'],
        6003:['11300元宝',98,11300,'98元11300元宝'],
        6004:['32000元宝',268,32000,'268元32000元宝'],
        6005:['84200元宝',648,84200,'648元84200元宝'],
        6006:['99800元宝',998,99800,'998元99800元宝'],
        6007:['月卡',40,0,'月卡'],
        6008:['终身卡',98,0,'终身卡'],
        6009:['一元购',1,0,'1元限购1号.计VIP和充值次数'],
        6010:['一元购',1,0,'1元限购2号.计VIP和充值次数'],
        6011:['一元购',1,0,'1元限购3号.计VIP和充值次数'],
        6012: ['3元', 3, 0, '限购档3元。'],
        6013: ['18元', 18, 0, '限购档18元。'],
        6014: ['68元', 68, 0, '限购档68元。'],
        6015: ['168元', 168, 0, '限购档168元。'],
        6016: ['328元', 328, 0, '限购档328元。'],
        6017: ['518元', 518, 0, '限购档518元。'],
        6018: ['648元', 648, 0, '限购档648元。'],
        6019: ['998元', 998, 0, '强攻令'],
        6020: ['168元', 168, 0, '购买一颗转生丸。'],
        6021: ['648元', 648, 0, '7.7折购买五颗转生丸。'],
        6022: ['18元', 18, 0, '每日限购档18元。'],
        6023: ['30元', 30, 0, '每日限购档30元。'],
        6024: ['98元', 98, 0, '每日限购档98元。'],
        6025: ['268元', 268, 0, '每日限购档268元。'],
        6026: ['518元', 518, 0, '每日限购档518元。'],
        6027: ['998元', 998, 0, '每日限购档998元。'],
    };

    if( product[args['goodsId']][1] == Number(args['money']) && sign == yssign){
        common.select('userid,channel','player',{ openid: args['id'] },(getUid)=> {

            if(typeof getUid.userid == 'undefined'){
                return invoke.res.json({ code: 1004 });
            }

            common.select('status','wxpay',{trade_no: trans_id},(replayJson)=>{
                var verSignData = {
                    trade_no: trans_id,
                    uid: getUid.userid,
                    pid: args['goodsId'],
                    gameid: gameid,
                    time: args['time']
                };
                var talkd = {
                    trade_no: trans_id,
                    uid: getUid.userid,
                    pid: args['goodsId'],
                    gameid: gameid,
                    time: args['time'],
                    svr_id:args['serverId'],
                    product:product[args['goodsId']],
                    partner:partner,
                    channel:arr[0]
                };
                if(replayJson.status ===0){
                    LoginDb.conn.execute('update wxpay set status=1,buy_time=' + args['time'] + ' where trade_no="'+trans_id+'"',{}, (err, connection, result)=> {
                        if (err)throw err;
                        common.noticeNode(verSignData, args['serverId'],function(notice){
                            common.TalkingData(talkd,function(tak){
                                if(tak == 0){
                                    return invoke.res.json({ code: notice.result });
                                }else{
                                    return invoke.res.json({ code: 1001 });
                                }
                            });
                        });
                    });
                }else {
                    if ( typeof replayJson.status == 'undefined') {
                        LoginDb.conn.execute('insert into wxpay set ?', {
                            trade_no: trans_id,
                            uid: getUid.userid,
                            pid: args['goodsId'],
                            gameid: gameid,
                            create_order: args['time'],
                            buy_time: args['time'],
                            channel: getUid.channel,
                            param: JSON.stringify(invoke.args),
                            svr_id: args['serverId'],
                            status: 1
                        }, (err, conn, result)=> {
                            if (err) throw err;
                            common.noticeNode(verSignData, args['serverId'],function(notice){
                                common.TalkingData(talkd,function(tak){
                                    if(tak == 0){
                                        return invoke.res.json({ code: notice.result });
                                    }else{
                                        return invoke.res.json({ code: 1001 });
                                    }
                                });

                            });
                        });
                    }else if(replayJson.status == 1){
                        common.noticeNode(verSignData, args['serverId'],function(notice){
                            common.TalkingData(talkd,function(tak){
                                if(tak == 0){
                                    return invoke.res.json({ code: notice.result });
                                }else{
                                    return invoke.res.json({ code: 1001 });
                                }
                            });
                        });
                    }else{
                        return invoke.res.json({code:0});
                    }
                }
            });
        });
    }else{
        return invoke.res.json({code:1003});
    }
}

