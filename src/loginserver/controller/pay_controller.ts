import log = require('../../util/log');
import common = require('../common');
import async = require('async');

export function index(invoke){
    var str,args = invoke.args;
    delete args['m'];
    delete args['IP'];
    str = common.strBuff(args);
    log.sInfo('index args = ' + str);
    return invoke.res.json('SUCCESS');
}

export function test(invoke){
    var task1 =function(callback){
        console.log("task1");
        callback(null,"11")
    }

    var task2 =function(q,callback){
        console.log("task2");
        console.log("task1函数传入的值: "+q);
        callback(null,"22")
    }

    var task3 =function(q,callback){
        console.log("task3");
        console.log("task2函数传入的值: "+q);
        callback(null,"33")
    }
    console.time("waterfall方法");
    async.waterfall([task1,task2,task3],function(err,result){

        console.log("waterfall");
        if (err) console.log(err);
        console.log("result : "+result);
        console.timeEnd("waterfall方法");
    })
}