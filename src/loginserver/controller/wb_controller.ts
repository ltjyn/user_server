import log = require('../../util/log');
import LoginDb = require('../../database/impl/login_db');
import Dispatcher = require('../../http/dispatcher');
import RedisMgr = require('../../redis/redis_mgr');
import request = require("request");
import SDK = require('../sdk');
var Ygapi = require('../../molten/yg_api.js');
var gameid = 5;
export function login(invoke:Dispatcher.Invoke):any {

    return invoke.res.json('wb: back, ret=' + JSON.stringify(invoke.args));

    var token = invoke.args['token'];
    var fuid = invoke.args['fuid'];
    var url = 'http://api.11h5.com:4100/login?cmd=checkUserToken&userToken=' + token;
    // var url = 'http://www.moltentec.com/htgame/index.php?c=Testmc&m=checkUserToken&token=' + token;
    var redirect_url = 'http://qcdn.moltentec.com/ivu.html?'+Math.random();
    var gameurl = 'http://qcdn.moltentec.com/aiweiyouNew/5/';

    var currtime = Math.floor(Date.now() / 1000);

    request(url,function (error, response, body) {

        if (!error && response.statusCode == 200) {
            var data = JSON.parse(body);

            if (data.error == 403 || typeof data.uid == 'undefind' || data.uid === null || data.uid == '') {
                return invoke.res.redirect(redirect_url);
            }

            SDK.select('userid,headimgurl,nickname,token','player',{ openid: data.uid },(replyJson)=> {

                try{
                    data.nickname = decodeURIComponent(data.nickname);
                }catch(e){
                    log.sError('nickname err = %s', data.nickname);
                }

                if(typeof replyJson.userid == 'undefined') {
                    SDK.insert(currtime,data.headimgurl, data.nickname, data.uid, token, (uid)=> {

                        if(typeof fuid != "undefined"){
                            SDK.select('userid','player',{ openid: fuid },(getfuid)=> {
                                LoginDb.conn.execute('insert ignore into offered set ?', {userid:getfuid.userid,offered:uid,gameid:gameid}, ()=>{});
                            });
                        }
                        LoginDb.conn.execute('insert ignore into daykeep set ?', { userid: uid, time: currtime, gameid: gameid, channel:'vu' }, function () {});
                        return invoke.res.redirect(gameurl+'index.html?token='+token +'&uid='+ data.uid);
                    });
                }else{
                    data.token = token;
                    var updateData = {
                        token:token,
                        headimgurl:data.headimgurl,
                        nickname: data.nickname
                    }
                    SDK.update(updateData,{openid:data.uid},(updateJson)=>{
                        LoginDb.conn.execute('insert ignore into daykeep set ?', { userid: replyJson.userid, time: currtime, gameid: gameid, channel:'vu' }, function () {});
                        RedisMgr.getInstance('accountTokenRedis').del(replyJson.token,(del_token)=>{});
                        RedisMgr.getInstance('accountTokenRedis').hmset(token,
                            {headimgurl: data.headimgurl,
                            nickname: data.nickname,
                            uid:replyJson.userid,
                            token: data.token
                        }, 604800, ()=> {
                            return invoke.res.redirect(gameurl+'index.html?token='+token +'&uid='+ data.uid);
                        });
                    });
                }
            });
        } else {
            return invoke.res.redirect(redirect_url);
        }
    });



    /*LoginDb.conn.execute('insert into userinfo set ?', data, (err,conn,result)=> {
         if (err) throw err;
         //console.log('INSERT ID:',result.insertId);
         var uid = result.insertId;
         var accountTokenRedis = RedisMgr.getInstance('accountTokenRedis');

         accountTokenRedis.del(token, (error) => {
            if(error) throw error;
         });

         accountTokenRedis.hmset(token, {
             uid: uid,
             headimgurl: args['headimgurl'],
             nickname: args['nickname'],
             token: args['token']
         }, 86400, ()=> {

            invoke.res.redirect(url + token);
         });
     });*/
    /*
    * LoginDb.conn.execute('select uid from userinfo where ?',{openid :data.uid}, function (err, connection, result) {
         if (err) throw err;
         if (!result || result.length === 0) {

         }
         return invoke.res.json(result);
     });*/
    /*for(var key in args){
        if(key !='IP' && key != 'm'){
            data += key +'='+ args[key]+',';
        }
    }*/
    //log.sError('role create error, data=' + data);
    //invoke.res.json(data);
    /* SELECT  TABLE */
    /*var uid = invoke.args['uid'];
    LoginDb.conn.execute('select * from ' + tableName + ' where ?', { uid: uid }, function (err, connection, result) {
        if (err) {
            return invoke.res.json('null');
        }
        if (!result || result.length === 0) {
            return invoke.res.json(result);
        }
        return invoke.res.json(result[0]);
    });*/
    /*SDK.select('uid,headimgurl,nickname',{ openid: openid },(replyJson)=> {
        return invoke.res.json(replyJson);
    });*/
    /* INSERT INTO TABLE */
    /*RedisMgr.getInstance('accountTokenRedis').hmget(token, ['uid','headimgurl','nickname'], (err,replyJson)=> {
        return invoke.res.json(replyJson);
    });*/
    /*SDK.insert(headimgurl, nickname, openid, token,(uid)=> {
        invoke.res.redirect('http://www.baidu.com?token='+uid);
    });*/
    /* UPDATE  TABLE */
    /* update(1,args);
     function update(id:number,args:any):any{
        LoginDb.conn.execute('update passport set token="' +args['token']+ '" where uid='+id,{}, (err, connection, result)=> {
            if (err)  return;
            if (!result || result.length === 0) {
                return ;
            }
            return invoke.res.json(result);
        });
    }
    invoke.res.json('echo1: give ur args back, ret=' + JSON.stringify(invoke.args));*/
}

export function pay(invoke:Dispatcher.Invoke):any{
    var args = invoke.args;
    delete args['IP'];
    log.sInfo(JSON.stringify(args));
    Ygapi.init({'apiKey' : 'IL7SnndiPhUT9YeOAKshJDQBeZO6yC8U','gameId' : 133});
    Ygapi.verifyDelivery(args, function (err) {
        if (err) {
            // 返回客户端表示发货失败
            console.log(err);
            return invoke.res.send(err.Error);
        } else {
            var proid = {
                8208:6,8209:30,8210:68,8211:128,8212:328,8213:648,8214:33,8215:99
            }
            var vuproTopro = {
                8208:5001,8209:5002,8210:5003,8211:5004,8212:5005,8213:5006,8214:5007,8215:5008
            }

            if(proid[args['product_id']] == parseInt(args['rmb'])){
                // 检验订单是否重复，由于网络原因发货平台可能会重复通知
                SDK.select('userid','player',{ openid: args['uid'] },(getUid)=> {
                    SDK.select('status','wxpay',{trade_no:args['trans_id']},(replayJson)=>{
                        var time = Math.floor(Date.now() / 1000);
                        if(replayJson.status ===0){
                            LoginDb.conn.execute('update wxpay set status=1,buy_time=' + time + ' where trade_no="'+args['trans_id']+'"',{}, (err, connection, result)=> {
                                if (err)throw err;
                                return invoke.res.send('SUCCESS');
                            });
                        }else {
                            if (typeof replayJson.status == 'undefined') {
                                LoginDb.conn.execute('insert into wxpay set ?', {
                                    trade_no: args['trans_id'],
                                    uid: getUid.userid,
                                    pid: vuproTopro[args['product_id']],
                                    gameid: gameid,
                                    create_order: time,
                                    buy_time: time,
                                    channel: 'vu',
                                    param: JSON.stringify(invoke.args),
                                    svr_id: args['userdata'],
                                    status: 1
                                }, (err, conn, result)=> {
                                    if (err) throw err;
                                    var verSignData = {
                                        trade_no: args['trans_id'],
                                        uid: getUid.userid,
                                        pid: vuproTopro[args['product_id']],
                                        gameid: gameid,
                                        time: time
                                    };
                                    noticeNode(verSignData);
                                });
                            }else if(replayJson.status == 1){
                                var verSignData = {
                                    trade_no: args['trans_id'],
                                    uid: getUid.userid,
                                    pid: vuproTopro[args['product_id']],
                                    gameid: gameid,
                                    time: time
                                };
                                noticeNode(verSignData);
                            }else{
                                return invoke.res.send('SUCCESS');
                            }
                        }
                    });
                });
            }else {
                // 发货相关逻辑 : 根据商户平台配置不同的产品id(productId), 发送不同的钻石
                return invoke.res.send('fail');
                // 发货成功后返回客户端表示发货成功
                //res.end('SUCCESS');
            }
        }
    });

    function noticeNode(verSignData){
        var mySign = SDK.genSign(verSignData, gameid);
        var content = SDK.strBuff(verSignData);
        SDK.select('connectIp', 'web_data', {id: gameid}, (getIp)=> {
            var payUrl = 'http://' + getIp.connectIp + '/pay?m=index&' + content + '&svr_id=' + args['userdata'] + '&sign=' + mySign;
            console.log(payUrl);
            request(payUrl,function (error, response, paybody) {
                if (!error && response.statusCode == 200) {
                    var paydata = JSON.parse(paybody);
                    if(paydata == 'SUCCESS') {
                        LoginDb.conn.execute('update wxpay set status=2 where trade_no="' + args['trans_id'] + '"', {}, function (err, connection, result) {
                            log.sInfo(' paydata = %d ',result.affectedRows);
                            if (err){
                                throw err;
                            }
                            if(result.affectedRows)
                            {
                                return invoke.res.send('SUCCESS');
                            }else{
                                return invoke.res.send('fail');
                            }

                        });
                    }else {
                        return invoke.res.send('fail');
                    }
                }else {
                    return invoke.res.send('fail');
                }
            });
        });
    }
}