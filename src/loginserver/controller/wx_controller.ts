/**
 * 这种控制器是不需要拦截器拦截的写法
 */

import log = require('../../util/log');
import Dispatcher = require('../../http/dispatcher');
import index = require('../../../index');
import express = require('express');
import HttpUtil = require('../../util/http_util');
import Enum = require('../../util/enum');
import crypto = require('crypto');
import SDK = require('../sdk');

var config = require('../../config/untrack/loginserver.json');

var weixinappId = config['info']['weixinappid'];
var weixinsecret = config['info']['weixinsecret'];
var currtime = Math.floor(Date.now() / 1000);
class LoginController {
    // TODO need to be put on cdn
    index(invoke:Dispatcher.Invoke):void {
        var token = invoke.args['token'];
        var uid = invoke.args['uid'];

        invoke.res.sendFile(index.sourceRoot + '/src/page/index.html');
    }

    login(invoke:Dispatcher.Invoke):void {
        var gameid = invoke.args['gameid'];
        if (invoke.req.headers['user-agent'].indexOf('MicroMessenger') === -1) {
            invoke.res.sendFile(index.sourceRoot + '/src/page/index.html?gameid=' + gameid);
        }
        else {
            // TODO
            var urlInfo = '';
            var redirectUrl =
                invoke.res.redirect(urlInfo);
        }
    }

    ret(invoke:Dispatcher.Invoke):void {
        var code = invoke.args['code'];
        if (code) {
            this.accessToken(code, (uid, token)=> {
                var now = parseInt((Date.now() / 1000).toString());
                // TODO
                var url = 'aaa' + now + '&token=' + token + '&uid=' + uid;
                invoke.res.redirect(url);
            })
        }
    }

    private accessToken(code:any, cb:(uid, token)=>void):void {
        //TODO
        var url = 'aaa' + weixinappId + '&secret=' + weixinsecret + '&code=' + code + 'grant_type=authorization_code';
        HttpUtil.httpGet(url, (err, data) => {
            if (err) {
                throw err;
            }

            var obj = JSON.parse(data);
            this.getUserInfo(obj['token'], obj['openid'], (headimgurl, nickname, openid) => {
                if (err) {
                    throw err;
                }

                // insert
                var md5sum = crypto.createHash('md5');
                md5sum.update(code);
                var ret = md5sum.digest('hex');

                SDK.insert(currtime,headimgurl, nickname, openid, ret, (uid)=> {
                    cb(uid, obj['token']);
                });
            });
        }, Enum.HTTP_RES_DATA_TYPE.RAW_DATA, 15);
    }

    private getUserInfo(token, openId, cb:(headimgurl, nickname, openid)=>void) {
        //TODO
        var url = 'aaa' + token + '&openid=' + openId + '&lang=zh_CN';
        HttpUtil.httpGet(url, (err, data) => {
            if (err) {
                throw err;
            }
            var obj = JSON.parse(data);
            cb(obj['headimgurl'], obj['nickname'], obj['openid']);
        }, Enum.HTTP_RES_DATA_TYPE.RAW_DATA, 15);
    }
}

export = LoginController;