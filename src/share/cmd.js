module.exports = require("protobufjs").newBuilder({})['import']({
    "package": null,
    "messages": [
        {
            "name": "Error",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "code",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "param",
                    "id": 2
                }
            ]
        },
        {
            "name": "Reward",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "id",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "count",
                    "id": 2
                }
            ]
        },
        {
            "name": "Pair",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "key",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "value",
                    "id": 2
                }
            ]
        },
        {
            "name": "HeroBattle",
            "fields": [
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "armor",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "currentHp",
                    "id": 9
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "health",
                    "id": 10
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "attack",
                    "id": 11
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "defence",
                    "id": 12
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "critical",
                    "id": 13
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "hit",
                    "id": 14
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "dodge",
                    "id": 15
                }
            ]
        },
        {
            "name": "BattleArmy",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "army",
                    "id": 1
                }
            ]
        },
        {
            "name": "Hero",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "uid",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "ID",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "name",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "isUnlock",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "hairType",
                    "id": 10
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "hairColor",
                    "id": 11
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "faceType",
                    "id": 12
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "skinColor",
                    "id": 13
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "cape",
                    "id": 14
                },
                {
                    "rule": "optional",
                    "type": "HeroBattle",
                    "name": "dungeonCopy",
                    "id": 20
                },
                {
                    "rule": "optional",
                    "type": "HeroBattle",
                    "name": "bossCopy",
                    "id": 21
                },
                {
                    "rule": "optional",
                    "type": "HeroBattle",
                    "name": "arenaCopy",
                    "id": 22
                }
            ]
        },
        {
            "name": "Equip",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "uid",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "ID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "level",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "exp",
                    "id": 4
                }
            ]
        },
        {
            "name": "FriendHero",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "accountId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "Hero",
                    "name": "hero",
                    "id": 2
                }
            ]
        },
        {
            "name": "FightTeam",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "heros",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "hires",
                    "id": 2
                }
            ]
        },
        {
            "name": "FightRoundData",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "attackerSide",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "attackerIndex",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "defenderIndex",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "attackFlag",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "isSlay",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "damage",
                    "id": 6
                }
            ]
        },
        {
            "name": "CacheHealth",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "uid",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "currentHp",
                    "id": 2
                }
            ]
        },
        {
            "name": "FightRound",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "CacheHealth",
                    "name": "leftHealth",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "CacheHealth",
                    "name": "rightHealth",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "FightRoundData",
                    "name": "roundEntry",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "Loot",
                    "name": "fightLoot",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "TYPE",
                    "name": "fightType",
                    "id": 5
                }
            ],
            "enums": [
                {
                    "name": "TYPE",
                    "values": [
                        {
                            "name": "PVE",
                            "id": 0
                        },
                        {
                            "name": "PVP",
                            "id": 1
                        },
                        {
                            "name": "PVE_EPIC_BOSS",
                            "id": 2
                        },
                        {
                            "name": "PVE_CALL_BOSS",
                            "id": 3
                        }
                    ]
                }
            ]
        },
        {
            "name": "PvEFightNet",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "stageID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "hasFinishedWave",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "monsterGroupID",
                    "id": 3
                }
            ]
        },
        {
            "name": "CraftTask",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "buildUid",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "craftSkillID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "leftTime",
                    "id": 3
                }
            ]
        },
        {
            "name": "AchievementProgress",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "ID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "counter",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "updateTime",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "AchievementStatus",
                    "name": "status",
                    "id": 4
                }
            ],
            "enums": [
                {
                    "name": "AchievementStatus",
                    "values": [
                        {
                            "name": "DOING",
                            "id": 0
                        },
                        {
                            "name": "FINISHED_NOT_READ",
                            "id": 1
                        },
                        {
                            "name": "FINISHED_HAS_READ",
                            "id": 2
                        }
                    ]
                }
            ]
        },
        {
            "name": "Quest",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "ID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "TYPE",
                    "name": "questType",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "STATUS",
                    "name": "questStatus",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "Progress",
                    "name": "questProgress",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "updateTime",
                    "id": 6
                }
            ],
            "messages": [
                {
                    "name": "Progress",
                    "fields": [
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "ID",
                            "id": 1
                        },
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "counter",
                            "id": 2
                        }
                    ]
                }
            ],
            "enums": [
                {
                    "name": "TYPE",
                    "values": [
                        {
                            "name": "NULL",
                            "id": 0
                        },
                        {
                            "name": "NORMAL",
                            "id": 1
                        },
                        {
                            "name": "LIMIT_TIME",
                            "id": 2
                        }
                    ]
                },
                {
                    "name": "STATUS",
                    "values": [
                        {
                            "name": "NEW",
                            "id": 0
                        },
                        {
                            "name": "READ",
                            "id": 1
                        },
                        {
                            "name": "CAN_COMPLETE",
                            "id": 2
                        },
                        {
                            "name": "COMPLETE",
                            "id": 3
                        }
                    ]
                }
            ]
        },
        {
            "name": "QuestUpdate",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "ID",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "Quest.STATUS",
                    "name": "questStatus",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "Quest.Progress",
                    "name": "questProgress",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "updateTime",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "Quest.TYPE",
                    "name": "questType",
                    "id": 5
                }
            ]
        },
        {
            "name": "CompleteQuest",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "ID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "completeTime",
                    "id": 2
                }
            ]
        },
        {
            "name": "Loot",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Pair",
                    "name": "resource",
                    "id": 1
                }
            ]
        },
        {
            "name": "FightLoot",
            "fields": [
                {
                    "rule": "optional",
                    "type": "Loot",
                    "name": "normalLoot",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "Loot",
                    "name": "specialLoot",
                    "id": 2
                }
            ]
        },
        {
            "name": "LeaderboardItem",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "accountId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "Avatar",
                    "name": "avatar",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "level",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "username",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "achievementID",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "score",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "rank",
                    "id": 7
                }
            ]
        },
        {
            "name": "FriendApplication",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "friendId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "username",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "applyTime",
                    "id": 3
                }
            ]
        },
        {
            "name": "CenterFriendInfo",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "accountId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "usernmae",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "level",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "armorID",
                    "id": 4
                }
            ]
        },
        {
            "name": "ArenaHistory",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "arenaId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "rank",
                    "id": 2
                }
            ]
        },
        {
            "name": "FightHero",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "uid",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "ID",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "name",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "hairType",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "hairColor",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "faceType",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "skinColor",
                    "id": 7
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "cape",
                    "id": 8
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "currentHp",
                    "id": 9
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "health",
                    "id": 10
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "attack",
                    "id": 11
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "defence",
                    "id": 12
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "critical",
                    "id": 13
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "hit",
                    "id": 14
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "dodge",
                    "id": 15
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "armorID",
                    "id": 16
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "armorLevel",
                    "id": 17
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "level",
                    "id": 18
                }
            ]
        },
        {
            "name": "RoleProfile",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "accountId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "friendCode",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "HeroInfo",
                    "name": "heroInfo",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "ProgressionSummary",
                    "name": "summary",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "ArenaCombatSummary",
                    "name": "arena",
                    "id": 5
                }
            ],
            "messages": [
                {
                    "name": "HeroBattleInfo",
                    "fields": [
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "armorID",
                            "id": 1
                        },
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "armorLevel",
                            "id": 2
                        },
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "maxHp",
                            "id": 3
                        },
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "attack",
                            "id": 4
                        },
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "defence",
                            "id": 5
                        }
                    ]
                },
                {
                    "name": "HeroInfo",
                    "fields": [
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "level",
                            "id": 1
                        },
                        {
                            "rule": "required",
                            "type": "string",
                            "name": "name",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "uint32",
                            "name": "hairType",
                            "id": 3
                        },
                        {
                            "rule": "optional",
                            "type": "uint32",
                            "name": "hairColor",
                            "id": 4
                        },
                        {
                            "rule": "optional",
                            "type": "uint32",
                            "name": "faceType",
                            "id": 5
                        },
                        {
                            "rule": "optional",
                            "type": "uint32",
                            "name": "skinColor",
                            "id": 6
                        },
                        {
                            "rule": "optional",
                            "type": "uint32",
                            "name": "cape",
                            "id": 7
                        },
                        {
                            "rule": "optional",
                            "type": "HeroBattleInfo",
                            "name": "dungeonCopy",
                            "id": 10
                        },
                        {
                            "rule": "optional",
                            "type": "HeroBattleInfo",
                            "name": "bossCopy",
                            "id": 11
                        },
                        {
                            "rule": "optional",
                            "type": "HeroBattleInfo",
                            "name": "arenaCopy",
                            "id": 12
                        }
                    ]
                },
                {
                    "name": "ProgressionSummary",
                    "fields": [
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "equipAchievementID",
                            "id": 1
                        },
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "completedAchievementCount",
                            "id": 2
                        },
                        {
                            "rule": "repeated",
                            "type": "uint32",
                            "name": "epicBossesFought",
                            "id": 3
                        }
                    ]
                },
                {
                    "name": "ArenaCombatSummary",
                    "fields": [
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "win",
                            "id": 1
                        },
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "loss",
                            "id": 2
                        },
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "revengeWin",
                            "id": 3
                        },
                        {
                            "rule": "repeated",
                            "type": "ArenaHistory",
                            "name": "tournamentWinnings",
                            "id": 4
                        }
                    ]
                }
            ]
        },
        {
            "name": "FriendInfo",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "accountId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "RoleProfile.HeroInfo",
                    "name": "heroInfo",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "win",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "loss",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "TYPE",
                    "name": "infoType",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "equipAchievementID",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "battleLeftCoolDown",
                    "id": 7
                }
            ],
            "enums": [
                {
                    "name": "TYPE",
                    "values": [
                        {
                            "name": "APPLICATION",
                            "id": 1
                        },
                        {
                            "name": "FRIEND",
                            "id": 2
                        }
                    ]
                }
            ]
        },
        {
            "name": "Avatar",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "armorID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "armorLevel",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "hairType",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "hairColor",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "faceType",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "skinColor",
                    "id": 6
                }
            ]
        },
        {
            "name": "OpponentInfo",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "accountId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "Avatar",
                    "name": "avatar",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "level",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "username",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "achievementID",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "score",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "rank",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "isRevenge",
                    "id": 8
                }
            ]
        },
        {
            "name": "LastTopArenaRole",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "accountId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "Avatar",
                    "name": "avatar",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "username",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "level",
                    "id": 4
                }
            ]
        },
        {
            "name": "LastTournamentReward",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "rank",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "rewardId",
                    "id": 2
                }
            ]
        },
        {
            "name": "TournamentHistory",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "tournamentId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "rank",
                    "id": 2
                }
            ]
        },
        {
            "name": "LastBossReward",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "bossID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "rank",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "rewardId",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_login_cs_login_auth",
            "options": {
                "code": 6000
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "passport",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "platformId",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "deviceUid",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "device",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "deviceType",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "deviceToken",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "clientVersion",
                    "id": 7
                }
            ]
        },
        {
            "name": "cmd_login_sc_login_auth",
            "options": {
                "code": 6001
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "serverId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "gmAuth",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_login_cs_login_getServerList",
            "options": {
                "code": 6002
            },
            "fields": []
        },
        {
            "name": "cmd_login_sc_login_getServerList",
            "options": {
                "code": 6003
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Server",
                    "name": "serverList",
                    "id": 1
                }
            ],
            "messages": [
                {
                    "name": "Server",
                    "fields": [
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "id",
                            "id": 1
                        },
                        {
                            "rule": "required",
                            "type": "string",
                            "name": "name",
                            "id": 2
                        },
                        {
                            "rule": "required",
                            "type": "bool",
                            "name": "canLogin",
                            "id": 3
                        }
                    ]
                }
            ]
        },
        {
            "name": "cmd_login_cs_login_chooseServer",
            "options": {
                "code": 6004
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "serverId",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_login_sc_login_chooseServer",
            "options": {
                "code": 6005
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ip",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "port",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "resVersion",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "version",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "resServerAddr",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "canUpdate",
                    "id": 6
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "forbidFuncList",
                    "id": 7
                }
            ]
        },
        {
            "name": "cmd_login_cs_login_getInfo",
            "options": {
                "code": 6006
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "platformId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "clientVersion",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_login_sc_login_getInfo",
            "options": {
                "code": 6007
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "notice",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "version",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "updateAddr",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_login_cs_login_queryAccountIdServerId",
            "options": {
                "code": 6008
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "passport",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "platformId",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "queryAccountId",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_login_sc_login_queryAccountIdServerId",
            "options": {
                "code": 6009
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "serverId",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_center_cs_center_handConnect",
            "options": {
                "code": 7000
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "serverId",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_center_sc_center_handConnect",
            "options": {
                "code": 7001
            },
            "fields": []
        },
        {
            "name": "cmd_center_cs_center_heartBeat",
            "options": {
                "code": 7002
            },
            "fields": []
        },
        {
            "name": "cmd_center_sc_center_heartBeat",
            "options": {
                "code": 7003
            },
            "fields": []
        },
        {
            "name": "cmd_rpc_cs_rpc_rpcCall",
            "options": {
                "code": 7004
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "rpcCode",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "uid",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "request",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_rpc_sc_rpc_rpcCall",
            "options": {
                "code": 7005
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "uid",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bytes",
                    "name": "response",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlayerInfo",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "accountId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "username",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "level",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "hairType",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "hairColor",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "faceType",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "skinColor",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "cape",
                    "id": 8
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "achievementId",
                    "id": 9
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "dungeonArmorID",
                    "id": 10
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "dungeonArmorLevel",
                    "id": 11
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "arenaArmorID",
                    "id": 12
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "arenaArmorLevel",
                    "id": 13
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "bossArmorID",
                    "id": 14
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "bossArmorLevel",
                    "id": 15
                }
            ]
        },
        {
            "name": "RoleFriendInfo",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "applicationIdList",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "friendIdList",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "PlayerInfo",
                    "name": "playerInfoList",
                    "id": 3
                }
            ]
        },
        {
            "name": "FriendCount",
            "fields": [
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "applicationCount",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "friendCount",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_center_sc_newFriendApplication",
            "options": {
                "code": 7500
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "accountId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "applicationCount",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_center_sc_newFriend",
            "options": {
                "code": 7501
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "accountId",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "CenterFriendInfo",
                    "name": "friendInfo",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_center_sc_delNewFriend",
            "options": {
                "code": 7502
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "accountId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "deleteFriendId",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_cs_role_reconnect",
            "options": {
                "code": 8000,
                "async": 1
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "clientVersion",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "resVersion",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_sc_role_reconnect",
            "options": {
                "code": 8001
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "clientVersion",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "clientDownloadUrl",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "resVersion",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "resServerAddr",
                    "id": 5
                }
            ]
        },
        {
            "name": "cmd_cs_role_online",
            "options": {
                "code": 1,
                "async": 1
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "passport",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "platformId",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "deviceUid",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "device",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "deviceType",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "deviceToken",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "accountIdForGMLogin",
                    "id": 7
                }
            ]
        },
        {
            "name": "cmd_sc_role_online",
            "options": {
                "code": 2
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_role_heartBeat",
            "options": {
                "code": 3
            },
            "fields": []
        },
        {
            "name": "cmd_sc_role_heartBeat",
            "options": {
                "code": 4
            },
            "fields": []
        },
        {
            "name": "cmd_cs_role_signInfo",
            "options": {
                "code": 5
            },
            "fields": []
        },
        {
            "name": "cmd_sc_role_signInfo",
            "options": {
                "code": 6
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "SignInInfo",
                    "name": "signInfo",
                    "id": 2
                }
            ],
            "messages": [
                {
                    "name": "SignInInfo",
                    "fields": [
                        {
                            "rule": "required",
                            "type": "int32",
                            "name": "loginDays",
                            "id": 1
                        },
                        {
                            "rule": "required",
                            "type": "bool",
                            "name": "firstLogin",
                            "id": 2
                        },
                        {
                            "rule": "repeated",
                            "type": "Reward",
                            "name": "reward",
                            "id": 3
                        }
                    ]
                }
            ]
        },
        {
            "name": "cmd_sc_role_initProperty",
            "options": {
                "code": 7
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Pair",
                    "name": "values",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "Pair",
                    "name": "sundry",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_sc_role_updateProperty",
            "options": {
                "code": 8
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Pair",
                    "name": "values",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "Pair",
                    "name": "sundry",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_sc_role_initHero",
            "options": {
                "code": 9
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Hero",
                    "name": "allHero",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_role_updateHero",
            "options": {
                "code": 10
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Hero",
                    "name": "allHero",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_role_initHeroBattle",
            "options": {
                "code": 11
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "BattleArmy",
                    "name": "dungeonArmy",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "BattleArmy",
                    "name": "bossArmy",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "BattleArmy",
                    "name": "arenaArmy",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_sc_role_updateHeroBattle",
            "options": {
                "code": 12
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "BattleArmy",
                    "name": "dungeonArmy",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "BattleArmy",
                    "name": "bossArmy",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "BattleArmy",
                    "name": "arenaArmy",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_sc_role_initEquip",
            "options": {
                "code": 13
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Equip",
                    "name": "allEquip",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_role_updateEquip",
            "options": {
                "code": 14
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Equip",
                    "name": "allEquip",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_role_deleteEquip",
            "options": {
                "code": 15
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "deleteUidArray",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_role_initBattle",
            "options": {
                "code": 16
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "isFighting",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "FightRound.TYPE",
                    "name": "fightType",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "fightSeed",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_sc_role_initDungeon",
            "options": {
                "code": 17
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "completeHighestStageID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "currentStage",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "FightTeam",
                    "name": "team",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_sc_role_initInfo",
            "options": {
                "code": 18
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "accountId",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_role_updateGuide",
            "options": {
                "code": 19
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "progress",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_role_updateGuide",
            "options": {
                "code": 20
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_role_getGift",
            "options": {
                "code": 21
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "code",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_role_getGift",
            "options": {
                "code": 22
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "Reward",
                    "name": "reward",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_cs_role_finishGuideBattle",
            "options": {
                "code": 23
            },
            "fields": []
        },
        {
            "name": "cmd_sc_role_finishGuideBattle",
            "options": {
                "code": 24
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "Loot",
                    "name": "reward",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_cs_battle_fightStart",
            "options": {
                "code": 202
            },
            "fields": []
        },
        {
            "name": "cmd_sc_battle_fightStart",
            "options": {
                "code": 203
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "randomSeed",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "FightLoot",
                    "name": "fightLootArray",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "leftSlay",
                    "id": 4
                }
            ]
        },
        {
            "name": "cmd_cs_battle_fightFinish",
            "options": {
                "code": 204
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "result",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "totalRound",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "useSlayRound",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "restoreRound",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "specialRound",
                    "id": 5
                }
            ]
        },
        {
            "name": "cmd_sc_battle_fightFinish",
            "options": {
                "code": 205
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "hasFinishedWave",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "monsterGroupID",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "rightDeathCount",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "hasBossActive",
                    "id": 5
                }
            ]
        },
        {
            "name": "cmd_cs_battle_dungeonStart",
            "options": {
                "code": 206
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "stageID",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "FightTeam",
                    "name": "team",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_sc_battle_dungeonStart",
            "options": {
                "code": 207
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "stageID",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "FightTeam",
                    "name": "team",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "hasFinishedWave",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "monsterGroupID",
                    "id": 5
                },
                {
                    "rule": "repeated",
                    "type": "FightHero",
                    "name": "hireFriendList",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "rightDeathCount",
                    "id": 7
                }
            ]
        },
        {
            "name": "cmd_cs_battle_dungeonFinish",
            "options": {
                "code": 208
            },
            "fields": []
        },
        {
            "name": "cmd_sc_battle_dungeonFinish",
            "options": {
                "code": 209
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "exp",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "completeHighestStageID",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_cs_battle_abandonProcess",
            "options": {
                "code": 210
            },
            "fields": []
        },
        {
            "name": "cmd_sc_battle_abandonProcess",
            "options": {
                "code": 211
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_battle_recoverHp",
            "options": {
                "code": 212
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "heroUid",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "useResID",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_sc_battle_recoverHp",
            "options": {
                "code": 213
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_battle_restoreHealth",
            "options": {
                "code": 214
            },
            "fields": []
        },
        {
            "name": "cmd_sc_battle_restoreHealth",
            "options": {
                "code": 215
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_battle_useSpecial",
            "options": {
                "code": 216
            },
            "fields": []
        },
        {
            "name": "cmd_sc_battle_useSpecial",
            "options": {
                "code": 217
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                }
            ]
        },
        {
            "name": "City",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Area",
                    "name": "areas",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "Build",
                    "name": "builds",
                    "id": 2
                }
            ],
            "messages": [
                {
                    "name": "Area",
                    "fields": [
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "ID",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "bool",
                            "name": "open",
                            "id": 2,
                            "options": {
                                "default": false
                            }
                        },
                        {
                            "rule": "repeated",
                            "type": "sint32",
                            "name": "grid",
                            "id": 3
                        },
                        {
                            "rule": "optional",
                            "type": "uint32",
                            "name": "leftTime",
                            "id": 4,
                            "options": {
                                "default": 0
                            }
                        }
                    ]
                },
                {
                    "name": "Build",
                    "fields": [
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "uid",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "uint32",
                            "name": "ID",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "uint32",
                            "name": "direction",
                            "id": 3
                        },
                        {
                            "rule": "optional",
                            "type": "uint32",
                            "name": "level",
                            "id": 4
                        },
                        {
                            "rule": "optional",
                            "type": "uint32",
                            "name": "collectGold",
                            "id": 5
                        },
                        {
                            "rule": "optional",
                            "type": "uint32",
                            "name": "leftTime",
                            "id": 6
                        },
                        {
                            "rule": "optional",
                            "type": "Status",
                            "name": "status",
                            "id": 7
                        }
                    ],
                    "enums": [
                        {
                            "name": "Status",
                            "values": [
                                {
                                    "name": "NULL",
                                    "id": 0
                                },
                                {
                                    "name": "BUILDING",
                                    "id": 1
                                },
                                {
                                    "name": "FINISHED",
                                    "id": 2
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "name": "Consume",
            "fields": [],
            "messages": [
                {
                    "name": "Resource",
                    "fields": [
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "id",
                            "id": 1
                        },
                        {
                            "rule": "required",
                            "type": "int32",
                            "name": "left",
                            "id": 2
                        },
                        {
                            "rule": "required",
                            "type": "int32",
                            "name": "consume",
                            "id": 3
                        }
                    ]
                }
            ]
        },
        {
            "name": "cmd_cs_build_init",
            "options": {
                "code": 300
            },
            "fields": []
        },
        {
            "name": "cmd_sc_build_init",
            "options": {
                "code": 301
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "City.Area",
                    "name": "areas",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "City.Build",
                    "name": "builds",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_cs_build_openArea",
            "options": {
                "code": 302
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "ID",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_build_openArea",
            "options": {
                "code": 303
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "ID",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "leftTime",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_cs_build_updatePlace",
            "options": {
                "code": 304
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "City.Area",
                    "name": "areas",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "City.Build",
                    "name": "builds",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_sc_build_updatePlace",
            "options": {
                "code": 305
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_build_collectGold",
            "options": {
                "code": 306
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "uid",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_build_collectGold",
            "options": {
                "code": 307
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "uid",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "gold",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_cs_build_sellBuild",
            "options": {
                "code": 308
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "uid",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_build_sellBuild",
            "options": {
                "code": 309
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "uid",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "gold",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_cs_build_requestArea",
            "options": {
                "code": 310
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "areaIDArray",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_build_requestArea",
            "options": {
                "code": 311
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "City.Area",
                    "name": "areaArray",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_cs_build_upgradeBuild",
            "options": {
                "code": 312
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "uid",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_build_upgradeBuild",
            "options": {
                "code": 313
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "uid",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "level",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_cs_build_requestBuild",
            "options": {
                "code": 314
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "buildUidArray",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_build_requestBuild",
            "options": {
                "code": 315
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "City.Build",
                    "name": "buildArray",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_cs_build_accelerateBuilding",
            "options": {
                "code": 316
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "BuildType",
                    "name": "buildType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "id",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_sc_build_accelerateBuilding",
            "options": {
                "code": 317
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "BuildType",
                    "name": "buildType",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "id",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_cs_chance_openbox",
            "options": {
                "code": 400
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "boxId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "batch",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_sc_chance_openbox",
            "options": {
                "code": 401
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "Reward",
                    "name": "reward",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_cs_equip_doCraftTask",
            "options": {
                "code": 500
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "buildUid",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "craftID",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_sc_equip_doCraftTask",
            "options": {
                "code": 501
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "CraftTask",
                    "name": "craftTask",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_cs_equip_finishCraftTask",
            "options": {
                "code": 502
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "buildUid",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_equip_finishCraftTask",
            "options": {
                "code": 503
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "equipUid",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "buildUid",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_cs_equip_enhanceEquip",
            "options": {
                "code": 504
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "equipUidArray",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_equip_enhanceEquip",
            "options": {
                "code": 505
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "equipUid",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_cs_equip_accelerateCraftTask",
            "options": {
                "code": 506
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "buildUid",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_equip_accelerateCraftTask",
            "options": {
                "code": 507
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "buildUid",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_cs_equip_combineEquip",
            "options": {
                "code": 508
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "equipUidArray",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_equip_combineEquip",
            "options": {
                "code": 509
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "combineEquipUid",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_sc_equip_initCraftTask",
            "options": {
                "code": 510
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "CraftTask",
                    "name": "craftTasks",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_gm_useCommand",
            "options": {
                "code": 800
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "command",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_gm_useCommand",
            "options": {
                "code": 801
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_hero_setArmor",
            "options": {
                "code": 1000
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "type",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "HeroArmor",
                    "name": "heroArmor",
                    "id": 2
                }
            ],
            "messages": [
                {
                    "name": "HeroArmor",
                    "fields": [
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "heroUid",
                            "id": 1
                        },
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "armorUid",
                            "id": 2
                        }
                    ]
                }
            ]
        },
        {
            "name": "cmd_sc_hero_setArmor",
            "options": {
                "code": 1001
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "HeroArmor",
                    "name": "heroArmor",
                    "id": 2
                }
            ],
            "messages": [
                {
                    "name": "HeroArmor",
                    "fields": [
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "heroUid",
                            "id": 1
                        },
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "armorUid",
                            "id": 2
                        }
                    ]
                }
            ]
        },
        {
            "name": "cmd_cs_hero_setAppearance",
            "options": {
                "code": 1002
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "heroUid",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "name",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "hairType",
                    "id": 10
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "hairColor",
                    "id": 11
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "faceType",
                    "id": 12
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "skinColor",
                    "id": 13
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "cape",
                    "id": 14
                }
            ]
        },
        {
            "name": "cmd_sc_hero_setAppearance",
            "options": {
                "code": 1003
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_quest_readQuest",
            "options": {
                "code": 1100
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "ID",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_quest_readQuest",
            "options": {
                "code": 1101
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "ID",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_cs_quest_completeQuest",
            "options": {
                "code": 1102
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "ID",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_quest_completeQuest",
            "options": {
                "code": 1103
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "ID",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_sc_quest_initQuest",
            "options": {
                "code": 1104
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Quest",
                    "name": "quest",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_quest_updateQuest",
            "options": {
                "code": 1105
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "QuestUpdate",
                    "name": "questUpdate",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_achievement_equipTitle",
            "options": {
                "code": 1200
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "achievementID",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_achievement_equipTitle",
            "options": {
                "code": 1201
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_achievement_readAchievement",
            "options": {
                "code": 1202
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "achievementID",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_achievement_readAchievement",
            "options": {
                "code": 1203
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_achievement_initAchievement",
            "options": {
                "code": 1250
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "AchievementProgress",
                    "name": "achProgress",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_achievement_updateAchievement",
            "options": {
                "code": 1251
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "AchievementProgress",
                    "name": "achProgress",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_shop_init",
            "options": {
                "code": 1300
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "type",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_shop_init",
            "options": {
                "code": 1301
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "ShopItem",
                    "name": "shopItems",
                    "id": 1
                }
            ],
            "messages": [
                {
                    "name": "ShopItem",
                    "fields": []
                }
            ]
        },
        {
            "name": "cmd_cs_shop_buy",
            "options": {
                "code": 1302
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "id",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_shop_buy",
            "options": {
                "code": 1303
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "Reward",
                    "name": "reward",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "Pair",
                    "name": "propertyModify",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "id",
                    "id": 4
                }
            ]
        },
        {
            "name": "cmd_cs_shop_buyBuild",
            "options": {
                "code": 1304
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "id",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "City.Area",
                    "name": "area",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "direction",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_sc_shop_buyBuild",
            "options": {
                "code": 1305
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "City.Build",
                    "name": "build",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "City.Area",
                    "name": "area",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_sc_charge_info",
            "options": {
                "code": 1306
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "currency",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "price",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_cs_summonboss_startFight",
            "options": {
                "code": 1400
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "FightTeam",
                    "name": "team",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_summonboss_startFight",
            "options": {
                "code": 1401
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "randomSeed",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "FightLoot",
                    "name": "fightLootArray",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "leftSlay",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "FightHero",
                    "name": "hireFriendList",
                    "id": 5
                }
            ]
        },
        {
            "name": "cmd_cs_summonboss_finishFight",
            "options": {
                "code": 1402
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "result",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "totalRound",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "useSlayRound",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "restoreRound",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "specialRound",
                    "id": 5
                }
            ]
        },
        {
            "name": "cmd_sc_summonboss_finishFight",
            "options": {
                "code": 1403
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "bossID",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "bossLeftHp",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_cs_summonboss_summonBoss",
            "options": {
                "code": 1405
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "bossID",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_summonboss_summonBoss",
            "options": {
                "code": 1406
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "bossID",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "bossLeftHp",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_cs_summonboss_selectTeam",
            "options": {
                "code": 1407
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "FightTeam",
                    "name": "team",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_summonboss_selectTeam",
            "options": {
                "code": 1408
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "bossId",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "bossHp",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "FightHero",
                    "name": "hireFriendList",
                    "id": 4
                }
            ]
        },
        {
            "name": "cmd_sc_summonboss_initInfo",
            "options": {
                "code": 1450
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "bossList",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "bossID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "bossLeftHp",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "FightTeam",
                    "name": "team",
                    "id": 4
                }
            ]
        },
        {
            "name": "cmd_sc_summonboss_updateInfo",
            "options": {
                "code": 1451
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "newBossList",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_test_echo",
            "options": {
                "code": 1600
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "msg",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "id",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_sc_test_echo",
            "options": {
                "code": 1601
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "msg",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "id",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_cs_friend_initFriend",
            "options": {
                "code": 1800,
                "async": 1
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "isBackground",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_friend_initFriend",
            "options": {
                "code": 1801
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "friendCount",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "applicationCount",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "FriendInfo",
                    "name": "friendList",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "isBackground",
                    "id": 5
                }
            ]
        },
        {
            "name": "cmd_cs_friend_applyForFriend",
            "options": {
                "code": 1802,
                "async": 1
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "friendCode",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_friend_applyForFriend",
            "options": {
                "code": 1803
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_friend_handleApplication",
            "options": {
                "code": 1804,
                "async": 1
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "friendId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "TYPE",
                    "name": "type",
                    "id": 2
                }
            ],
            "enums": [
                {
                    "name": "TYPE",
                    "values": [
                        {
                            "name": "ACCEPT",
                            "id": 1
                        },
                        {
                            "name": "REJECT",
                            "id": 2
                        }
                    ]
                }
            ]
        },
        {
            "name": "cmd_sc_friend_handleApplication",
            "options": {
                "code": 1805
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_friend_seeProfile",
            "options": {
                "code": 1806,
                "async": 1
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "friendId",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_friend_seeProfile",
            "options": {
                "code": 1807
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "RoleProfile",
                    "name": "profile",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_cs_friend_delFriend",
            "options": {
                "code": 1810,
                "async": 1
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "friendId",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_friend_delFriend",
            "options": {
                "code": 1811
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_friend_gainReward",
            "options": {
                "code": 1812
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "ID",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_friend_gainReward",
            "options": {
                "code": 1813
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "ID",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_cs_friend_fetchPartFriend",
            "options": {
                "code": 1814
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "startIndex",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "size",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_sc_friend_fetchPartFriend",
            "options": {
                "code": 1815
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "FriendInfo",
                    "name": "friendList",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_cs_friend_startFight",
            "options": {
                "code": 1820,
                "async": 1
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "friendId",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_friend_startFight",
            "options": {
                "code": 1821
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "FightHero",
                    "name": "heros",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "randomSeed",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_cs_friend_finishFight",
            "options": {
                "code": 1822,
                "async": 1
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "result",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "totalRound",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "useSlayRound",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_sc_friend_finishFight",
            "options": {
                "code": 1823
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "earned",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "winTimesScore",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "revengeScore",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "totalScore",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "winTimesRewardGet",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "scoreRewardGet",
                    "id": 7
                }
            ]
        },
        {
            "name": "cmd_cs_friend_initHireInfo",
            "options": {
                "code": 1824
            },
            "fields": []
        },
        {
            "name": "cmd_sc_friend_initHireInfo",
            "options": {
                "code": 1825
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "haveHiredCount",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "coolDown",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "allHiredFriendList",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_sc_friend_newFriendApplication",
            "options": {
                "code": 1850
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "applicationCount",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_friend_count",
            "options": {
                "code": 1851
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "friendCount",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "applicationCount",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_sc_friend_updateHireInfo",
            "options": {
                "code": 1853
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "haveHiredCount",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "coolDown",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "newHiredFriendList",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_sc_friend_initInfo",
            "options": {
                "code": 1854
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "referFriendCount",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "gainedIDList",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_sc_friend_updateInfo",
            "options": {
                "code": 1855
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "referFriendCount",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_arena_initInfo",
            "options": {
                "code": 1900,
                "async": 1
            },
            "fields": []
        },
        {
            "name": "cmd_sc_arena_initInfo",
            "options": {
                "code": 1901
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "score",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "rank",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "winStreak",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "streakEndTime",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "recoverEnergyTime",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "topPlayerName",
                    "id": 7
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "topFriendName",
                    "id": 8
                }
            ]
        },
        {
            "name": "cmd_cs_arena_initBattleList",
            "options": {
                "code": 1902,
                "async": 1
            },
            "fields": []
        },
        {
            "name": "cmd_sc_arena_initBattleList",
            "options": {
                "code": 1903
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "OpponentInfo",
                    "name": "opponentList",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_cs_arena_selectTeam",
            "options": {
                "code": 1904
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "FightTeam",
                    "name": "team",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_arena_selectTeam",
            "options": {
                "code": 1905
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_arena_startFight",
            "options": {
                "code": 1906,
                "async": 1
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "fightType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "opponentId",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_sc_arena_startFight",
            "options": {
                "code": 1907
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "randomSeed",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "FightHero",
                    "name": "heros",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "fightType",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "arenaEnergyNextTime",
                    "id": 5
                }
            ]
        },
        {
            "name": "cmd_cs_arena_finishFight",
            "options": {
                "code": 1908,
                "async": 1
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "result",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "totalRound",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "useSlayRound",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_sc_arena_finishFight",
            "options": {
                "code": 1909
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "earned",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "winTimesScore",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "revengeScore",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "totalScore",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "winTimesRewardGet",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "scoreRewardGet",
                    "id": 7
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "winStreak",
                    "id": 8
                }
            ]
        },
        {
            "name": "cmd_cs_arena_rankList",
            "options": {
                "code": 1910,
                "async": 1
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "rankType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "page",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "group",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_sc_arena_rankList",
            "options": {
                "code": 1911
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "rankType",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "LeaderboardItem",
                    "name": "roles",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "currentPage",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "totalPage",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "currentGroup",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "totalGroup",
                    "id": 7
                }
            ]
        },
        {
            "name": "cmd_sc_arena_onlineInfo",
            "options": {
                "code": 1950
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "tournamentId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "tournamentLeftTime",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "LastTopArenaRole",
                    "name": "topRoles",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "totalWinCount",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "totalLossCount",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "revengeWins",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "FightTeam",
                    "name": "team",
                    "id": 7
                },
                {
                    "rule": "repeated",
                    "type": "TournamentHistory",
                    "name": "tournamentHistory",
                    "id": 8
                },
                {
                    "rule": "optional",
                    "type": "LastTournamentReward",
                    "name": "lastTournamentReward",
                    "id": 9
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "arenaEnergyNextTime",
                    "id": 10
                }
            ]
        },
        {
            "name": "cmd_sc_arena_updateTournamentInfo",
            "options": {
                "code": 1951
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "tournamentId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "TOURNAMENT_PROGRESS",
                    "name": "tournamentProgress",
                    "id": 2
                }
            ],
            "enums": [
                {
                    "name": "TOURNAMENT_PROGRESS",
                    "values": [
                        {
                            "name": "NOT_START",
                            "id": 0
                        },
                        {
                            "name": "PVP_START",
                            "id": 1
                        },
                        {
                            "name": "RESULT_SORTING",
                            "id": 2
                        },
                        {
                            "name": "TOURNAMENT_END",
                            "id": 3
                        }
                    ]
                }
            ]
        },
        {
            "name": "Activity",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "activityId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "ACTIVITY_TYPE",
                    "name": "activityType",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "leftTime",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "lastUpdateTime",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "resourceVersion",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "title",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "endTime",
                    "id": 7
                }
            ]
        },
        {
            "name": "cmd_sc_activity_initInfo",
            "options": {
                "code": 2000
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Activity",
                    "name": "activities",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_activity_updateActivity",
            "options": {
                "code": 2001
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Activity",
                    "name": "updateActivities",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_sc_activity_deleteActivity",
            "options": {
                "code": 2002
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "deleteActivitiesId",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_activity_queryInfo",
            "options": {
                "code": 2010
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "activityIdList",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_activity_queryInfo",
            "options": {
                "code": 2011
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Activity",
                    "name": "activities",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "activityIdList",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_sc_activity_loginGift",
            "options": {
                "code": 2050
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "LoginGift",
                    "name": "giftList",
                    "id": 1
                }
            ],
            "messages": [
                {
                    "name": "LoginGift",
                    "fields": [
                        {
                            "rule": "required",
                            "type": "uint32",
                            "name": "activityId",
                            "id": 1
                        },
                        {
                            "rule": "repeated",
                            "type": "Reward",
                            "name": "reward",
                            "id": 2
                        }
                    ]
                }
            ]
        },
        {
            "name": "ConsumeInfo",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "requireCount",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "resID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "resCount",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_sc_activity_diamondConsumeReward",
            "options": {
                "code": 2051
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "ConsumeInfo",
                    "name": "rewardList",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_cs_activity_initDiamondConsume",
            "options": {
                "code": 2052
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "activityId",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_activity_initDiamondConsume",
            "options": {
                "code": 2053
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "totalCount",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "ConsumeInfo",
                    "name": "consumeList",
                    "id": 3
                }
            ]
        },
        {
            "name": "ShadowChest",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "chestCount",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "price",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "Reward",
                    "name": "reward",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_cs_activity_initShadowChest",
            "options": {
                "code": 2054
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "activityId",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_activity_initShadowChest",
            "options": {
                "code": 2055
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "activityId",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "ShadowChest",
                    "name": "chestList",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "showRewardList",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "chanceStar",
                    "id": 5
                }
            ]
        },
        {
            "name": "cmd_cs_activity_openShadowChest",
            "options": {
                "code": 2056
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "activityId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "openCount",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_sc_activity_openShadowChest",
            "options": {
                "code": 2057
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "activityId",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "Reward",
                    "name": "reward",
                    "id": 3
                }
            ]
        },
        {
            "name": "FlourishingChest",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "chestId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "chance",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "icon",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "lootCount",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "price",
                    "id": 5
                }
            ]
        },
        {
            "name": "cmd_cs_activity_initFlourishingChest",
            "options": {
                "code": 2058
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "activityId",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_activity_initFlourishingChest",
            "options": {
                "code": 2059
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "activityId",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "FlourishingChest",
                    "name": "chestList",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "showRewardList",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "chanceStar",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "haveBoughtCount",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "chanceBonusId",
                    "id": 7
                }
            ]
        },
        {
            "name": "cmd_cs_activity_openFlourishingChest",
            "options": {
                "code": 2060
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "activityId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "chestId",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_sc_activity_openFlourishingChest",
            "options": {
                "code": 2061
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "activityId",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "Reward",
                    "name": "reward",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "haveBoughtCount",
                    "id": 4
                }
            ]
        },
        {
            "name": "CraftRequire",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "armorID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "requireCount",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_cs_activity_initCraft",
            "options": {
                "code": 2062
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "activityId",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_activity_initCraft",
            "options": {
                "code": 2063
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "activityId",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "CraftRequire",
                    "name": "craftList",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "Reward",
                    "name": "reward",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "CRAFT_REWARD_STATUS",
                    "name": "redeemStatus",
                    "id": 5
                }
            ],
            "enums": [
                {
                    "name": "CRAFT_REWARD_STATUS",
                    "values": [
                        {
                            "name": "NOT_EARN",
                            "id": 0
                        },
                        {
                            "name": "HAS_EARN",
                            "id": 1
                        }
                    ]
                }
            ]
        },
        {
            "name": "cmd_cs_activity_redeemCraftReward",
            "options": {
                "code": 2064
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "activityId",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_activity_redeemCraftReward",
            "options": {
                "code": 2065
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "activityId",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_cs_boss_initInfo",
            "options": {
                "code": 2100,
                "async": 1
            },
            "fields": []
        },
        {
            "name": "cmd_sc_boss_initInfo",
            "options": {
                "code": 2101
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "rank",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "score",
                    "id": 3
                }
            ]
        },
        {
            "name": "cmd_cs_boss_startFight",
            "options": {
                "code": 2102
            },
            "fields": []
        },
        {
            "name": "cmd_sc_boss_startFight",
            "options": {
                "code": 2103
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "randomSeed",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "FightLoot",
                    "name": "fightLootArray",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "leftSlay",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "bossEnergyNextTime",
                    "id": 5
                }
            ]
        },
        {
            "name": "cmd_cs_boss_finishFight",
            "options": {
                "code": 2104,
                "async": 1
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "result",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "totalRound",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "useSlayRound",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "restoreRound",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "specialRound",
                    "id": 5
                }
            ]
        },
        {
            "name": "cmd_sc_boss_finishFight",
            "options": {
                "code": 2105
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "damageDone",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "totalDamage",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "damageRank",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "killCount",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "milestoneRewardID",
                    "id": 6
                }
            ]
        },
        {
            "name": "cmd_cs_boss_rankList",
            "options": {
                "code": 2106,
                "async": 1
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "page",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_boss_rankList",
            "options": {
                "code": 2107
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "LeaderboardItem",
                    "name": "roles",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "currentPage",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "totalPage",
                    "id": 4
                }
            ]
        },
        {
            "name": "cmd_cs_boss_selectTeam",
            "options": {
                "code": 2108
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "FightTeam",
                    "name": "team",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_boss_selectTeam",
            "options": {
                "code": 2109
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "FightHero",
                    "name": "hireFriendList",
                    "id": 6
                }
            ]
        },
        {
            "name": "cmd_sc_boss_initBossInfo",
            "options": {
                "code": 2151
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "BOSS_PROGRESS",
                    "name": "bossProgress",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "bossID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "bossLevel",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "isActive",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "bossLeftActiveTime",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "bossTotalDuration",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "leftHealth",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "killCount",
                    "id": 8
                },
                {
                    "rule": "optional",
                    "type": "LastBossReward",
                    "name": "lastBossReward",
                    "id": 9
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "bossEnergyNextTime",
                    "id": 10
                },
                {
                    "rule": "optional",
                    "type": "FightTeam",
                    "name": "team",
                    "id": 11
                }
            ]
        },
        {
            "name": "cmd_sc_boss_initKillList",
            "options": {
                "code": 2152
            },
            "fields": [
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "bossList",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_boss_newBossInfo",
            "options": {
                "code": 2153
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "bossID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "BOSS_PROGRESS",
                    "name": "bossProgress",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "bossLevel",
                    "id": 3,
                    "options": {
                        "default": 1
                    }
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "isActive",
                    "id": 4,
                    "options": {
                        "default": true
                    }
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "bossLeftActiveTime",
                    "id": 5,
                    "options": {
                        "default": 0
                    }
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "bossTotalDuration",
                    "id": 6,
                    "options": {
                        "default": 0
                    }
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "leftHealth",
                    "id": 7
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "killCount",
                    "id": 8,
                    "options": {
                        "default": 0
                    }
                },
                {
                    "rule": "optional",
                    "type": "LastBossReward",
                    "name": "lastBossReward",
                    "id": 9
                }
            ]
        },
        {
            "name": "Mail",
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "uid",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "title",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "sender",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "sendTime",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "content",
                    "id": 5
                },
                {
                    "rule": "repeated",
                    "type": "Reward",
                    "name": "attachment",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "MailStatus",
                    "name": "status",
                    "id": 7
                }
            ],
            "enums": [
                {
                    "name": "MailStatus",
                    "values": [
                        {
                            "name": "NULL",
                            "id": 0
                        },
                        {
                            "name": "NEW",
                            "id": 1
                        },
                        {
                            "name": "READ",
                            "id": 2
                        },
                        {
                            "name": "RECEIVED",
                            "id": 3
                        }
                    ]
                }
            ]
        },
        {
            "name": "cmd_cs_mail_initList",
            "options": {
                "code": 2200
            },
            "fields": []
        },
        {
            "name": "cmd_sc_mail_initList",
            "options": {
                "code": 2201
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "totalPage",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "Mail",
                    "name": "mailList",
                    "id": 4
                }
            ]
        },
        {
            "name": "cmd_cs_mail_queryPage",
            "options": {
                "code": 2202
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "page",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_mail_queryPage",
            "options": {
                "code": 2203
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "Mail",
                    "name": "mailList",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_cs_mail_receiveAttachment",
            "options": {
                "code": 2204
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "mailUid",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_mail_receiveAttachment",
            "options": {
                "code": 2205
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "mailUid",
                    "id": 2
                }
            ]
        },
        {
            "name": "cmd_cs_mail_readMail",
            "options": {
                "code": 2206
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "mailUid",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_mail_readMail",
            "options": {
                "code": 2207
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "Error",
                    "name": "error",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_mail_onlineInfo",
            "options": {
                "code": 2250
            },
            "fields": [
                {
                    "rule": "required",
                    "type": "uint32",
                    "name": "newMailCount",
                    "id": 1
                }
            ]
        },
        {
            "name": "cmd_sc_mail_updateInfo",
            "options": {
                "code": 2251
            },
            "fields": [
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "newMailCount",
                    "id": 1
                }
            ]
        }
    ],
    "enums": [
        {
            "name": "BuildType",
            "values": [
                {
                    "name": "AREA",
                    "id": 1
                },
                {
                    "name": "BUILD",
                    "id": 2
                }
            ]
        },
        {
            "name": "Sundry",
            "values": [
                {
                    "name": "ACHIEVEMENT_TITLE",
                    "id": 1
                }
            ]
        },
        {
            "name": "ACTIVITY_TYPE",
            "values": [
                {
                    "name": "NULL",
                    "id": 0
                },
                {
                    "name": "LOGIN_GIFT",
                    "id": 1
                },
                {
                    "name": "DIAMOND_CONSUME",
                    "id": 2
                },
                {
                    "name": "SHADOW_CHEST",
                    "id": 3
                },
                {
                    "name": "FLOURISHING_CHEST",
                    "id": 4
                },
                {
                    "name": "CRAFT",
                    "id": 5
                },
                {
                    "name": "LIMIT_QUEST",
                    "id": 6
                }
            ]
        },
        {
            "name": "BOSS_PROGRESS",
            "values": [
                {
                    "name": "NOT_START",
                    "id": 0
                },
                {
                    "name": "START",
                    "id": 1
                },
                {
                    "name": "RESULT_SORTING",
                    "id": 2
                },
                {
                    "name": "BOSS_END",
                    "id": 3
                }
            ]
        }
    ]
}).build();