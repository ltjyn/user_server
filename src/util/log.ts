import log4js = require('log4js');
import sourceMap = require('source-map-support');
import util = require('util');
import mkdirp = require('mkdirp');
import index = require('../../index');

class Log {
    private static m_logger:log4js.Logger;

    public static init(dirName:any):void {
        var logDir =  index.sourceRoot + '/logs/' + dirName;
        mkdirp.sync(logDir);

        log4js.configure({
            appenders: [
                {
                    type: "dateFile",
                    filename: logDir + "/gameserver.log",
                    pattern: ".yyyy-MM-dd",
                    layout: {
                        type: "pattern",
                        pattern: "%d{yyyy-MM-dd hh:mm:ss},%x{level},%x{line},%x{roleId},%x{content}",
                        tokens: {
                            line: () => {
                                return Log.getLineInfo();
                            },
                            roleId: (log) => {
                                return log.data[0];
                            },
                            /* 此处 pattern 中使用 %x{content} 代替 %m 是为了支持 roleId */
                            content: (log) => {
                                return util.format.apply(util, log.data.slice(1));
                            },
                            level: (log) => {
                                switch (log.level.level) {
                                    case 5000   :
                                        return 1;   /* trace */
                                    case 10000  :
                                        return 1;   /* debug */
                                    case 20000  :
                                        return 2;   /* info */
                                    case 30000  :
                                        return 3;   /* warning */
                                    case 40000  :
                                        return 4;   /* error */
                                    case 50000  :
                                        return 4;   /* fatal */
                                }
                                return 0;
                            }

                        }
                    },
                    category: ["default"]
                },
                {
                    type: "console",
                    layout: {
                        type: "pattern",
                        /* 多加了颜色 */
                        pattern: "%[%d{yyyy-MM-dd hh:mm:ss},%p,%x{line},%x{roleId},%x{content}%]",
                        tokens: {
                            line: (log) => {
                                return Log.getLineInfo();
                            },
                            roleId: (log) => {
                                return log.data[0];
                            },
                            /* 此处 pattern 中使用 %x{content} 代替 %m 是为了支持 roleId */
                            content: (log) => {
                                return util.format.apply(util, log.data.slice(1));
                            }
                        }
                    },
                    category: ["default"]
                }
            ]
            //replaceConsole: true
        });
        this.m_logger = log4js.getLogger('default');
        this.m_logger.setLevel(log4js.levels.INFO);
    }

    private static getLineInfo() {
        //var oldLimit = Error['stackTraceLimit'];
        //Error['stackTraceLimit'] = Infinity;

        var dummyObject = {};
        var v8Handler = Error['prepareStackTrace'];
        Error['prepareStackTrace'] = (t, v8StackTrace:any[]) => {
            /* 使用9是因为要跳过 log4j 本身的堆栈 */
            return [sourceMap.wrapCallSite(v8StackTrace[9])];
        };
        Error['captureStackTrace'](dummyObject, arguments.callee);

        var stack:any = dummyObject['stack'][0];
        Error['prepareStackTrace'] = v8Handler;
        //Error['stackTraceLimit'] = oldLimit;

        return /([^\/]*)$/.exec(stack.getFileName())[1] + ":" + stack.getFunctionName() + ":" + stack.getLineNumber();
        // 对于匿名函数 getFunctionName 返回null
        //return stack.getFileName() + ":" + stack.getFunctionName() + ":" + stack.getLineNumber();
    }

    // user
    public static uInfo(roleId:number, message:string, ...args:any[]):void {
        this.m_logger.info.apply(this.m_logger, [].slice.call(arguments));
    }

    public static uWarn(roleId:number, message:string, ...args:any[]):void {
        this.m_logger.warn.apply(this.m_logger, [].slice.call(arguments));
    }

    public static uError(roleId:number, message:string, ...args:any[]):void {
        this.m_logger.error.apply(this.m_logger, [].slice.call(arguments));
    }

    // system
    public static sDebug(message:string, ...args:any[]):void {
        this.m_logger.debug.apply(this.m_logger, [0].concat([].slice.call(arguments)));
    }

    public static sInfo(message:string, ...args:any[]):void {
        this.m_logger.info.apply(this.m_logger, [0].concat([].slice.call(arguments)));
    }

    public static sWarn(message:string, ...args:any[]):void {
        this.m_logger.warn.apply(this.m_logger, [0].concat([].slice.call(arguments)));
    }

    public static sError(message:string, ...args:any[]):void {
        this.m_logger.error.apply(this.m_logger, [0].concat([].slice.call(arguments)));
    }

    public static getLogger():any {
        var _format = ':remote-addr :method :url :http-version :status :response-time ms';
        return log4js.connectLogger(this.m_logger, {format: _format});
    }
}

export = Log;