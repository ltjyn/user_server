var ErrorCode = {
    NO_ERROR: 0,                              // 正常
    COMMON: {
        SYSTEM_ERROR : 10000,               // 系统错误
        CTL_NOT_FOUND : 10001,              // 控制器找不到
        CTL_FUNC_NOT_FOUND : 10002,         // 控制器方法找不到
        CONFIG_NOT_FOUND: 10003,            // 配置未找到
        JSON_PARSE_ERROR: 10004,            // JSON反序列化错误
        HTTP_NO_RESPONSE: 10005,            // http无响应
        REQ_FREQUENT: 10006,                // 访问过于频繁
        UID_MISSING: 10007,                 // uid missing
        TOKEN_MISSING: 10008,               // token missing
        CTL_NOT_INTERCEPT: 10009,           // 不需要拦截
        ROLE_NOT_EXIST: 10010,              // 角色不存在
        SERVER_NOT_OPEN: 10011              // 服务器维护中
    },
    REDIS: {
        ERROR: 11000,
        SELECT_ERROR: 11001,
        GET_ERROR: 11002,
        SET_ERROR: 11003,
        EXPIRE_ERROR: 11004,
        INCR_ERROR: 11005,
        DEL_ERROR: 11006,
        HSET_ERROR: 11007,
        HMSET_ERROR: 11008,
        HMGET_ERROR: 11009,
        HINCRBY_ERROR: 11010,
        ZSCORE_ERROR: 110011,
        GETRANGE_ERROR: 11012,
        HGETALL_ERROR: 11013,
        ZADD_ERROR: 11014,
        ZREVRANGE_ERROR: 11015,
        KEYS_ERROR: 11016
    },
    MYSQL: {
        GET_CONN_ERROR: 12001,
        INSERT_ERROR: 12002,
        DEL_ERROR: 12003,
        UPDATE_ERROR: 12004,
        SELECT_ERROR: 12005,
        EXEC_ERROR: 12006,
    },
    GAME: {
        'SUCC' : 0, //操作成功, 萌萌哒~
        'SYS_ERR' : 1, //出了一点小状况
        'PARAM_ERR' : 2, //缺少必要参数
        'PLAYER_NOT_EXIST' : 3, //长时间没有登录，请重新登录
        'PARAM_INVALID' : 4, //非法参数
        'SKILL_NOT_EXIST' : 5, //技能不存在
        'SKILL_REACH_MAX_LEVEL' : 6, //技能已达到最高等级
        'NOT_ENOUGH_YXB' : 7, //金币不足
        'EQUIP_NOT_EXIST' : 8, //装备不存在
        'EQUIP_REACH_MAX_LEVEL' : 9, //装备已达到最高等级
        'NOT_ENOUGH_GOLD' : 10, //钻石不足
        'NOT_ENOUGH_SKILL_POINT' : 11, //技能点数不足
        'CAN_NOT_USE_GOLD' : 12, //不能使用钻石升级
        'NOT_MEET_MONSTER' : 13, //还没有遇到小怪兽哦
        'ALREADY_GET_FOCUS_PRIZE' : 14, //已经领取过关注奖励了哦
        'NOT_FOCUS' : 15, //您还未关注公众号哦
        'ACH_NOT_EXIST' : 16, //成就不存在
        'ALREADY_GET_ACH' : 17, //已经领取成就
        'ACH_NOT_REACH' : 18, //还未达到领取成就条件
        'SHARE_NOT_COOL_DOWN' : 19, //分享还未达到冷却时间
        'SHARE_EXCEED_DAILY_LIMIT' : 20, //分享达到每日上限
        'BUY_EXCEED_MAX_LIMIT' : 21, //购买达到上限
        'PRODUCT_NOT_EXIST' : 22, //商品不存在
        'ITEM_NOT_EXIST' : 23, // 物品不存在
        'NOT_ENOUGH_ITEM' : 24, // 物品不足
        'BATTLE_TOO_FAST' : 25, // 战斗太快
        'SOLDIER_NOT_EXIST' : 26, // 士兵不存在
        'FLOOR_UNLOCK' : 27, // 该层未解锁
        'BATTLE_FIELD_NOT_EXIST' : 28, // 登录状态已失效，请重新登录
        'DEMON_CLICK_NOT_COOL_DOWN' : 29, // 妖精召唤正在使用中
        'CANNOT_USE_SKILL' : 30, // 不能使用该技能
        'BATTLE_INVALID' : 31, // 战斗数据非法
        'SKILL_NOT_COOL_DOWN' : 32, // 技能未冷却
        'SKILL_EMPTY_LEVEL' : 33, // 技能还没升级过
        'SKILL_CONDITION_NOT_FIT' : 34, // 技能未达到升级条件
        'ALIVE_TOO_FAST' : 35, // 验证战斗太快
        'JUMP_CONDITION_NOT_FIT' : 36, // 解锁条件未满足
        'CAN_NOT_JUMP_BEFORE' : 37,// 已经解锁了之前的关卡
        'SKILL_ALL_COOL_DOWN' : 38,// 技能都已经冷却无需再购买
        'ALREADY_FRIENDS' : 39,// "你们已经是好友了",
        'APPLY_SUCCESS' : 40,// "申请成功",
        'ALREADY_APPLY' : 41,// "已经申请过了",
        'NOT_ENOUGH_YXB_SHOW' : 42,//: "金币不足",
        'ALREADY_AWARD_CDKEY' : 43,//: "礼包已经领取过了",
        'ALREADY_AWARD_MONTH_CARD' : 44,//: "月卡奖励已经领取了"
        'CDKEY_NOT_EXIST' : 45,//: "CDKEY不存在",
        'NO_PAY' : 100, // 您还没有充值过哦
        'ALREADY_GET_FIRST_PAY' : 101, // 已经领取过首充礼包
    }
};

export = ErrorCode;