import request = require('request');
import Enum = require('./enum')
import ErrorCode = require('./error_code');

/**
 * @example: httpGet('http://10.1.1.156/test?m=redis',function(error,body){},'json')
 *      获取http://10.1.1.156/test?cmd=redis返回的结果并解析为json对象
 */
export function httpGet(url:string, callback:(err, data)=>void, dataType:Enum.HTTP_RES_DATA_TYPE, timeout:number) {
    var options = {'url': url};
    typeof(timeout) != 'undefined' ? options['timeout'] = timeout : '';
    request.get(options, (error, response, body) => {
        var result = parseHttpResBody(error, response, body, dataType);
        return callback(result.error, result.data);
    });
}

/**
 * http POST请求
 * 比httpGet函数多了个form表单参数(Json对象)，其他参数参考httpGet
 * @example:
 */
export function httpPost(url:string, form:{[key:string]:any}, callback:(err, data)=>void, dataType:Enum.HTTP_RES_DATA_TYPE, timeout:number) {
    var options = {'url': url, 'form': form};
    typeof(timeout) != 'undefined' ? options['timeout'] = timeout : '';

    request.post(options, (error, response, body) => {
        var result = parseHttpResBody(error, response, body, dataType);
        return callback(result.error, result.data);
    });
}

//http get/post请求，返回包体解析函数
function parseHttpResBody(error:any, response:any, body:string, dataType:Enum.HTTP_RES_DATA_TYPE) {
    if (error) {
        //TODO:根据error.code是否为ETIMEDOUT可判断请求或响应是否超时
        //如果需要可在这里增加判断是否超时的错误码
        return {'error': ErrorCode.COMMON.HTTP_NO_RESPONSE, 'data': null};
    }
    if (response.statusCode == 200) {
        if (body) {
            if (dataType == Enum.HTTP_RES_DATA_TYPE.JSON) { //body返回的结果为json字符串
                try {
                    body = body.replace("\n", '');
                    var data = JSON.parse(body);
                    return {'error': 0, 'data': data};
                } catch (e) {
                    //TODO:json字符串解析为json对象失败，可能字符串中含有特殊编码字符
                    return {'error': ErrorCode.COMMON.JSON_PARSE_ERROR, 'data': null};
                }
            } else {
                return {'error': 0, 'data': null};
            }
        } else {
            return {'error': 0, 'data': null};
        }
    }
}