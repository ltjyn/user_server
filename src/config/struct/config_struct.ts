//********** header **********//
import fs = require('fs');
import CustomError = require('../../util/errors');
import ERRC = require('../../util/error_code');


//********** body **********//
// achievementdb
export class achievementDB {
	ID:any;				//ID
	type:any;			//类型
	number:any;			//序号
	count:any;			//内容数量
	item:any;			//奖励道具
	item_count:any;		//道具数量
	currency_type:any;	//奖励类型
	currency_num:any;	//奖励
	dec:any;			//描述
	constructor(data) {
		this.ID = data.ID;
		this.type = data.type;
		this.number = data.number;
		this.count = data.count;
		this.item = data.item;
		this.item_count = data.item_count;
		this.currency_type = data.currency_type;
		this.currency_num = data.currency_num;
		this.dec = data.dec;
	}
}
class achievementDBMgr {
    achievementDBConfig : {[ID:number]: achievementDB} = {};
    constructor(data) {
        this.achievementDBConfig = {};
        Object.keys(data).forEach((key) => {
            this.achievementDBConfig[data[key].ID] = new achievementDB(data[key]);
        });
    }
    public get(ID:number):achievementDB {
        var config = this.achievementDBConfig[ID];
        if (!config) {
            throw new CustomError.UserError(ERRC.COMMON.CONFIG_NOT_FOUND, {
                msg: 'COMMON.CONFIG_NOT_FOUND, achievementdb, ID=' + ID
            })
        }
        return config;
    }
    public all():{[ID:number]: achievementDB} {
        return this.achievementDBConfig;
    }
}

// boss_skilldb
export class boss_skillDB {
	ID:any;		//ID
	name:any;	//boss技能名
	type:any;	//作用类型
	lv:any;		//技能等级
	target:any;	//作用路线
	count:any;	//加成量
	dec:any;	//描述
	icon:any;	//图标
	constructor(data) {
		this.ID = data.ID;
		this.name = data.name;
		this.type = data.type;
		this.lv = data.lv;
		this.target = data.target;
		this.count = data.count;
		this.dec = data.dec;
		this.icon = data.icon;
	}
}
class boss_skillDBMgr {
    boss_skillDBConfig : {[ID:number]: boss_skillDB} = {};
    constructor(data) {
        this.boss_skillDBConfig = {};
        Object.keys(data).forEach((key) => {
            this.boss_skillDBConfig[data[key].ID] = new boss_skillDB(data[key]);
        });
    }
    public get(ID:number):boss_skillDB {
        var config = this.boss_skillDBConfig[ID];
        if (!config) {
            throw new CustomError.UserError(ERRC.COMMON.CONFIG_NOT_FOUND, {
                msg: 'COMMON.CONFIG_NOT_FOUND, boss_skilldb, ID=' + ID
            })
        }
        return config;
    }
    public all():{[ID:number]: boss_skillDB} {
        return this.boss_skillDBConfig;
    }
}

// herodb
export class heroDB {
	ID:any;			//ID
	res:any;		//资源id
	name:any;		//名字
	type:any;		//类型
	hp:any;			//生命
	attack:any;		//攻击
	lv_up_gold:any;	//升级系数
	nature_num:any;	//钻石解锁费用
	speed:any;		//移速
	max_speed:any;	//最大移速
	time:any;		//出战时间
	up_time:any;	//升级提速
	max_time:any;	//最快出战时间
	pos:any;		//上中下路标记
	constructor(data) {
		this.ID = data.ID;
		this.res = data.res;
		this.name = data.name;
		this.type = data.type;
		this.hp = data.hp;
		this.attack = data.attack;
		this.lv_up_gold = data.lv_up_gold;
		this.nature_num = data.nature_num;
		this.speed = data.speed;
		this.max_speed = data.max_speed;
		this.time = data.time;
		this.up_time = data.up_time;
		this.max_time = data.max_time;
		this.pos = data.pos;
	}
}
class heroDBMgr {
    heroDBConfig : {[ID:number]: heroDB} = {};
    constructor(data) {
        this.heroDBConfig = {};
        Object.keys(data).forEach((key) => {
            this.heroDBConfig[data[key].ID] = new heroDB(data[key]);
        });
    }
    public get(ID:number):heroDB {
        var config = this.heroDBConfig[ID];
        if (!config) {
            throw new CustomError.UserError(ERRC.COMMON.CONFIG_NOT_FOUND, {
                msg: 'COMMON.CONFIG_NOT_FOUND, herodb, ID=' + ID
            })
        }
        return config;
    }
    public all():{[ID:number]: heroDB} {
        return this.heroDBConfig;
    }
}

// hero_basicdb
export class hero_basicDB {
	ID:any;			//ID
	lv:any;			//等级
	hp:any;			//生命
	attack:any;		//攻击
	lv_up_gold:any;	//升级金币
	constructor(data) {
		this.ID = data.ID;
		this.lv = data.lv;
		this.hp = data.hp;
		this.attack = data.attack;
		this.lv_up_gold = data.lv_up_gold;
	}
}
class hero_basicDBMgr {
    hero_basicDBConfig : {[ID:number]: hero_basicDB} = {};
    constructor(data) {
        this.hero_basicDBConfig = {};
        Object.keys(data).forEach((key) => {
            this.hero_basicDBConfig[data[key].ID] = new hero_basicDB(data[key]);
        });
    }
    public get(ID:number):hero_basicDB {
        var config = this.hero_basicDBConfig[ID];
        if (!config) {
            throw new CustomError.UserError(ERRC.COMMON.CONFIG_NOT_FOUND, {
                msg: 'COMMON.CONFIG_NOT_FOUND, hero_basicdb, ID=' + ID
            })
        }
        return config;
    }
    public all():{[ID:number]: hero_basicDB} {
        return this.hero_basicDBConfig;
    }
}

// itemdb
export class itemDB {
	ID:any;			//ID
	res:any;		//资源od
	name:any;		//道具名字
	lv:any;			//初始等级
	max_lv:any;		//最大等级
	quarty:any;		//品质
	type:any;		//类型
	count:any;		//基础加成
	lv_up:any;		//升级加成
	dec:any;		//描述
	up_skill:any;	//改变技能
	up_hero:any;	//改变英雄
	upSoldier:any;	//改变士兵
	upMonster:any;	//改变怪物
	constructor(data) {
		this.ID = data.ID;
		this.res = data.res;
		this.name = data.name;
		this.lv = data.lv;
		this.max_lv = data.max_lv;
		this.quarty = data.quarty;
		this.type = data.type;
		this.count = data.count;
		this.lv_up = data.lv_up;
		this.dec = data.dec;
		this.up_skill = data.up_skill;
		this.up_hero = data.up_hero;
		this.upSoldier = data.upSoldier;
		this.upMonster = data.upMonster;
	}
}
class itemDBMgr {
    itemDBConfig : {[ID:number]: itemDB} = {};
    constructor(data) {
        this.itemDBConfig = {};
        Object.keys(data).forEach((key) => {
            this.itemDBConfig[data[key].ID] = new itemDB(data[key]);
        });
    }
    public get(ID:number):itemDB {
        var config = this.itemDBConfig[ID];
        if (!config) {
            throw new CustomError.UserError(ERRC.COMMON.CONFIG_NOT_FOUND, {
                msg: 'COMMON.CONFIG_NOT_FOUND, itemdb, ID=' + ID
            })
        }
        return config;
    }
    public all():{[ID:number]: itemDB} {
        return this.itemDBConfig;
    }
}

// item_randomdb
export class item_randomDB {
	ID:any;			//ID
	item_type:any;	//关卡难度
	random:any;		//随机概率
	item_id:any;	//随机道具组
	constructor(data) {
		this.ID = data.ID;
		this.item_type = data.item_type;
		this.random = data.random;
		this.item_id = data.item_id;
	}
}
class item_randomDBMgr {
    item_randomDBConfig : {[ID:number]: item_randomDB} = {};
    constructor(data) {
        this.item_randomDBConfig = {};
        Object.keys(data).forEach((key) => {
            this.item_randomDBConfig[data[key].ID] = new item_randomDB(data[key]);
        });
    }
    public get(ID:number):item_randomDB {
        var config = this.item_randomDBConfig[ID];
        if (!config) {
            throw new CustomError.UserError(ERRC.COMMON.CONFIG_NOT_FOUND, {
                msg: 'COMMON.CONFIG_NOT_FOUND, item_randomdb, ID=' + ID
            })
        }
        return config;
    }
    public all():{[ID:number]: item_randomDB} {
        return this.item_randomDBConfig;
    }
}

// monster_lvdb
export class monster_lvDB {
	ID:any;			//ID
	lv:any;			//关卡难度
	monsterID:any;	//怪物组id
	hp:any;			//生命
	att:any;		//攻击
	gold:any;		//掉落yxb
	bossID:any;		//boss组id
	bosshp:any;		//生命
	bossatt:any;	//攻击
	bossgold:any;	//掉落yxb
	mapID:any;		//地图id
	formation:any;	//阵容
	skill_list:any;	//boss技能
	constructor(data) {
		this.ID = data.ID;
		this.lv = data.lv;
		this.monsterID = data.monsterID;
		this.hp = data.hp;
		this.att = data.att;
		this.gold = data.gold;
		this.bossID = data.bossID;
		this.bosshp = data.bosshp;
		this.bossatt = data.bossatt;
		this.bossgold = data.bossgold;
		this.mapID = data.mapID;
		this.formation = data.formation;
		this.skill_list = data.skill_list;
	}
}
class monster_lvDBMgr {
    monster_lvDBConfig : {[ID:number]: monster_lvDB} = {};
    constructor(data) {
        this.monster_lvDBConfig = {};
        Object.keys(data).forEach((key) => {
            this.monster_lvDBConfig[data[key].ID] = new monster_lvDB(data[key]);
        });
    }
    public get(ID:number):monster_lvDB {
        var config = this.monster_lvDBConfig[ID];
        if (!config) {
            throw new CustomError.UserError(ERRC.COMMON.CONFIG_NOT_FOUND, {
                msg: 'COMMON.CONFIG_NOT_FOUND, monster_lvdb, ID=' + ID
            })
        }
        return config;
    }
    public all():{[ID:number]: monster_lvDB} {
        return this.monster_lvDBConfig;
    }
}

// next_storeydb
export class next_storeyDB {
	ID:any;		//ID
	lv:any;		//关卡难度
	gold:any;	//解锁费用
	constructor(data) {
		this.ID = data.ID;
		this.lv = data.lv;
		this.gold = data.gold;
	}
}
class next_storeyDBMgr {
    next_storeyDBConfig : {[ID:number]: next_storeyDB} = {};
    constructor(data) {
        this.next_storeyDBConfig = {};
        Object.keys(data).forEach((key) => {
            this.next_storeyDBConfig[data[key].ID] = new next_storeyDB(data[key]);
        });
    }
    public get(ID:number):next_storeyDB {
        var config = this.next_storeyDBConfig[ID];
        if (!config) {
            throw new CustomError.UserError(ERRC.COMMON.CONFIG_NOT_FOUND, {
                msg: 'COMMON.CONFIG_NOT_FOUND, next_storeydb, ID=' + ID
            })
        }
        return config;
    }
    public all():{[ID:number]: next_storeyDB} {
        return this.next_storeyDBConfig;
    }
}

// shopdb
export class shopDB {
	ID:any;			//ID
	name:any;		//名字
	diamond:any;	//消费钻石
	dec:any;		//描述
	constructor(data) {
		this.ID = data.ID;
		this.name = data.name;
		this.diamond = data.diamond;
		this.dec = data.dec;
	}
}
class shopDBMgr {
    shopDBConfig : {[ID:number]: shopDB} = {};
    constructor(data) {
        this.shopDBConfig = {};
        Object.keys(data).forEach((key) => {
            this.shopDBConfig[data[key].ID] = new shopDB(data[key]);
        });
    }
    public get(ID:number):shopDB {
        var config = this.shopDBConfig[ID];
        if (!config) {
            throw new CustomError.UserError(ERRC.COMMON.CONFIG_NOT_FOUND, {
                msg: 'COMMON.CONFIG_NOT_FOUND, shopdb, ID=' + ID
            })
        }
        return config;
    }
    public all():{[ID:number]: shopDB} {
        return this.shopDBConfig;
    }
}

// skilldb
export class skillDB {
	ID:any;				//ID
	group:any;			//组别
	name:any;			//技能名
	skill_type:any;		//主,被动类型
	type:any;			//技能类型
	lv:any;				//等级
	cd:any;				//冷却
	time:any;			//持续时间
	need_lv:any;		//需要勇士等级
	need_plies:any;		//需要层等级
	need_skill_id:any;	//需要解锁技能组id
	count_1:any;		//属性1
	count_2:any;		//属性2
	gold:any;			//费用yxb
	dec_1_1:any;		//描述1
	dec_1_2:any;		//描述2
	dec_1_3:any;		//描述3
	constructor(data) {
		this.ID = data.ID;
		this.group = data.group;
		this.name = data.name;
		this.skill_type = data.skill_type;
		this.type = data.type;
		this.lv = data.lv;
		this.cd = data.cd;
		this.time = data.time;
		this.need_lv = data.need_lv;
		this.need_plies = data.need_plies;
		this.need_skill_id = data.need_skill_id;
		this.count_1 = data.count_1;
		this.count_2 = data.count_2;
		this.gold = data.gold;
		this.dec_1_1 = data.dec_1_1;
		this.dec_1_2 = data.dec_1_2;
		this.dec_1_3 = data.dec_1_3;
	}
}
class skillDBMgr {
    skillDBConfig : {[ID:number]: skillDB} = {};
    constructor(data) {
        this.skillDBConfig = {};
        Object.keys(data).forEach((key) => {
            this.skillDBConfig[data[key].ID] = new skillDB(data[key]);
        });
    }
    public get(ID:number):skillDB {
        var config = this.skillDBConfig[ID];
        if (!config) {
            throw new CustomError.UserError(ERRC.COMMON.CONFIG_NOT_FOUND, {
                msg: 'COMMON.CONFIG_NOT_FOUND, skilldb, ID=' + ID
            })
        }
        return config;
    }
    public all():{[ID:number]: skillDB} {
        return this.skillDBConfig;
    }
}

// welfaredb
export class welfareDB {
	ID:any;			//ID
	name:any;		//名字
	cost_type:any;	//消费类型
	cost_num:any;	//次数
	cost:any;		//消费值
	yxb:any;		//获得多少游戏币
	diamond:any;	//获得多少钻
	item_ID:any;	//获得装备ID
	item_num:any;	//获得装备数量
	constructor(data) {
		this.ID = data.ID;
		this.name = data.name;
		this.cost_type = data.cost_type;
		this.cost_num = data.cost_num;
		this.cost = data.cost;
		this.yxb = data.yxb;
		this.diamond = data.diamond;
		this.item_ID = data.item_ID;
		this.item_num = data.item_num;
	}
}
class welfareDBMgr {
    welfareDBConfig : {[ID:number]: welfareDB} = {};
    constructor(data) {
        this.welfareDBConfig = {};
        Object.keys(data).forEach((key) => {
            this.welfareDBConfig[data[key].ID] = new welfareDB(data[key]);
        });
    }
    public get(ID:number):welfareDB {
        var config = this.welfareDBConfig[ID];
        if (!config) {
            throw new CustomError.UserError(ERRC.COMMON.CONFIG_NOT_FOUND, {
                msg: 'COMMON.CONFIG_NOT_FOUND, welfaredb, ID=' + ID
            })
        }
        return config;
    }
    public all():{[ID:number]: welfareDB} {
        return this.welfareDBConfig;
    }
}

//********** footer **********//
export class ConfigMgr {
	achievementdb:achievementDBMgr = null;
	boss_skilldb:boss_skillDBMgr = null;
	herodb:heroDBMgr = null;
	hero_basicdb:hero_basicDBMgr = null;
	itemdb:itemDBMgr = null;
	item_randomdb:item_randomDBMgr = null;
	monster_lvdb:monster_lvDBMgr = null;
	next_storeydb:next_storeyDBMgr = null;
	shopdb:shopDBMgr = null;
	skilldb:skillDBMgr = null;
	welfaredb:welfareDBMgr = null;

	public loadAllConfig(jsonDir) {
		var contents, json;

		/// achievementdb
        try {
            contents = fs.readFileSync(jsonDir + 'achievementdb.json');
            json = JSON.parse(contents);
            this.achievementdb = new achievementDBMgr(json);
        } catch (err) {
            throw new Error('achievementdb.json read failed');
        }
		/// boss_skilldb
        try {
            contents = fs.readFileSync(jsonDir + 'boss_skilldb.json');
            json = JSON.parse(contents);
            this.boss_skilldb = new boss_skillDBMgr(json);
        } catch (err) {
            throw new Error('boss_skilldb.json read failed');
        }
		/// herodb
        try {
            contents = fs.readFileSync(jsonDir + 'herodb.json');
            json = JSON.parse(contents);
            this.herodb = new heroDBMgr(json);
        } catch (err) {
            throw new Error('herodb.json read failed');
        }
		/// hero_basicdb
        try {
            contents = fs.readFileSync(jsonDir + 'hero_basicdb.json');
            json = JSON.parse(contents);
            this.hero_basicdb = new hero_basicDBMgr(json);
        } catch (err) {
            throw new Error('hero_basicdb.json read failed');
        }
		/// itemdb
        try {
            contents = fs.readFileSync(jsonDir + 'itemdb.json');
            json = JSON.parse(contents);
            this.itemdb = new itemDBMgr(json);
        } catch (err) {
            throw new Error('itemdb.json read failed');
        }
		/// item_randomdb
        try {
            contents = fs.readFileSync(jsonDir + 'item_randomdb.json');
            json = JSON.parse(contents);
            this.item_randomdb = new item_randomDBMgr(json);
        } catch (err) {
            throw new Error('item_randomdb.json read failed');
        }
		/// monster_lvdb
        try {
            contents = fs.readFileSync(jsonDir + 'monster_lvdb.json');
            json = JSON.parse(contents);
            this.monster_lvdb = new monster_lvDBMgr(json);
        } catch (err) {
            throw new Error('monster_lvdb.json read failed');
        }
		/// next_storeydb
        try {
            contents = fs.readFileSync(jsonDir + 'next_storeydb.json');
            json = JSON.parse(contents);
            this.next_storeydb = new next_storeyDBMgr(json);
        } catch (err) {
            throw new Error('next_storeydb.json read failed');
        }
		/// shopdb
        try {
            contents = fs.readFileSync(jsonDir + 'shopdb.json');
            json = JSON.parse(contents);
            this.shopdb = new shopDBMgr(json);
        } catch (err) {
            throw new Error('shopdb.json read failed');
        }
		/// skilldb
        try {
            contents = fs.readFileSync(jsonDir + 'skilldb.json');
            json = JSON.parse(contents);
            this.skilldb = new skillDBMgr(json);
        } catch (err) {
            throw new Error('skilldb.json read failed');
        }
	}
}
