/**
 * Created by showbu on 15-11-25.
 */
import request = require('request');
import Log = require('../util/log');
import ErrorCode = require('../util/error_code');
//import GameUtil = require('../util/game_util');

var MoltenApi:any = {};
// 校验订单号地址
MoltenApi.MOLTEN_VERIFY_URL
    = "http://www.moltentec.com/htgame/index.php?c=Node&m=verify";
MoltenApi.MOLTEN_FOCUS_URL
    = "http://www.moltentec.com/htgame/index.php?c=Node&m=subscribe&uid=";
MoltenApi.MOLTEN_CDKEY_URL
    = "http://www.moltentec.com/htgame/index.php?c=Node&m=get_cdk_type";

function combinData(data) {
    var list = [];

    for (var key in data) {
        list.push(key);
    }
    list.sort();

    var content = "";
    for (var i in list) {
        var key = list[i];
        var value = data[key];
        if (value) {
            content += ((i == 0 ? "" : "&") + key + "=" + value);
        } else {
            content += ((i == 0 ? "" : "&") + key + "=");
        }
    }

    return content;
}

function genSign(data, key) {
    delete data['sign']; // 除去sign参数
    var content = combinData(data);
    content += ("&appkey=" + key);
    console.log('string', content);
    var crypto = require("crypto");
    return crypto.createHash("md5").update(content).digest("hex").toUpperCase();
}

export function init(opts) {
    this.apiKey_ = opts.apiKey;
    this.gameId_ = opts.gameId;
}

export function verifyDelivery(requestArgs, callback) {

    if (typeof requestArgs === 'undefined') {
        var err = new Error('requestArgs is not array');
        callback(err);
        return ;
    }

    var uid = requestArgs['uid'],
        tradeNo = requestArgs['trade_no'],
        productId = requestArgs['pid'],
        transId = requestArgs['pid'],
        sign = requestArgs['sign'],
        time = requestArgs['time'],
        gameId = requestArgs['gameid'];

    //gameid pid sign time trade_no uid

        //self = this;

    //console.log(typeof uid);

    if (typeof uid === 'undefined'
        || typeof time === 'undefined'
        || typeof sign === 'undefined'
        ||typeof gameId === 'undefined'
        //|| typeof reqid === 'undefined'
        //|| typeof notifyId === 'undefined'
        || typeof transId === 'undefined'
        || typeof productId === 'undefined'
        ) {
        var err = new Error('Invalid request args');
        callback(err);
        return ;
    }

    var inputParams = {
        'gameid' : gameId,
        'pid' : productId,
        'time' : time,
        'trade_no' : tradeNo,
        'uid' : uid,
    };

    var mySign = genSign(inputParams, this.apiKey_);
    if (sign != mySign) {
        Log.sError('Sign Err, recv = ' + sign + ', my = ' + mySign);
        var err = new Error('Sign Err, recv = ' + sign + ', my = ' + mySign);
        callback(err);
        return ;
    }

    var verifyParams = {
        'gameid' : this.gameId_,
        'trade_no' : tradeNo
    };

    var verifySign = genSign(verifyParams, this.apiKey_);
    verifyParams['sign'] = verifySign;
    var verifyQuery = combinData(verifyParams);

    //console.log(MoltenApi.MOLTEN_VERIFY_URL + '&' + verifyQuery);
    // 验证订单是否成功
    request(MoltenApi.MOLTEN_VERIFY_URL + '&' + verifyQuery,
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                body = body.replace(/[\r\n]/g, '');
                if (body === 'SUCCESS') {
                    callback(null);
                    return ;
                } else {
                    // 订单失败
                    var err = new Error('Trans Failed');
                    callback(err);
                    return ;
                }
            } else {
                var err = new Error('Verify network error');
                callback(err);
                return ;
            }
        });
}

export function isAttention(uid, callback) {

    //var self = this;
    if (typeof uid !== 'number') {
        var err = new Error('uid is not number');
        callback(err);
        return ;
    }

    //var requestArgs = {
    //    gameid : self.gameId_,
    //    uid : uid
    //};
    //var sign = genSign(requestArgs, self.apiKey_);
    //requestArgs['sign'] = sign;
    //var query = combinData(requestArgs);
    //uid = 3;
    var url = MoltenApi.MOLTEN_FOCUS_URL + uid;
    request(url,
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                body = body.replace(/[\r\n]/g, '');
                //body = body.replace(/\n/g, '');
                if (body === 'SUCCESS') {
                    return callback(null, true);
                } else {
                    return callback(null, false);
                }
            } else {
                var err = new Error('Get isAttention network error');
                callback(err);
                return ;
            }
        });
}

export function cdkeyAward(requestArgs, callback) {

    if (typeof requestArgs === 'undefined') {
        //var err = new Error('requestArgs is not array');
        callback(ErrorCode.GAME.PARAM_ERR);
        return ;
    }

    var uid = requestArgs['uid'],
        cdkey = requestArgs['cdkey'],
        gameId = requestArgs['gameid'];

    if (typeof uid === 'undefined'
        || typeof cdkey === 'undefined'
        ||typeof gameId === 'undefined'
    ) {
        //var err = new Error('Invalid request args');
        callback(ErrorCode.GAME.PARAM_INVALID);
        return ;
    }

    var verifyParams = {
        'uid' : uid,
        'gameid' : gameId,
        'cdkey' : cdkey
    };

    var verifyQuery = combinData(verifyParams);
    // 验证CDKEY是否能够领取
    request(MoltenApi.MOLTEN_CDKEY_URL + '&' + verifyQuery,
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                body = body.replace(/[\r\n]/g, '');
                body = JSON.parse(body);
                if (body.ret === 'SUCCESS') {
                    callback(null, body.type);
                    return ;
                } else {

                    // 获取CDKEY 失败
                    //var err = new Error('Trans Failed');
                    callback(ErrorCode.GAME.CDKEY_NOT_EXIST);
                    return ;
                }
            } else {
                //var err = new Error('Verify network error');
                callback(ErrorCode.GAME.CDKEY_NOT_EXIST);
                return ;
            }
        });
}