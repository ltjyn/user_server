/*jslint node: true*/
'use strict';

var request = require('request'),
    util = require('util'),
    querystring = require('querystring'),
    YouGuangApi = {};

// 校验订单号地址
YouGuangApi.YOU_GUANG_VERIFY_URL
    = "http://login.vutimes.com/pay/paygate/verify.php";
YouGuangApi.YOU_GUANG_FOCUS_URL 
    = "http://login.vutimes.com/pay/paygate/focus.php";

function combinData(data) {

    var list = [];

    for (var key in data) {
        list.push(key);
    }
    list.sort();

    var content = "";
    for (var i in list) {
        var key = list[i];
        var value = data[key];
        if (value) {
            content += ((i == 0 ? "" : "&") + key + "=" + value);
        } else {
            content += ((i == 0 ? "" : "&") + key + "=");
        }
    }

    return content;
}

function genSign(data, key) {

    delete data['sign']; // 除去sign参数
    var content = combinData(data);
    content += ("&key=" + key);
    var crypto = require("crypto");
    return crypto.createHash("md5").update(content).digest("hex").toUpperCase();
}

YouGuangApi.init = function (opts) {
    this.apiKey_ = opts.apiKey;
    this.gameId_ = opts.gameId;
}

YouGuangApi.verifyDelivery = function (requestArgs, callback) {

    if (typeof requestArgs === 'undefined') {
        var err = new Error('requestArgs is not array'); 
        callback(err); 
        return ;
    }

    var uid = requestArgs['uid'],
        rmb = requestArgs['rmb'],
        sign = requestArgs['sign'],
        reqid = requestArgs['reqid'],
        transId = requestArgs['trans_id'],
        productId = requestArgs['product_id'],
        notifyId = requestArgs['notify_id'],
        self = this;

    if (typeof uid === 'undefined'
            || typeof rmb === 'undefined'
            || typeof sign === 'undefined'
            || typeof reqid === 'undefined'
            || typeof transId === 'undefined'
            || typeof productId === 'undefined'
            || typeof notifyId === 'undefined') {
        var err = new Error('Invalid request args'); 
        callback(err);
        return ;
    }

    var mySign = genSign(requestArgs, self.apiKey_);

    if (sign != mySign) {
        var err = new Error(
                util.format('Sign Err, recv = "%s", my = "%s"', 
                sign, mySign));
        callback(err);
        return ;
    }

    var verifyParams = {
        'gameid' : self.gameId_,
        'notify_id' : notifyId
    };
    var verifySign = genSign(verifyParams, self.apiKey_);
    verifyParams['sign'] = verifySign;
    var verifyQuery = combinData(verifyParams);

    // 验证订单是否成功
    var request = require('request');
    request(self.YOU_GUANG_VERIFY_URL + '?' + verifyQuery, 
            function (error, response, body) {
        if (!error && response.statusCode == 200) {
            if (body === 'SUCCESS') {
                callback(null);
                return ;
            } else {
                // 订单失败 
                var err = new Error('Trans Failed');
                callback(err);
                //callback(null);
                return ;
            }
        } else {
            var err = new Error('Verify network error');
            callback(err);
            return ;
        }
    });
}

YouGuangApi.isFocus = function (uid, callback) {

    var self = this;
    if (typeof uid !== 'number') {
        var err = new Error('uid is not number'); 
        callback(err); 
        return ;
    }

    var requestArgs = {
        gameid : self.gameId_,
        uid : uid
    };
    var sign = genSign(requestArgs, self.apiKey_);
    requestArgs['sign'] = sign;
    var query = combinData(requestArgs);

    request(this.YOU_GUANG_FOCUS_URL + '?' + query, 
            function (error, response, body) {
        if (!error && response.statusCode == 200) {
            if (body === 'YES') {
                return callback(null, true);
            } else {
                return callback(null, false);
            }
        } else {
            var err = new Error('Get isFocus network error');
            callback(err);
            return ;
        }
    });
}

module.exports = YouGuangApi;

