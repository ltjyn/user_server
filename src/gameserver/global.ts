import express = require('express');
import log = require('../util/log');

export var stateInfo = {
    canLogin: true,
    errorCode: null
};

/**
 * 游戏发包入口
 * @param res
 * @param data
 */
export function sendJson(res:express.Response, data:any) {
    log.sInfo('data send %j', data);
    res.json(data);
}