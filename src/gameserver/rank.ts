/**
 * Created by showbu on 15-11-24.
 */
import log = require('../util/log');
import GameConst = require('./game_const');
import HttpUtil = require('../util/http_util');
import Enum = require('../util/enum');
import RedisMgr = require('../redis/redis_mgr');
import BaseController = require('../gameserver/base_controller');
import GameUtil = require('../util/game_util');
import WorldDB = require('../database/impl/world_db');

var async = require('async');
var roleRedis = RedisMgr.getInstance('roleRedis');

//class GameRank{
export var rankRedisName:string = GameConst.RANK_KEY_NAME;
export var redisPrefix = 'role';

//public init(){
//    this.rankRedisName = GameConst.RANK_KEY_NAME;
//    this.redisPrefix = 'role';
//}

export function getRedisKey(uid:number) {
    return redisPrefix + ':' + uid;
}

export function getDataFields():any {
    var fields = ['nickname','headimgurl','maxFloor'];
    return fields;
}

// 批量拉取信息
export function getUserInfoList(uidList, callback) {
    var userInfos = {};
    async.each(uidList, function (uid, cb) {
        uid = parseInt(uid, 10);
        //var accountTokenRedis = RedisMgr.getInstance('accountTokenRedis');

        roleRedis.hmget(getRedisKey(uid), getDataFields(), (err, reply)=> {
            if (err || reply == null) {
                userInfos[uid] = null;
                return cb(null);
            }
            // 缓存不存在，从db查询
            else if (Object.keys(reply).length === 0) {
                WorldDB.conn.execute('select * from player_info_' + GameUtil.getTableNum(uid) + ' where ?', {uid: uid}, (err, connection, result)=> {
                    if (err) {
                        userInfos[uid] = null;
                        return cb(null);
                    }

                    if (!result || result.length === 0) {
                        userInfos[uid] = null;
                        return cb(null);
                    }
                    userInfos[uid] = {
                        nickname:'',
                        headimgurl:'',
                        maxFloor:0
                    }
                    userInfos[uid] = GameUtil.deserialize(userInfos[uid], result[0]);
                    return cb(null);
                })
            }
            // 缓存命中
            else {
                userInfos[uid] = {
                    nickname:'',
                    headimgurl:'',
                    maxFloor:0
                }
                if(reply['nickname']){
                    var len = reply['nickname'].length;
                    if(len > 0){
                        if(reply['nickname'][len - 1] != '\"'){
                            reply['nickname'] += '\"';
                        }
                    }
                }
                userInfos[uid] = GameUtil.deserialize(userInfos[uid], reply);
                return cb(null);
            }
        });
    }, function (err) {
        if (err) {
            return callback(err);
        }

        return callback(null, userInfos);
    });
}

// 拉取用户排名
export function getUserRank(uid, callback):void {
    //var data = [this.rankRedisName, uid];
    roleRedis.zrevrank(rankRedisName, uid, function (err, reply) {
        if (err) {
            //logger.error('get rank of ' + uid + ' failed, err = ' + err);
            return callback(err);
        }

        // 无排名
        if (reply == null) {
            return callback(null, 0);
        }

        var rank = Number(reply) + 1;

        return callback(null, rank);
    });
}

// 获取附近排名
export function getNearRanks(_uid, callback):void {
    roleRedis.zrevrank(rankRedisName, _uid, function (err, reply) {
        if (err) {
            log.sError('get my rank failed, err = ' + err);
            return callback(err);
        }

        // 无排名
        if (reply == null) {
            return callback(null, []);
        }

        var myRank = Number(reply);
        var min:number = myRank - 2;
        var max:number = myRank + 2;
        if (min < 0) {
            min = 0;
        }

        //var data = [this.rankRedisName, min, max, 'WITHSCORES'];
        roleRedis.zrevrange(rankRedisName, min, max, function (err, reply) {
            if (err) {
                log.sError('get near rank failed, err = ' + err);
                return callback(err);
            } else {
                var nearRanks = [];
                for (var i = 0; i < reply.length; i += 2) {
                    nearRanks.push({
                        uid: Number(reply[i]),
                        score: Number(reply[i + 1]),
                        rank: min + i / 2 + 1
                    });
                }
                return callback(null, nearRanks);
            }
        });
    });
}

// 获取前几名
export function getTopRanks(callback):void {
    roleRedis.zrevrange(rankRedisName, 0, 9, function (err, reply) {
        if (err) {
            log.sError('get top rank failed, err = ' + err);
            return callback(err);
        } else {
            var topRanks = [];
            for (var i = 0; i < reply.length; i += 2) {
                topRanks.push({
                    uid: Number(reply[i]),
                    score: Number(reply[i + 1]),
                    rank: i / 2 + 1
                });
            }
            return callback(null, topRanks);
        }
    });
}

// 获取额外参数
export function fetchExtraFields(data, info, extraFields):any {
    if (Array.isArray(extraFields)) {
        for (var i = 0; i < extraFields.length; i++) {
            var extraName = extraFields[i];
            if (typeof info[extraName] !== 'undefined') {
                data[extraName] = info[extraName];
            }
        }
    }

    return data;
}

// 获取好友排行榜
export function getFriendRankList(_uid, _scoreFieldName, _cb, _extraFrields?):void {
    //var self = this;

    HttpUtil.httpGet('http://115.159.41.217/htgame/index.php?c=Node&m=getFriends&uid=' + _uid, function (err, data) {
        if (err) {
            //logger.error('get friend rank list failed, err = ' + err);
            return _cb(err);
        }

        if (data.error == 40002) {
            return _cb(null, []);
        } else if (data.error) {
            //logger.error('get friend rank list failed, errno = ' + data.error);
            return _cb(data.error);
        }

        var fuids = data.friendList;
        var ranks = {};
        //var scores = {};
        async.each(fuids, function (fuid, cb) {
            getUserRank(fuid, function (err, rank) {
                if (err) {
                    return cb(err);
                } else if (rank != 0) {
                    ranks[fuid] = rank;
                }
                return cb(null);
            });
        }, function (err) {
            if (err) {
                return _cb(err);
            }

            var hasRankUids = Object.keys(ranks);
            getUserInfoList(hasRankUids, function (err, result) {
                if (err) {
                    return _cb(err);
                }

                var friendList = [];
                for (var i = 0; i < hasRankUids.length; i++) {
                    var fuid = hasRankUids[i];
                    var info = result[fuid];
                    if(info == null || Object.keys(info).length == 0){
                        info.nickname = '';
                        info.headimgurl = '';
                    }
                    var data = {
                        rank: ranks[fuid],
                        nickname: info.nickname,
                        headimgurl: info.headimgurl,
                        score: info[_scoreFieldName]
                    };
                    // 额外需要的参数
                    data = fetchExtraFields(data, info, _extraFrields);
                    friendList.push(data);
                }
                return _cb(null, friendList);
            });
        });
    }, Enum.HTTP_RES_DATA_TYPE.JSON, 2000);
}

// 获取排行榜列表
export function getRankList(_uid, _cb, _extraFrields?):void {
    //var self = this;
    async.parallel({
            'one': getNearRanks.bind(null, _uid),
            'two': getTopRanks.bind(null)
        },
        function (err, results) {
            if (err) {
                return _cb(err);
            }

            var nearRanks = results['one'];
            var topRanks = results['two'];

            var uidMap = {};
            //var uidList = [];
            for (var i = 0; i < nearRanks.length; i++) {
                var uid = nearRanks[i].uid;
                uidMap[uid] = true;
            }
            for (var i = 0; i < topRanks.length; i++) {
                var uid = topRanks[i].uid;
                uidMap[uid] = true;
            }

            var uidList = Object.keys(uidMap);
            getUserInfoList(uidList, function (err, result) {
                if (err) {
                    return _cb(err);
                }

                for (var i = 0; i < nearRanks.length; i++) {
                    var uid = nearRanks[i].uid;
                    var info = result[uid];
                    if (info != null && Object.keys(info).length > 0 && info.headimgurl != 'undefined') {
                        nearRanks[i].headimgurl = info.headimgurl;
                    } else {
                        nearRanks[i].headimgurl = '';
                    }

                    if (info != null && Object.keys(info).length > 0 && info.nickname != 'undefined') {
                        nearRanks[i].nickname = info.nickname;
                    } else {
                        nearRanks[i].nickname = '';
                    }

                    // 额外需要的参数
                    nearRanks[i] = fetchExtraFields(nearRanks[i], info, _extraFrields);
                }

                for (var i = 0; i < topRanks.length; i++) {
                    var uid = topRanks[i].uid;
                    var info = result[uid];
                    if(info != null && Object.keys(info).length > 0 && info.headimgurl != 'undefined'){
                        topRanks[i].headimgurl = info.headimgurl;
                    }
                    else {
                        topRanks[i].headimgurl = '';
                    }

                    if (info !== null && Object.keys(info).length > 0 && info.nickname != 'undefined') {
                        topRanks[i].nickname = info.nickname;
                    } else {
                        topRanks[i].nickname = '';
                    }

                    // 额外需要的参数
                    topRanks[i] = fetchExtraFields(topRanks[i], info, _extraFrields);
                }

                return _cb(null, nearRanks, topRanks);
            });
        }
    );
}

export function resetRank():void{

}
//}

//export GameRank;