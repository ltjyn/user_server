/**
 * Created by showbu on 15-11-28.
 */
import MoltenApi = require('../../molten/molten_api');
import GameConst = require('../game_const');
import log = require('../../util/log');
import Role = require('../role');
import Dispatcher = require('../../http/dispatcher');
import ErrorCode = require('../../util/error_code');
import Global = require('../../gameserver/global');
import Time = require('../../util/time');

import RedisMgr = require('../../redis/redis_mgr');
var roleRedis = RedisMgr.getInstance('roleRedis');

MoltenApi.init({
    apiKey : GameConst.PAY_KEY,
    gameId : GameConst.GAME_ID
});

function transerRMBToDiamond(productId) {
    if (productId == 2001) {
        return 200;
    } else if (productId == 2002) {
        return 630;
    } else if (productId == 2003) {
        return 3500;
    } else if (productId == 2004) {
        return 6500;
    } else if (productId == 2005) {
        return 14000;
    } else if (productId == 2006) {
        return 31000;
    } else if (productId == 2007) {// yue ka
        return 0;
    } else {
        return 0;
    }
}

export function index(invoke:Dispatcher.Invoke):void {
    //var self = this;
    var args = invoke.req.query;
    var productId = args['pid'];
    var tradeNo = args['trade_no'];

    MoltenApi.verifyDelivery(invoke.req.query, function (err) {

        if (err) {
            log.sError('delivery verify failed, err = ' + err
                + ', args = ' + JSON.stringify(args));
            return invoke.res.json('VERIFY ERROR');
        }

        var num = transerRMBToDiamond(productId);
        var role = new Role(args['uid'], '');

        role.load((err, isExist)=> {
            if (err) {
                log.sError('load player ' + args['uid'] + ' failed, err = ' + err);
                //return self.res.end('GET PLAYER ERROR');
                return invoke.res.json('GET PLAYER ERROR');
            } else if (!isExist) {

                log.sError('player ' + args['uid'] + ' not find');
                //return self.res.end('PLAYER NOT FOUND');
                return invoke.res.json('PLAYER NOT FOUND');
            } else {
                if(productId == 2007){// yue ka
                    if(role.isMonthGiftIn()){//zai yue ka shijian nei
                        num = 3650;//fanfu chong yueka 3650 gold
                    }
                    else{
                        num = 0;
                        role.set('buyMonthItemTime', Time.realNow());
                    }
                }

                role.charge(tradeNo, num, GameConst.ADD_DIAMOND_REASON.CHARGE, function (err) {
                    if (err) {
                        log.sError('player ' + args['uid'] + ' charge failed');
                        return invoke.res.json('CHARGE FAILED');
                    } else {
                        //log.sInfo('uid:%d charge success!', args['uid']);
                        role.save(()=> {
                        }, false, true);
                        return invoke.res.json('SUCCESS');
                    }
                });
            }
        });
    });
}

export function resetRank(invoke:Dispatcher.Invoke):void {
    //for(var i = 0; i < 10 ; i++){
    //    WorldDB.conn.execute('select * from player_info_' + i, (err, connection, result)=> {
    //        if (err) {
    //
    //            return cb(null);
    //        }
    //
    //        if (!result || result.length === 0) {
    //            userInfos[uid] = null;
    //            return cb(null);
    //        }
    //        userInfos[uid] = {
    //            nickname:'',
    //            headimgurl:'',
    //            maxFloor:0
    //        }
    //        userInfos[uid] = GameUtil.deserialize(userInfos[uid], result[0]);
    //        return cb(null);
    //    })
    //}

    //roleRedis.keys(['role:*'], (err, reply)=>{
    //    if(err){
    //        return invoke.res.json('FAILED');
    //    }
    //    for(var i = 0; i < reply.length; i ++){
    //        var redisName = reply[i];
    //        roleRedis.hmget(redisName, ['uid'], (err, reply)=> {
    //            if(reply['uid'] !== 'undefined'){
    //                var role = new Role(reply['uid'], '');
    //
    //                role.load((err, isExist)=> {
    //                    if (err) {
    //
    //                    }else if (!isExist) {
    //
    //                    }
    //                    else{
    //                        role.syncMaxFloorRank();
    //                    }
    //                });
    //            }
    //        });
    //    }
    //    return invoke.res.json('SUCCESS');
    //});
}

//export function addParam(invoke:Dispatcher.Invoke):void {
//    var args = invoke.req.query;
//    var name = args.name;
//    var value = args.value;
//    var key = args.key;
//
//    if(key === GameConst.PAY_KEY){
//        for(var i = 0; i < 10 ; i++){
//            WorldDB.conn.execute('update player_info_' + i + 'set ' + name + ' = ' + name + '+' + value,{}, (err, connection, result)=> {
//                if (err) {
//                    return;
//                }
//
//                if (err) {
//                    return;
//                }
//
//                if (!result || result.length === 0) {
//                    return ;
//                }
//            })
//        }
//    }
//}