/**
 * 这种隶属于游戏的逻辑控制器
 */

import log = require('../../util/log');
import WorldDB = require('../../database/impl/world_db');
import Role = require('../role');
import Dispatcher = require('../../http/dispatcher');
import ErrorCode = require('../../util/error_code');
import BaseController = require('../base_controller');
import Time = require('../../util/time');
import GameConst = require('../game_const');
import GameRank = require('../rank');
import GameUtil = require('../../util/game_util');
import MoltenApi = require('../../molten/molten_api');
import GameData = require('../game_data');
var configMgr = require('../../config').configMgr;

//class GameController extends BaseController {
export function playerLogin(uid, args):void {

    var role = new Role(uid, args['token']);
    role.load((err, isExist)=> {
        if (!isExist) {
            role.create((err)=> {
                if (err) {
                    log.sError('role create error, uid=' + uid);
                }
                response();
            });
        }
        else {
            role.updateNick((err)=> {
                if (!err) {
                    var time = Time.realNow();
                    var rmbPay = role.get('rmbPay');
                    role.set('rmbPay', 0);
                    var offlineData = role.calcOffline();
                    role.set('lastLoginTime', time);
                    role.set('lastAliveTime', time);

                    response(rmbPay, offlineData);
                }
                else{
                    Dispatcher.pop(uid, null, err);
                }
            });
        }
    });

    function response(rmbPay?:number, offlineData?:any) {
        //role.syncMaxFloorRank();
        GameConst.heroMaxLv = Object.keys(configMgr.hero_basicdb.all()).length;

        var data = role.getCopyData();
        if (rmbPay) {
            data.rmbpay = rmbPay;
        }

        if (offlineData) {
            data.offlineTime = offlineData.offlineTime;
            data.offlineYxb = offlineData.offlineYxb;
        }

        if (data.drawEffectTime <= Time.realNow()) {
            data.drawEffectTime = 0;
            data.drawEffectCount = 0;

            role.set('drawEffectTime', 0);
            role.set('drawEffectCount', 0);
        }

        // 每天分享数据清零
        var lastShareDate = new Date(data.lastShareTime * 1000);
        var nowDate = new Date();
        if (!GameUtil.isSameDay(lastShareDate, nowDate)) {
            data.dayShare = 0;
            role.set('dayShare', 0);
        }

        var lastDropTime = new Date(data.lastItemDropTime * 1000);
        if (!GameUtil.isSameDay(lastDropTime, nowDate)) {
            data.dayDrop = 0;
            role.set('dayDrop', 0);
        }

        data.now = Time.realNow();
        data.showCdkey = 1;
        var floor = Math.max(data.maxFloor, 1);
        floor = floor % 10 == 0 ? floor : Math.floor(floor / 10) * 10 + 10;
        data.unLockYxb = role.getUnlockYxb(floor);

        floor = Math.max(data.maxFloor, 1);
        var sealFloor = floor % 10 == 0 ? floor - 10 + 1 : Math.floor(floor/10) * 10 + 1;
        sealFloor = Math.max(sealFloor, 1);

        data.weaponList = role.getWeaponList();
        data.skillList = role.getSkillList();
        data.soldierList = role.getSoldierList();
        data.heroInfo = role.getHeroInfo();
        data.battleFiled = role.getBattleFiled(sealFloor);

        //if(data.isFirstPay){//如果首充过了,则主动清除rmbPay
        //    //data.rmbPay = 0;
        //    role.set('rmbPay', 0);
        //}

        role.save(()=> {
        }, false, true);

        Dispatcher.pop(uid, data);
    }
}

export function alive(uid, args):void {
    BaseController.loadRole(uid, args['token'], (err, role:Role) => {
        if (err) {
            return Dispatcher.pop(uid, null, err);
        }

        var input = args['body'].replace(/ /g, '+');
        input = JSON.parse(input);
        // TODO 业务
        var addYxb = input.addYxb.toString();
        var heroBorn = input.heroBorn;
        var heroDie = input.heroDie;
        var killEnemy = input.killEnemy;
        var killBox = input.killBox;
        //var useSkill = input.useSkill;
        var weaponUp = input.weaponUp;
        var bskBorn = input.bskBorn;
        var highestFloorClear = input.highestFloorClear;
        var soldierNumber = input.sn;
        var bossNumber = input.bn;
        var maxFloor = Math.max(input.maxFloor, 1);

        addYxb = addYxb.replace(/ /g, '+');
        addYxb = Number(addYxb);
        bskBorn = Number(bskBorn);
        heroBorn = Number(heroBorn);
        killEnemy = Number(killEnemy);
        killBox = Number(killBox);
        //useSkill = Number(useSkill);
        weaponUp = Number(weaponUp);
        heroDie = Number(heroDie);
        maxFloor = Number(maxFloor);
        soldierNumber = Number(soldierNumber);
        bossNumber = Number(bossNumber);

        if (isNaN(addYxb) || isNaN(heroBorn) || isNaN(bskBorn)|| isNaN(killEnemy)|| isNaN(killBox)|| isNaN(weaponUp)
            || isNaN(heroDie) || isNaN(maxFloor) || isNaN(soldierNumber)  || isNaN(bossNumber)) {
            log.sError('[PARAM INVALID] : %d invalid %s %s %s %s %s %s addGold %s',
                uid,
                isNaN(addYxb), isNaN(heroBorn), isNaN(bskBorn),
                isNaN(heroDie), isNaN(maxFloor),isNaN(soldierNumber),isNaN(bossNumber), args.addYxb);
            return Dispatcher.pop(uid, null, ErrorCode.GAME.PARAM_INVALID);
        }

        var now = Time.realNow();
        var sendData:any = {};
        sendData.now = now;

        //TODO alive package interval < 3 is cheat
        if(now - role.data.lastAliveTime < 3){
            //role.recvAliveTime = now;
            return Dispatcher.pop(uid, null, ErrorCode.GAME.ALIVE_TOO_FAST);
        }

        var skillList = role.get('skillList');

        var curFloorConf = configMgr.monster_lvdb.get(maxFloor);

        if(typeof curFloorConf === 'undefined'){
            return Dispatcher.pop(uid, null, ErrorCode.GAME.BATTLE_INVALID);
        }

        var weaponJBSZ = role.getWeaponInfoById(9);
        var weaponYCJZ = role.getWeaponInfoById(8);
        var weaponQLGZ = role.getWeaponInfoById(11);
        var weaponJYFG = role.getWeaponInfoById(17);

        var skillMoney = role.getSkillInfoByLv(7, skillList[7].lv);
        var moneyLastTime = skillMoney.lastTime - skillMoney.cd + 10;
        var moneyAdd = 0;
        if(skillMoney.lastTime > 0 && moneyLastTime > now){
            moneyAdd = skillMoney.count_1;
        }
        var s_yxb = curFloorConf.gold;
        var b_yxb = curFloorConf.bossgold;

        var x_add = (1.5 + weaponJBSZ.param * 0.01) * (0.05 + weaponYCJZ.param * 0.01 + 1);
        var yxb_add = 1 + weaponQLGZ.param * 0.01 + (moneyAdd * 0.01) * (1 + weaponJYFG.param * 0.01);

        var max_yxb = Math.ceil((s_yxb * soldierNumber + b_yxb * bossNumber) * yxb_add);

        var cheatNum = role.get('cheatNum');
        var cheatTime = role.get('lastCheatTime');
        if(cheatTime > 0 && cheatNum > 0){
            var lastCheatDate = new Date(cheatTime * 1000);
            var nowDate = new Date(now * 1000);
            if(!GameUtil.isSameDay(lastCheatDate, nowDate)){
                cheatNum = 0;
            }
        }

        if(soldierNumber > 9 || bossNumber > 2){
            //TODO sb cheat
            addYxb = s_yxb * x_add;
            cheatNum ++;
            cheatTime = now;
        }
        else if(addYxb > max_yxb){
            //TODO yxb cheat
            addYxb = s_yxb * x_add;
            cheatNum ++;
            cheatTime = now;
        }
        else{
            var delta = now - role.data.lastAliveTime;
            if(delta < GameConst.BATTLE.BATTLE_ALIVE_INTERVAL){
                addYxb = (addYxb / GameConst.BATTLE.BATTLE_ALIVE_INTERVAL) * delta;
            }
        }

        //if(cheatNum === 20){
        //    //TODO cheat log
        //    log.sWarn('uid = %d is cheat!!', role.data.uid);
        //}

        var oldMaxFloor = role.get('maxFloor');
        var firstTimes = false;
        if (maxFloor > oldMaxFloor) firstTimes = true;

        role.set('lastAliveTime', Time.realNow());
        role.set('highestfloorclear', highestFloorClear);
        role.add('yxb', addYxb);
        role.add('killEnemy', killEnemy);
        role.add('killBox', killBox);
        role.add('heroBorn', heroBorn);
        role.add('soldierDie', heroDie);
        role.add('weaponUp', weaponUp);
        role.set('cheatNum', cheatNum);
        role.set('lastCheatTime', cheatTime);

        role.battleDropWeapon(firstTimes);
        var syncWeaponData = role.genDropWeaponSyncData();
        for (var key in syncWeaponData) {
            sendData[key] = syncWeaponData[key];
        }

        var lastSyncDbTime = role.get('sealFloor');
        if (maxFloor > oldMaxFloor) {
            if(maxFloor - oldMaxFloor > 5){
                log.sInfo("jump floor fast uid:%d now:%d old:%d", uid, maxFloor, oldMaxFloor);
            }

            var floorCheatNum = role.get('floorCheatNum');
            var floorCheatTime = role.get('floorLastCheatTime');
            if(floorCheatTime > 0 && floorCheatNum > 0){
                var lastCheatDate = new Date(floorCheatTime * 1000);
                var nowDate = new Date(now * 1000);
                if(!GameUtil.isSameDay(lastCheatDate, nowDate)){
                    floorCheatNum = 0;
                }
            }

            if(oldMaxFloor > 10){
                var preFloorConf = configMgr.monster_lvdb.get(oldMaxFloor);
                if(typeof preFloorConf === 'undefined'){
                    return Dispatcher.pop(uid, null, ErrorCode.GAME.BATTLE_INVALID);
                }

                var maxHp = preFloorConf.bosshp;
                var maxAtt = preFloorConf.bossatt;

                var soldierList = role.get('soldierList');
                for (var sid in soldierList) {
                    var soldier = soldierList[sid];
                    var soldierInfo = role.getSoldierInfoByLv(sid, soldier.lv);

                    if (soldier.lv == 0) {
                        break;
                    }
                    var time = Math.ceil(soldierInfo.maxhp/maxAtt);
                    maxHp -= soldierInfo.attack * time;
                }
                if(maxHp > 0){
                    var heroInfo = role.getHeroInfoByLv(role.get('heroLv'));
                    var time = Math.ceil(heroInfo.maxhp / maxAtt);
                    var zgy = 3.5;// 3.5 zhen guan yu
                    //TODO shenbing
                    zgy *= 5;

                    var skill1 = skillList[1];
                    var skill2 = skillList[9];

                    var skillInfo1 = role.getSkillInfoByLv(1, skill1.lv);
                    var skillInfo2 = role.getSkillInfoByLv(9, skill2.lv);
                    maxHp -= (heroInfo.attack * time * (skillInfo1.count_1 + skillInfo2.count_1) * zgy);
                    if(maxHp > 0){
                        //TODO cheat
                        maxFloor = oldMaxFloor;
                        floorCheatNum ++;
                        floorCheatTime = now;

                        log.sWarn('%d cheat :: old = %d floor = %d, num = %d', role.data.uid, oldMaxFloor, maxFloor, floorCheatNum);
                        log.sWarn('%d cheat :: heroLv = %d heroAtk = %d skillInfo1 = %d, skillInfo2 = %d',
                            role.data.uid, heroInfo.lv, heroInfo.attack, skillInfo1.count_1, skillInfo2.count_1);
                        for(var sid in soldierList){
                            var soldier = soldierList[sid];
                            var soldierInfo = role.getSoldierInfoByLv(sid, soldier.lv);

                            if (soldier.lv == 0) {
                                break;
                            }
                            log.sWarn('%d cheat ::soldierInfo id = %d hp = %d atk = %d ', role.data.uid, sid, soldierInfo.maxhp, soldierInfo.attack);
                        }
                    }
                }
            }

            role.syncMaxFloorRank();
            role.set('maxFloor', maxFloor);
            role.set('sealFloor', Time.realNow());
            role.set('floorCheatNum', floorCheatNum);
            role.set('floorLastCheatTime', floorCheatTime);

            role.save(()=> {
            }, false, true);
        } else {
            var curTime = Time.realNow();
            if (lastSyncDbTime + 60 < curTime) {
                role.set('sealFloor', curTime);
                //log.sInfo('sync db1');
                role.save(()=> {
                }, false, true);
            } else {
                //log.sInfo('sync redis');
                role.save(()=> {
                }, true, false);
            }
        }

        sendData.maxFloor = role.get('maxFloor');
        sendData.yxb = role.get('yxb');
        sendData.gold = role.get('gold');
        sendData.killEnemy = role.get('killEnemy');
        sendData.heroDie = role.get('soldierDie');
        sendData.heroBorn = role.get('heroBorn');
        sendData.killBox = role.get('killBox');
        sendData.weaponUp = role.get('weaponUp');
        sendData.useSkill = role.get('useSkill');

        return Dispatcher.pop(uid, sendData);
    });
}

export function getFriendsRankList(uid, args):void {
    uid = Number(uid);
    //var self = this;
    GameRank.getFriendRankList(uid, 'maxFloor', function (err, rankList) {
        if (err) {
            return Dispatcher.pop(uid, null, ErrorCode.GAME.SYS_ERR);
        }
        return Dispatcher.pop(uid, {friendList : rankList});
    });
}

export function getRankList(uid, args):void {
    uid = Number(uid);
    //var self = this;
    GameRank.getRankList(uid, function (err, nearRanks, topRanks) {
        if (err) {
            return Dispatcher.pop(uid, null, ErrorCode.GAME.SYS_ERR);
        }
        for (var i = 0; i < nearRanks.length; i++) {
            nearRanks[i].score = Math.floor(nearRanks[i].score / Math.pow(10, 10));
        }
        for (var i = 0; i < topRanks.length; i++) {
            topRanks[i].score = Math.floor(topRanks[i].score / Math.pow(10, 10));
        }
        var data = {
            topRanks : topRanks,
            nearRanks : nearRanks
        };
        return Dispatcher.pop(uid, data);
    });
}

// 获取分享信息
export function getShareInfo(uid, args) {
    uid = Number(uid);
    //var self = this;
    //return Dispatcher.pop(uid, null, ErrorCode.GAME.PARAM_INVALID);

    BaseController.loadRole(uid, args['token'], (err, role:Role) => {
        if(err)
        {
            return Dispatcher.pop(uid, null, err);
        }
        var isAttention = role.get('isAttention');
        //var uid = role.get('uid');
        if (!isAttention) {
            MoltenApi.isAttention(uid, function (err, isAttention) {
                if (err) {
                    return Dispatcher.pop(uid, null, ErrorCode.GAME.SYS_ERR);
                } else {
                    if (isAttention) {
                        role.set('isAttention', 1);
                    } else {
                        role.set('isAttention', 0);
                    }
                    role.save(()=> {
                    }, true, false);
                    return response(role);
                }
            });
        } else {
            return response(role);
        }
    });

    function response(role) {
        var data = {
            //error : 0,
            isAttention : role.get('isAttention'),
            isAttentionAward : role.get('isAttentionAward')
        };
        return Dispatcher.pop(uid, data);
    }
}

// 勇士升级
export function heroUp(uid, args):void {
    //var self = this;
    var type = args.type;
    if (typeof type === 'undefined') {
        return Dispatcher.pop(uid, null, ErrorCode.GAME.PARAM_ERR);
    }

    type = Number(type);
    if (isNaN(type)) {
        return Dispatcher.pop(uid, null, ErrorCode.GAME.PARAM_ERR);
    }

    BaseController.loadRole(uid, args['token'], (err, role:Role) => {
        if (err) {
            return Dispatcher.pop(uid, null, err);
        }
        // TODO 业务
        var times = 0;
        if (type === 0) {
            times = 1;
        } else if (type === 1) {
            times = 10;
        } else if (type === 2) {
            times = 100;
        } else if (type === 3) {
            times = 1000;
        }
        var success = true;
        var soldierAtkSum = GameData.getSoldierAttackSum();
        for (var i = 0; success && i < times; i++) {
            success = role.heroUp(soldierAtkSum);
        }
        role.save(()=> {
        }, false, true);
        var heroInfo = [];
        var heroLv = role.get('heroLv');
        heroInfo.push(role.getHeroInfoByLv(heroLv));
        if(heroLv + 1 <= GameConst.heroMaxLv){
            heroInfo.push(role.getHeroInfoByLv(heroLv + 1));
        }

        var data = {
            lv: role.get('heroLv'),
            yxb: role.get('yxb'),
            heroInfo: heroInfo
        };
        return Dispatcher.pop(uid, data);
    });
}

export function skillUp(uid, args):void {
    //var self = this;
    var skillId = args.skillId;
    if (typeof skillId === 'undefined') {
        return Dispatcher.pop(uid, null, ErrorCode.GAME.PARAM_ERR);
    }

    skillId = Number(skillId);
    if (isNaN(skillId)) {
        return Dispatcher.pop(uid, null, ErrorCode.GAME.PARAM_ERR);
    }

    BaseController.loadRole(uid, args['token'], (err, role:Role) => {
        if(err)
        {
            return Dispatcher.pop(uid, null, err);
        }
        var oldHeroInfo = role.getHeroInfo();
        var oldSoldierList = role.getSoldierList();
        var err = role.skillUp(skillId);
        if (err) {
            return Dispatcher.pop(uid, null, err);
        }
        role.save(()=> {
        }, false, true);

        var newHeroInfo = role.getHeroInfo();
        var newSoldierList = role.getSoldierList();
        var data = {};
        var skillList = role.get('skillList');
        var skillLv = skillList[skillId].lv;
        data[skillId] = [];

        var skillData = role.getSkillInfoByLv(skillId, skillLv);
        data[skillId].push(skillData);
        if(skillLv + 1 <= skillData.maxLv){
            data[skillId].push(role.getSkillInfoByLv(skillId, skillLv + 1));
        }

        var resData:any = {};
        resData.lv = role.get('skillList')[skillId].lv;
        resData.yxb = role.get('yxb');
        resData.skillList = data;
        resData.skillId = skillId;

        if (!role.equalHeroInfo(oldHeroInfo, newHeroInfo)) {
            resData.heroInfo = newHeroInfo;
        }

        if (!role.equalSoldierList(oldSoldierList, newSoldierList)) {
            resData.soldierList = newSoldierList;
        }

        return Dispatcher.pop(uid, resData);
    });
}

// 士兵升级
export function soldierUp(uid, args):void {
    //var self = this;
    var sid = args.soldierId;
    var type = args.type;
    if (typeof sid === 'undefined'
        || typeof type === 'undefined') {
        return Dispatcher.pop(uid, null, ErrorCode.GAME.PARAM_INVALID);
    }

    sid = Number(sid);
    type = Number(type);
    if (isNaN(sid) || isNaN(type)) {
        return Dispatcher.pop(uid, null, ErrorCode.GAME.PARAM_INVALID);
    }

    BaseController.loadRole(uid, args['token'], (err, role:Role) => {
        if(err)
        {
            return Dispatcher.pop(uid, null, err);
        }
        var times = 0;
        if (type === 0) {
            times = 1;
        } else if (type === 1) {
            times = 10;
        } else if (type === 2) {
            times = 100;
        } else if (type === 3) {
            times = 1000;
        }
        var soldierList = role.get('soldierList');
        var soldier = soldierList[sid];
        if (typeof soldier === 'undefined') {
            return Dispatcher.pop(uid, null, ErrorCode.GAME.SOLDIER_NOT_EXIST);
        }

        var oldHeroInfo = role.getHeroInfo();
        var oldLv = soldier.lv;
        for (var i = 0; i < times; i++) {
            role.soldierUp(sid);
        }
        var newLv = soldier.lv;
        role.save(()=> {
        }, false, true);
        var data = {};
        data[sid] = [];
        data[sid].push(role.getSoldierInfoByLv(sid, newLv));
        if(newLv + 1 <= GameConst.heroMaxLv){
            data[sid].push(role.getSoldierInfoByLv(sid, newLv + 1));
        }

        var nextSid = sid + 1;
        if(nextSid === 36) // 特殊处理一下.. 36 是 天降神兵
        {
            nextSid = 37;
        }
        // 开启下一关士兵
        if (oldLv === 0 && newLv != 0 && typeof soldierList[nextSid] !== 'undefined') {
            data[nextSid] = [];
            data[nextSid].push(role.getSoldierInfoByLv(nextSid, 0));
            data[nextSid].push(role.getSoldierInfoByLv(nextSid, 1));
        }

        var resData:any = {};
        resData.yxb = role.get('yxb');
        resData.soldierList = data;
        var newHeroInfo = role.getHeroInfo();
        if (!role.equalHeroInfo(oldHeroInfo, newHeroInfo)) {
            resData.heroInfo = newHeroInfo;
        }

        return Dispatcher.pop(uid, resData);
    });
}

export function unlockFloor(uid, args):void {
    BaseController.loadRole(uid, args['token'], (err, role:Role) => {
        if(err)
        {
            return Dispatcher.pop(uid, null, err);
        }
        var maxFloor = role.get('maxFloor');
        if(maxFloor % 10 != 0)
        {
            //return ErrorCode.GAME.JUMP_CONDITION_NOT_FIT;
            return Dispatcher.pop(uid, null, ErrorCode.GAME.JUMP_CONDITION_NOT_FIT);
        }

        var unlockYxb = role.getUnlockYxb(maxFloor);
        if(unlockYxb === 0){
            return Dispatcher.pop(uid, null, ErrorCode.GAME.NOT_ENOUGH_YXB_SHOW);
        }

        if (role.reduceYxb(unlockYxb) === false) {
            //return ErrorCode.GAME.NOT_ENOUGH_YXB_SHOW;
            return Dispatcher.pop(uid, null, ErrorCode.GAME.NOT_ENOUGH_YXB_SHOW);
        }

        var nextUnlockYxb = role.getUnlockYxb(maxFloor + 10);
        if(nextUnlockYxb === 0)
        {
            log.sWarn('%d had come through all floor %d', this.data.uid, maxFloor + 10);
        }

        role.add('maxFloor', 1);
        role.set('highestfloorclear', 0);
        role.save(()=> {}, false, true);

        maxFloor ++;
        var resData = {
            maxFloor: role.get('maxFloor'),
            highestfloorclear: role.get('highestfloorclear'),
            battleField: role.getBattleFiled(maxFloor),
            nextUnlockYxb: nextUnlockYxb
        };

        return Dispatcher.pop(uid, resData);
    });
}

// 获取商城信息
export function openShop(uid, args):void {
    //var self = this;

    BaseController.loadRole(uid, args['token'], (err, role:Role) => {
        if(err)
        {
            return Dispatcher.pop(uid, null, err);
        }
        var productInfo = role.getProductInfo();
        return Dispatcher.pop(uid, productInfo);
    });
}

// 购买商品
export function shopBuy(uid, args):void {
    //var self = this;
    var productId = args.productId;
    var count = args.count;
    if (typeof productId === 'undefined'
        && typeof count === 'undefined') {
        return Dispatcher.pop(uid, null, ErrorCode.GAME.PARAM_ERR);
    }

    productId = Number(productId);
    count = Number(count);
    if (isNaN(productId)) {
        return Dispatcher.pop(uid, null, ErrorCode.GAME.PARAM_INVALID);
    }

    if (isNaN(count)) {
        count = 1;
    }

    BaseController.loadRole(uid, args['token'], (err, role:Role) => {
        if(err)
        {
            return Dispatcher.pop(uid, null, err);
        }
        if (productId === 4) {// super hero
            if (!role.isDemonClickCoolDown()) {
                return Dispatcher.pop(uid, null, ErrorCode.GAME.DEMON_CLICK_NOT_COOL_DOWN);
            }
            return pay(role, afterPay.bind(null, err, role));
        } else {
            return pay(role, afterPay.bind(null, err, role));
        }
    });

    // 付费
    function pay(role:Role, callback) {
        //var price = 0;
        var productConf = configMgr.shopdb.get(productId);
        var price = productConf.diamond;
        if(typeof productConf === 'undefined')
        {
            return callback(ErrorCode.GAME.PRODUCT_NOT_EXIST, null);
        }
        else
        {
            if (productId === 5) {//skill cd clear
                var skillList = role.get('skillList');
                var cdSkillCount = 0;
                for (var skillId in skillList) {
                    var skillInfo = skillList[skillId];
                    if (skillInfo.lv > 0 && !role.isSkillCooldown(skillId)) {
                        cdSkillCount++;
                    }
                }

                price = cdSkillCount * price;
                if (price == 0) {
                    return callback(ErrorCode.GAME.SKILL_ALL_COOL_DOWN, null);
                }
            }
        }

        var success = role.reduceGold(price,
            GameConst.ADD_DIAMOND_REASON.PRODUCT);
        if (success !== true) {
            return callback(ErrorCode.GAME.NOT_ENOUGH_GOLD, null);
        }

        return callback(ErrorCode.NO_ERROR, role);
    }

    // 付费后处理
    function afterPay(err, role:Role) {
        if (err) {
            return Dispatcher.pop(uid, null, err);
        }

        if (productId === 2) {
            var shopYxb = role.getProductInfo().shopYxb;
            role.addYxb(shopYxb);
            role.add('buyYxb', 1);
            log.uInfo(role.data.uid, 'shopBuy yxb success = %d', shopYxb);
        } else if(productId === 3){
            var shopSuperYxb = role.getProductInfo().shopSuperYxb;
            role.addYxb(shopSuperYxb);
            role.add('buyYxb', 10);
            log.uInfo(role.data.uid, 'shopBuy super yxb success = %d', shopSuperYxb);
        }
        else if (productId === 4) {
            role.useDemonClick();
            //role.save();
            role.add('callPeri', 1);
            log.uInfo(role.data.uid, 'shopBuy super useDemon success');
        } else if (productId === 5) {
            var skillList = role.get('skillList');
            for (var skillId in skillList) {
                var skillInfo = skillList[skillId];
                skillInfo.lastTime = 0;
            }
            role.set('skillList', skillList);
            log.uInfo(role.data.uid, 'shopBuy clear skill success');
        } else {
            return Dispatcher.pop(uid, null, ErrorCode.GAME.PRODUCT_NOT_EXIST);
        }

        role.save(()=> {
        }, false, true);

        var resData = {
            gold: role.get('gold'),
            yxb: role.get('yxb'),
            productId: productId,
            callPeri: role.get('callPeri'),
            buyYxb: role.get('buyYxb')
        };

        return Dispatcher.pop(uid, resData);
    }
}

export function getFirstAward(uid, args):void {
    //var self = this;

    BaseController.loadRole(uid, args['token'], (err, role:Role) => {
        if(err)
        {
            return Dispatcher.pop(uid, null, err);
        }
        if (role.get('isFirstPay')) {
            return Dispatcher.pop(uid, null, ErrorCode.GAME.ALREADY_GET_FIRST_PAY);
        }

        if (role.get('totalRmbPay') <= 0 && !role.isMonthGiftIn()) {
            return Dispatcher.pop(uid, null, ErrorCode.GAME.NO_PAY);
        }

        role.add('gold', 1000);
        role.dropWeapon(25);// chiyanzhandao
        role.set('isFirstPay', 1);
        //role.set('rmbPay', 0);//首充的话自己清rmbPay

        role.save(()=> {
        }, false, true);

        var data = {
            'gold': role.get('gold')
        };

        var syncWeaponData = role.genDropWeaponSyncData();

        for (var key in syncWeaponData) {
            data[key] = syncWeaponData[key];
        }

        return Dispatcher.pop(uid, data);
    });
}

// 使用技能
export function useSkill(uid, args):void {
    var self = this;
    var skillId = args.skillId;

    if (typeof skillId === 'undefined') {
        return Dispatcher.pop(uid, null, ErrorCode.GAME.PARAM_ERR);
    }

    skillId = Number(skillId);
    if (isNaN(skillId)) {
        return Dispatcher.pop(uid, null, ErrorCode.GAME.PARAM_INVALID);
    }

    BaseController.loadRole(uid, args['token'], (err, role:Role) => {
        if(err)
        {
            return Dispatcher.pop(uid, null, err);
        }
        var err = role.useSkill(skillId);
        if (err) {
            return Dispatcher.pop(uid, null, err);
        }

        role.save(()=> {
        }, false, true);
        var skillList = role.get('skillList');
        var data = {
            skillId: skillId,
            lastTime: Time.realNow(),
        };

        return Dispatcher.pop(uid, data);
    });
}

export function achieveAward(uid, args):void {
    //var self = this;
    var achId = args.achId;
    if (typeof achId === 'undefined') {
        return Dispatcher.pop(uid, null, ErrorCode.GAME.PARAM_ERR);
    }

    achId = Number(achId);
    if (isNaN(achId)) {
        return Dispatcher.pop(uid, null, ErrorCode.GAME.PARAM_INVALID);
    }

    var needId = args.needId;
    needId = Number(needId);

    BaseController.loadRole(uid, args['token'], (err, role:Role) => {
        if(err)
        {
            return Dispatcher.pop(uid, null, err);
        }
        var err = role.getAchPrize(achId);
        if (err) {
            return Dispatcher.pop(uid, null, err);
        }

        var data = {
            achBitset: role.get('achiAwardBit'),
            achId: achId,
            needId: needId,
            yxb: role.get('yxb'),
            gold: role.get('gold')
        };

        //var syncWeaponData = role.genDropWeaponSyncData();
        //
        //for (var key in syncWeaponData) {
        //    data[key] = syncWeaponData[key];
        //}

        role.save(()=> {
        }, false, true);

        return Dispatcher.pop(uid, data);
    });
}

export function luckDraw(uid, args):void {
    //var self = this;
    var type = args.type;

    type = Number(type);
    if (isNaN(type)) {
        type = 0;
    }

    BaseController.loadRole(uid, args['token'], (err, role:Role) => {
        if(err)
        {
            return Dispatcher.pop(uid, null, err);
        }
        var price = 0;
        var times = 0;
        if (type == 0) {
            price = 300;
            times = 1;
        } else {
            price = 3000;
            times = 12;
        }

        if(GameUtil.inActivity()){
            price *= 0.8;
        }

        var success = role.reduceGold(price,
            GameConst.ADD_DIAMOND_REASON.DRAW);
        if (!success) {
            return Dispatcher.pop(uid, null, ErrorCode.GAME.NOT_ENOUGH_GOLD);
        }

        for (var i = 0; i < times; i++) {
            //12抽活动
            if(i === 0 && times == 12){
                role.diamondDrawWeapon(true);
            }
            else{
                role.diamondDrawWeapon(false);
            }
        }

        var data = {
            gold: role.get('gold'),
            drawEffectTime: role.get('drawEffectTime'),
            drawEffectCount: role.get('drawEffectCount')
            //drawList: role.get('drawList')
        };

        var syncWeaponData = role.genDropWeaponSyncData();

        for (var key in syncWeaponData) {
            data[key] = syncWeaponData[key];
        }

        role.save(()=> {
        }, false, true);
        return Dispatcher.pop(uid, data);
        //if (syncWeaponData.battleField) {
        //    // 怪物有更新，需要更新战场
        //    var battle = new Battle(role);
        //    battle.load(function (err, isExist) {
        //        battle.updateMonster();
        //        battle.save();
        //        return Dispatcher.pop(uid, data);
        //    });
        //} else {
        //    return Dispatcher.pop(uid, data);
        //}
    });
}

// 领取关注奖励
export function getAttentionAward(uid, args) {
    BaseController.loadRole(uid, args['token'], (err, role:Role) => {
        if(err)
        {
            return Dispatcher.pop(uid, null, err);
        }
        //return Dispatcher.pop(uid, null, ErrorCode.GAME.PARAM_INVALID);
        var isAttentionAward = role.get('isAttentionAward');
        if (isAttentionAward) {
            return Dispatcher.pop(uid, null, ErrorCode.GAME.ALREADY_GET_FOCUS_PRIZE);
        }

        var isAttention = role.get('isAttention');
        //var uid = role.get('uid');
        if (!isAttention) {
            return Dispatcher.pop(uid, null, ErrorCode.GAME.NOT_FOCUS);
        }

        var addGold = 300;
        role.set('isAttentionAward', 1);
        role.addGold(addGold, GameConst.ADD_DIAMOND_REASON.FOCUS, '0', function () {
        });
        role.save(()=> {
        }, false, true);

        var data = {
            //error: ErrorCode.NO_ERROR,
            addGold: addGold,
            gold: role.get('gold')
        };

        return Dispatcher.pop(uid, data);
    });
}

// 领取分享奖励
export function getShareAward(uid, args) {
    BaseController.loadRole(uid, args['token'], (err, role:Role) => {
        if(err)
        {
            return Dispatcher.pop(uid, null, err);
        }
        //return Dispatcher.pop(uid, null, ErrorCode.NO_ERROR);
        var dayShare = role.get('dayShare');
        var shareAmount = role.get('shareAmount');
        var lastShareTime = role.get('lastShareTime');
        var now = Time.realNow();

        if (lastShareTime + 90*60 > now) {
            return Dispatcher.pop(uid, null, ErrorCode.GAME.SHARE_NOT_COOL_DOWN);
        }
        if (dayShare >= 3) {
            return Dispatcher.pop(uid, null, ErrorCode.GAME.SHARE_EXCEED_DAILY_LIMIT);
        }

        var addList = [200, 100, 100];
        var addGold = addList[dayShare];
        role.addGold(addGold, GameConst.ADD_DIAMOND_REASON.SHARE, '0', function () {
        });

        dayShare++;
        shareAmount++;
        lastShareTime = now;
        role.set('dayShare', dayShare);
        role.set('shareAmount', shareAmount);
        role.set('lastShareTime', lastShareTime);
        role.save(()=> {
        }, false, true);

        var data = {
            addGold: addGold,
            gold: role.get('gold'),
            yxb: role.get('yxb'),
            dayShare: dayShare,
            lastShareTime: lastShareTime,
            shareAmount: shareAmount
        };

        return Dispatcher.pop(uid, data);
    });
}

export function getMonthAward(uid, args){
    BaseController.loadRole(uid, args['token'], (err, role:Role) => {
        if(err)
        {
            return Dispatcher.pop(uid, null, err);
        }

        role.awardMonthGift((err, data) =>{
            if(err){
                return Dispatcher.pop(uid, null, err);
            }
            role.save(()=> {
            }, false, true);
            return Dispatcher.pop(uid, data);
        });
    });
}

export function getCDKeyAward(uid, args){
    BaseController.loadRole(uid, args['token'], (err, role:Role) => {
        if(err)
        {
            return Dispatcher.pop(uid, null, err);
        }

        args['uid'] = uid;
        role.awardCDKeyGift(args, (err, data) =>{
            if(err){
                return Dispatcher.pop(uid, null, err);
            }
            role.save(()=> {
            }, false, true);
            return Dispatcher.pop(uid, data);
        });
    });
}

//}
//
//export = GameController;