/**
 * Created by showbu on 15-12-28.
 */
import log = require('../../util/log');
import Role = require('../role');
import Dispatcher = require('../../http/dispatcher');
import RedisMgr = require('../../redis/redis_mgr');
import WorldDB = require('../../database/impl/world_db');
import async = require('async');
import GameUtil = require('../../util/game_util');
import GameConst = require('../game_const');

var roleRedis = RedisMgr.getInstance('roleRedis');

export function addParmToPlayer(invoke:Dispatcher.Invoke):void{
    var args = invoke.req.query;
    var parm = args['parm'];
    var add = args['add'];
    var uid = args['uid'];

    add = Number(add);

    async.series(
        {
            'one': addParamInDBWithUid.bind(null, uid, parm, add),
            'two': addParamInRedisWithUid.bind(null, uid, parm, add)
        },
        function (err, result){
            if(err){
                return invoke.res.json('FAIL = ' + err);
            }

            return invoke.res.json('addParmToPlayer SUCCESS!');
        });
}

export function addParm(invoke:Dispatcher.Invoke):void{
    var args = invoke.req.query;
    var parm = args['parm'];
    var add = args['add'];
    add = Number(add);

    async.series(
        {
            'one': addParamWithDB.bind(null, parm, add),
            'two': addParamWithRedis.bind(null, parm, add)
        },
        function (err, result){
            if(err){
                return invoke.res.json('FAIL = ' + err);
            }

            return invoke.res.json('addParm SUCCESS!');
        });
}

export function addParamWithDB(parm, add, cb){
    var tableNameList:any = {};
    for(var i=0; i<10; i++){
        tableNameList[i] = 'player_info_' + i;
    }

    async.each(tableNameList, function(tableName, callback){
        WorldDB.conn.execute('update ' + tableName + ' set ' + parm + ' = ' + parm + '+' + add, [], (err, connection, result)=> {
            if (err) {
                return callback(err);
            }
            if (!result || result.length === 0) {
                return callback(err);
            }
            log.sInfo('UPDATE %s affectedRows = %s', tableName, result.affectedRows);
            return callback(null);
        });
    },function(err){
        if(err){
            log.sInfo('UPDATE player error = ', err);
            return cb(err, 'one');
        }
        return cb(null, 'one');
    });
}

export function addParamWithRedis(parm, add, cb){
    roleRedis.keys(['role:*'], (err, reply)=>{
        if(err){
            return cb(err);
        }
        async.each(reply, function(roleName, callback){
            var roleKey:string = roleName.toString();
            roleRedis.hmget(roleKey, ['uid', parm], (err, reply)=> {
                if(err){
                    return callback(err);
                }
                if(typeof reply[parm] === 'undefined'){
                    return callback(new Error('reply[parm] undefined'));
                }
                var orgValue = Number(reply[parm]);
                if(isNaN(orgValue)){
                    return callback(new Error('isNaN(orgValue)'));
                }
                var saveData:{[key: string]:any} = {};
                saveData[parm] = orgValue + add;

                roleRedis.hmset(roleKey, saveData, 3600, (err)=> {
                    if(err) {
                        return callback(err);
                    }
                    log.sInfo('redis update %s [%s] OK = %s', roleKey, parm, saveData[parm]);
                    return callback(null);
                });
            });
        },function(err){
            if(err){
                log.sInfo('addParamWithRedis error = ', err);
                return cb(err, 'two');
            }
            return cb(null, 'two');
        });
    });
}

export function addParamInDBWithUid(uid, parm, add, cb){
    WorldDB.conn.execute('update player_info_' + GameUtil.getTableNum(uid) + ' set ' + parm + ' = ' + parm + '+' + add + ' where ?', {uid: uid}, (err, connection, result)=> {
        if (err) {
            return cb(err);
        }
        if (!result || result.length === 0) {
            return cb(err);
        }
        log.sInfo('UPDATE player_info_%d affectedRows = %s', GameUtil.getTableNum(uid), result.affectedRows);
        return cb(null);
    });
}

export function addParamInRedisWithUid(uid, parm, add, cb){
    //roleRedis.keys(['role:*'], (err, reply)=>{
    //    if(err){
    //        return cb(err);
    //    }
    //    async.each(reply, function(roleName, callback){
    var roleKey:string = 'role:'+uid;
    roleRedis.hmget(roleKey, ['uid', parm], (err, reply)=> {
        if(err){
            return cb(err);
        }
        if(typeof reply[parm] === 'undefined'){
            return cb(new Error('reply[parm] undefined'));
        }
        var orgValue = Number(reply[parm]);
        if(isNaN(orgValue)){
            return cb(new Error('isNaN(orgValue)'));
        }
        var saveData:{[key: string]:any} = {};
        saveData[parm] = orgValue + add;

        roleRedis.hmset(roleKey, saveData, 3600, (err)=> {
            if(err) {
                return cb(err);
            }
            log.sInfo('redis update %s [%s] OK = %s', roleKey, parm, saveData[parm]);
            return cb(null);
        });
    });
        //}
        //,function(err){
        //    if(err){
        //        log.sInfo('addParamWithRedis error = ', err);
        //        return cb(err, 'two');
        //    }
        //    return cb(null, 'two');
        //});
    //});
}

export function resetRank(invoke:Dispatcher.Invoke):void {
    var args = invoke.req.query;
    //var rankName = args.rankName;
    var rankName = GameConst.RANK_KEY_NAME;

    var tableNameList:any = {};
    for(var i=0; i<10; i++){
        tableNameList[i] = 'player_info_' + i;
    }

    async.each(tableNameList, function(tableName, callback){
        WorldDB.conn.execute('select * from ' + tableName, [], (err, connection, result)=> {
            if (err) {
                return callback(err);
            }
            if (!result || result.length === 0) {
                return callback(err);
            }

            async.each(result, function(data:any, callback){
                    var userInfo = {
                        uid:0,
                        buildList:{},
                        maxFloor:1
                    }

                    GameUtil.deserialize(userInfo, data);
                    if(userInfo.uid === 0){
                        return callback(new Error('userInfo.uid === 0'));
                    }

                    var maxFloor = userInfo.maxFloor;
                    if(maxFloor < 5){
                        return callback(null);
                    }
                    else{
                        //var monsterBornMedal = userInfo.monsterBornMedal;
                        var timeDiff = ((new Date('2020-01-01 00:00:00').getTime()) - (new Date().getTime())) / 1000;
                        //var score = Math.floor(monsterBorn + timeDiff + monsterBornMedal * GameConst.monsterBornMax)
                        var score = Math.floor(maxFloor * Math.pow(10, 10) + timeDiff);

                        var scoreData = [rankName, score, userInfo.uid];

                        // 添加排行
                        roleRedis.zadd(scoreData, 0, (err) =>{
                            if (err) {
                                log.sError('rank add failed, ' + err.stack);
                                return callback(err);
                            }
                            return callback(null);
                        });
                    }
                },function(err){
                    if(err){
                        return callback(err);
                    }
                    return callback(null);
                }
            );
        });
    },function(err){
        if(err){
            log.sInfo('UPDATE player error = ', err);
            return invoke.res.json('FAIL = ' + err);
        }
        return invoke.res.json('resetRank SUCCESS');
    });
}

export function saveRedisToDB(invoke:Dispatcher.Invoke):void {
    roleRedis.keys(['role:*'], (err, reply)=>{
        if(err){
            return invoke.res.json('FAIL = ' + err);
        }
        async.each(reply, function(roleName, callback){
            var roleKey:string = roleName.toString();
            var roleArr = roleKey.split(':');
            if(roleArr[1] === 'undefined'){
                return callback(new Error('roleArr[1] undefined'));
            }

            var uid:number = Number(roleArr[1]);
            if(isNaN(uid)){
                return callback(new Error('isNaN(uid)'));
            }
            var role = new Role(uid, '');
            role.load((err, isExist)=>{
                if(err || !isExist) {
                    callback(new Error('role not exist!'));
                }
                else {
                    role.save(()=> {}, false, true);
                    callback(null);
                }
            });
        },function(err){
            if(err){
                log.sInfo('addParamWithRedis error = ', err);
                return invoke.res.json('FAIL = ' + err);
            }
            return invoke.res.json('saveRedisToDB SUCCESS!');
        });
    });
}

//export function getUnlockBuildLength(buildList):number {
//    var count = 0;
//    for (var bid in buildList) {
//        var build = buildList[bid];
//        if (build.lv == 0) {
//            break;
//        }
//        count ++;
//    }
//    return count;
//}