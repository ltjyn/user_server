/**
 * Created by showbu on 15-11-21.
 */

import GameConst = require('./game_const');
import ErrorCode = require('../util/error_code');
import GameUtil = require('../util/game_util');

var configMgr = require('../config').configMgr;

class GameData {
    static weaponList:any = {};
    static skillList:any = {};
    static role:any;

    public static getSkillLv(skillId):number {
        var lv = 0;
        if (this.skillList[skillId] !== 'undefined') {
            lv = this.skillList[skillId].lv;
        }
        return lv;
    }

    // 获取所有士兵的攻击力总和
    public static getSoldierAttackSum():any {
        var soldierList = this.role.get('soldierList');
        var sum = 0;
        for (var sid in soldierList) {
            var soldier = soldierList[sid];
            if (soldier.lv == 0) {
                continue;
            }
            var ret = this.getSoldierData(sid, soldier.lv);
            sum += ret.atk;
        }
        return sum;
    }

    // 获取勇士数值
    public static getHeroData(lv, soldierAtk):any
    {
        lv = Number(lv);
        var num = lv;
        var num2 = lv + 1;

        //var maxHeroLv = Object.keys(configMgr.hero_basicdb.all()).length;
        if(num2 > GameConst.heroMaxLv) num2 = GameConst.heroMaxLv;

        var curBasicData = configMgr.hero_basicdb.get(num);
        var nextBasicData = configMgr.hero_basicdb.get(num2);
        var curHeroData = configMgr.herodb.get(1);//hero id = 1;

        var hpUp = 0;
        var atkUp = 0;
        var atkSumUp = 0;
        var appearTime = 0;
        var speedUp = 0;
        var lv_up_gold_up = 0;

        var skillLvStrength = this.getSkillLv(4);
        if(skillLvStrength > 0)
        {
            var ret = this.getSkillData(4, skillLvStrength)//liliang
            //if(typeof ret === 'ErrorCode'){
            //    return ret;
            //}

            hpUp += (ret.count_1 * 0.01);//hp
            atkUp += (ret.count_2 * 0.01);//atk
        }

        var skillLvHonor = this.getSkillLv(5);
        if(skillLvHonor > 0)
        {
            ret = this.getSkillData(5, skillLvHonor)//rongyu
            //if(typeof ret === 'ErrorCode'){
            //    return ret;
            //}
            var atkParam = (ret.count_1 * 0.01);//atk sum

            //var sum = this.getSoldierAttackSum();
            //if(typeof sum === 'ErrorCode'){
            //    return sum;
            //}
            atkSumUp = atkParam * soldierAtk;
        }

        hpUp += (this.getWeaponPara(4) * 0.01 + this.getWeaponPara(6) * 0.01 + this.getWeaponPara(23) * 0.01);
        atkUp += (this.getWeaponPara(5) * 0.01 + this.getWeaponPara(7) * 0.01 + this.getWeaponPara(24) * 0.01);
        speedUp += this.getWeaponPara(10) * 0.01;
        lv_up_gold_up += (this.getWeaponPara(26)) * 0.01;

        var heroData:any={};
        heroData.lv = lv;
        heroData.hp = Math.ceil(curBasicData.hp * curHeroData.hp * (1 + hpUp));
        heroData.atk = Math.ceil(curBasicData.attack * curHeroData.attack * (1 + atkUp) + atkSumUp); // 原始攻击力+荣誉技能加成
        heroData.speed = Math.ceil(curHeroData.speed * (1 + speedUp)); // 移动速度
        if(heroData.speed > curHeroData.max_speed)
        {
            heroData.speed = curHeroData.max_speed;
        }

        heroData.nextCoin = Math.ceil(nextBasicData.lv_up_gold * (1 - lv_up_gold_up)); // 升到lv+1需要多少金币
        heroData.time = Math.ceil(curHeroData.time * (1 - appearTime)); // 升到lv需要多少金币
        if(heroData.time < curHeroData.max_time)
        {
            heroData.time = curHeroData.max_time;
        }

        return heroData;
    }

    // 获取士兵数值
    public static getSoldierData(id, lv):any
    {
        lv = Number(lv);
        var num = lv == 0 ? 1 : lv;
        var num2 = lv + 1;
        //var maxHeroLv = Object.keys(configMgr.hero_basicdb.all()).length;
        if(num2 > GameConst.heroMaxLv) num2 = GameConst.heroMaxLv;

        var curBasicData = configMgr.hero_basicdb.get(num);
        var nextBasicData = configMgr.hero_basicdb.get(num2);

        var curHeroData = configMgr.herodb.get(id);//

        var hpUp = 0;
        var atkUp = 0;
        var atkSumUp = 0;
        var appearTime = 0;
        var speedUp = 0;
        var lv_up_gold_up = 0;

        var skillLvStrength = this.getSkillLv(4);
        if(skillLvStrength > 0)
        {
            var ret = this.getSkillData(4, skillLvStrength)//liliang
            //if(typeof ret === 'ErrorCode'){
            //    return ret;
            //}

            hpUp += (ret.count_1 * 0.01);//hp
            atkUp += (ret.count_2 * 0.01);//atk
        }

        var skillLvAppear = this.getSkillLv(3);
        if(skillLvAppear > 0)
        {
            var ret = this.getSkillData(3, skillLvAppear);
            //if(typeof ret === 'ErrorCode'){
            //    return ret;
            //}
            appearTime += (ret.count_1 * 0.01);//time
        }

        hpUp += (this.getWeaponPara(4) * 0.01 + this.getWeaponPara(6) * 0.01 + this.getWeaponPara(23) * 0.01);
        atkUp += (this.getWeaponPara(5) * 0.01 + this.getWeaponPara(7) * 0.01 + this.getWeaponPara(24) * 0.01);
        speedUp += this.getWeaponPara(10) * 0.01;
        lv_up_gold_up += (this.getWeaponPara(27)) * 0.01;
        if(curHeroData.pos == 1)
        {
            hpUp += this.getWeaponPara(1) * 0.01;
            atkUp += this.getWeaponPara(1) * 0.01;
        }
        else if(curHeroData.pos == 2)
        {
            hpUp += this.getWeaponPara(2) * 0.01;
            atkUp += this.getWeaponPara(2) * 0.01;
        }
        else if(curHeroData.pos == 3)
        {
            hpUp += this.getWeaponPara(3) * 0.01;
            atkUp += this.getWeaponPara(3) * 0.01;
        }

        if(num > 1000)
        {
            atkUp += this.getWeaponPara(21) * 0.01;
            hpUp += this.getWeaponPara(22) * 0.01;
        }

        var soldierData:any={};
        soldierData.lv = lv;
        soldierData.hp = Math.ceil(curBasicData.hp * curHeroData.hp * (1 + hpUp));
        soldierData.atk = Math.ceil(curBasicData.attack * curHeroData.attack * (1 + atkUp) + atkSumUp); // 原始攻击力+荣誉技能加成
        soldierData.speed = Math.ceil(curHeroData.speed * (1 + speedUp)); // 移动速度
        if(soldierData.speed > curHeroData.max_speed)
        {
            soldierData.speed = curHeroData.max_speed;
        }

        soldierData.nextCoin = Math.ceil(curHeroData.lv_up_gold * nextBasicData.lv_up_gold * (1 - lv_up_gold_up)); // 升到lv+1需要多少金币
        soldierData.time = curHeroData.time * (1 - appearTime); // 升到lv需要多少金币
        if(soldierData.time < curHeroData.max_time)
        {
            soldierData.time = curHeroData.max_time;
        }
        soldierData.pos = curHeroData.pos;

        soldierData.gold = curHeroData.nature_num;

        //cb(ErrorCode.NO_ERROR, soldierData);
        return soldierData;
    }

    // 获取技能数值
    public static getSkillData(skillID, lv):any
    {
        skillID = Number(skillID);
        if(skillID < 1) {
            skillID = 1;
        }

        lv = Number(lv);
        if(lv < 0)
        {
            lv = 0;
        }

        var skillConfId = lv;
        if(skillID > 1 && lv >= 1)
        {
            skillConfId --;
        }

        var maxlv = 0;

        if(typeof(GameConst.SKILL_DATA_MAXLEN[skillID]) !== 'undefined')
        {
            maxlv = GameConst.SKILL_DATA_MAXLEN[skillID];
            if(skillID == 1) maxlv ++;
        }

        var preSkillID = skillID - 1;
        var preMaxLv = 0;
        var skillIdx = 1;
        for(var i = preSkillID; i >= 1; i --)
        {
            preMaxLv = GameConst.SKILL_DATA_MAXLEN[i];
            if(typeof preMaxLv === 'undefined')
            {
                preMaxLv = 0;
            }
            else
            {
                if(i == 1) preMaxLv ++;
            }

            skillIdx += preMaxLv;
        }
        skillIdx += skillConfId;
        var skillConf = configMgr.skilldb.get(skillIdx);

        //var coin = 0.0;
        var num8 = skillConf.count_1;
        var num9 = skillConf.count_2;
        var cd = skillConf.cd;
        var time = skillConf.time;

        var param3 = 0;
        var param4 = 0;

        if(skillID == 1)//zhaohuan
        {
            var param1 = Math.ceil(this.getWeaponPara(25));//count_1
            num8 += param1;
        }
        else if(skillID == 9) //chiyan
        {
            var param1 = Math.ceil(this.getWeaponPara(31));//count_1
            num8 += param1;

            var param2 = Math.ceil(this.getWeaponPara(32));//count_2
            num9 += param2;

            param3 = Math.ceil(this.getWeaponPara(33));//cd
            param4 = Math.ceil(this.getWeaponPara(20));//time
        }
        else if(skillID == 8)//zongzhumiling
        {
            param3 = Math.ceil(this.getWeaponPara(30));//cd
            param4 = Math.ceil(this.getWeaponPara(19));//time
        }
        else if(skillID == 7)//qiandaizi
        {
            var param1 = Math.ceil(this.getWeaponPara(17));//count_1 & count_2
            num8 = Math.ceil(num8 * (1 + param1 * 0.01));
            num9 = Math.ceil(num9 * (1 + param1 * 0.01));

            param3 = Math.ceil(this.getWeaponPara(29));//cd
            param4 = Math.ceil(this.getWeaponPara(18));//time
        }
        else if(skillID == 6)//jixingjun
        {
            var param1 = Math.ceil(this.getWeaponPara(15));//count_1 & count_2
            num8 = Math.ceil(num8 * (1 + param1 * 0.01));
            num9 = Math.ceil(num9 * (1 + param1 * 0.01));

            param3 = Math.ceil(this.getWeaponPara(28));//cd
            param4 = Math.ceil(this.getWeaponPara(16));//time
        }

        cd -= param3;
        if(cd < 1)
        {
            cd = 1;
        }

        time += param4;
        if(time > 180)
        {
            time = 180;
        }

        var data = {
            skillID : skillID, // 技能id
            lv : lv,
            count_1 : Math.ceil(num8 * 10) / 10,
            count_2 : Math.ceil(num9 * 10) / 10,
            yxb : Math.ceil(skillConf.gold),
            needFloor : skillConf.need_plies,
            needLv : skillConf.need_lv,
            needSkillID : skillConf.need_skill_id,
            maxLv : maxlv,
            time : time,
            cd : cd,
        };

        return data;
    }

    public static getWeaponPara(itemID:number):number {
        var parame = this.getDataFromItemID(itemID);
        var lv = parame.lv;
        var f:number = 0;

        if(lv > 0)
        {
            f = parame.param;
        }

        return f;
    }

    public static getDataFromItemID(itemID:number):any {
        var lv = 0;
        if (typeof this.weaponList !== 'undefined') {
            var weaponLv = this.weaponList[itemID];
            if (typeof weaponLv !== 'undefined') {
                lv = weaponLv;
            }
        }

        var weaponConfig = configMgr.itemdb.get(itemID);

        var data = {
            itemID : itemID,
            lv : lv,
            type : weaponConfig.type,
            param : Math.ceil((weaponConfig.count + lv*weaponConfig.lv_up) * 10) / 10,
            maxlv : weaponConfig.max_lv
        };

        return data;
    }

    public static getOfflineCoin(offlineTime:number, maxFloor:number):number
    {
        //if(offlineTime > 28800) offlineTime = 28800;

        var minutes = Math.ceil(offlineTime / 60);
        var coin = this.getCoin(maxFloor, false, false) * minutes;

        return coin;
    }

    public static getCoin(floor, isTreasure, itemBouns):number
    {
        floor = Number(floor);
        floor = Math.max(floor, 1);

        var floorEnemyConfig = configMgr.monster_lvdb.get(floor);

        var baseCoin = floorEnemyConfig.gold;

        var parame:number = this.getWeaponPara(11);
        var num3 = 1 + (parame * 0.01);
        baseCoin *= num3;

        return Math.ceil(baseCoin);
    }

    public static getUnlockCoin(floor):number
    {
        floor = Math.floor(floor);
        var sealFloor:number = floor%10 == 0 ? floor/10 : floor/10 + 1;
        var index = sealFloor / 10 + 1;
        var nextStoreyConfig = configMgr.next_storeydb.get(index);

        var coin = nextStoreyConfig.gold;

        return Math.ceil(coin);
    }

    // 商店金币公式
    public static getShopCoin(maxFloor:number):number {
        var coin = Math.ceil(this.getCoin(maxFloor, false, false) * (288 + maxFloor * 1.21));
        if(coin < 300000) coin = 300000;
        return coin;
    }

    public static getSuperShopCoin(maxFloor:number):number {
        var coin = this.getShopCoin(maxFloor) * 10;
        if(coin < 300000 * 10) coin = 300000 * 10;
        return coin;
    }

    // 妖怪伤害
    public static getDemonAttack(maxFloor:number, heroLv:number) {
        var floor = Math.max(maxFloor, 1);
        var soldierAtkSum = this.getSoldierAttackSum();
        //var maxHeroLv = Object.keys(configMgr.hero_basicdb.all()).length;
        var heroData = this.getHeroData(heroLv, soldierAtkSum);
        var monsterConf = configMgr.monster_lvdb.get(floor);
        var monMaxAtk = monsterConf.att;

        var damage = Math.ceil(heroData.atk * (5 + maxFloor * 0.00125) + monMaxAtk/50);
        return damage;
    }

    public static setRole(role):void {
        this.role = role;
        this.weaponList = role.get('weaponList');
        this.skillList = role.get('skillList');
    }

    public static genTreasure(floor:number):boolean {
        //2% + 18号物品
        var prob = 0.02;
        var isTreasure = false;
        if (Math.random() < prob) {
            isTreasure = true;
        }

        if ((((floor + 1) % 5) == 0) || (floor < 0x19)) {
            isTreasure = false;
        }
        return isTreasure;
    }

    //public static makeEnemy(floor):any
    //{
    //    var num10;
    //    var num11;
    //
    //    var floorEnemyConfig = configMgr.monster_lvdb.get(floor);
    //    var formation = floorEnemyConfig.formation;
    //
    //    // 129 130号武器减少怪物血量
    //    num10 = 1 - this.getWeaponPara(34) * 0.01;
    //    num11 = 1 - this.getWeaponPara(35) * 0.01;
    //
    //    var enemy = {
    //        floor : floor,
    //        formation : formation,
    //        mosterID: floorEnemyConfig.monsterID,
    //        hp : Math.ceil(floorEnemyConfig.hp * num10),
    //        attack : Math.ceil(floorEnemyConfig.att * num11),
    //        gold : Math.ceil(floorEnemyConfig.gold * num11),
    //        bossID: floorEnemyConfig.bossID,
    //        bosshp : Math.ceil(floorEnemyConfig.bosshp * num10),
    //        bossatt : Math.ceil(floorEnemyConfig.bossatt * num11),
    //        bossgold : Math.ceil(floorEnemyConfig.bossgold),
    //        mapID : floorEnemyConfig.mapID,
    //    };
    //
    //    return enemy;
    //}

    public static getBossSkill(skillList:string):any{
        var data:{[ID:number]: any} = {};
        var bossSkill = GameUtil.stringToArray(skillList);
        if(bossSkill && bossSkill.length > 0){
            var c;
            for(c in bossSkill){
                var bossSkillConf = configMgr.boss_skilldb.get(bossSkill[c]);

                data[bossSkill[c]] = {};
                data[bossSkill[c]].type = bossSkillConf.type;
                data[bossSkill[c]].target = bossSkillConf.target;
                data[bossSkill[c]].count = bossSkillConf.count;
                data[bossSkill[c]].lv = bossSkillConf.lv;
            }
        }
        return data;
    }

    public static getCDKeyType(cdkey:string):number{
        return 0;
    }
}

export = GameData;