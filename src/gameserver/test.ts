import log = require('../util/log');
import WorldDB = require('../database/impl/world_db');
import sourceMapSupport = require('source-map-support');

var config = require('../config/untrack/gameserver.json');

// ts到js的映射预装
sourceMapSupport.install();
// 日志输出目录初始化
log.init('gameserver');
WorldDB.init(config['worlddb'], (err) => {
    if (err) {
        throw err;
    }

    var i;
    for ( i = 0; i < 100000; i++) {
        WorldDB.conn.execute('select * from player_info_0 where ?', {gold: 100}, ()=>{});
    }
});