/**
 * Created by showbu on 15-11-16.
 */

export var RANK_KEY_NAME = "lybRank";
export var BATTLE_KEY_NAME = "lyb"; // lyb battle

export var SOLDIER_ID_OFFSET = 2;

export var ADD_DIAMOND_REASON = {
    'UNKNOWN' : 0,
    'CHARGE' : 1,
    'EQUIP_UP' : 2,
    'FOCUS' : 3,
    'SHARE' : 4,
    'ACH' : 5,
    'PRODUCT' : 6,
    'DRAW' : 7,
    'DAY_TASK' : 8,
    'SOLDIER_UP' : 9
};

export var SKILL_DATA_MAXLEN = {
    '1' : 60,
    '2' : 50,
    '3' : 58,
    '4' : 50,
    '5' : 50,
    '6' : 30,
    '7' : 31,
    '8' : 30,
    '9' : 50
};

export var BATTLE = {
    HERO_ATTACK_INTERVAL : 200, // 毫秒级
    BATTLE_ALIVE_INTERVAL : 5, // 验证机制间隔秒级
    DEMON_MAX_CLICK_PER_SEC : 10, // 妖精每秒最大点击数
    MAX_DAYDROP_NUM : 2,
    MIN_DAYDROP_FLOOR : 20,
    MAX_DRAWWEAPON_EFFCOUNT : 15
};

export var GAME_ID = 2;
export var PAY_KEY = '7eac792bd64edb02106fae27727b15e8';

export var drawItemRand = [0, 0, 0, 0];//12抽的概率
export var times12DrawWeapon = [1, 2, 3];//12连抽必得装备
export var randItem:any = [4,5,6,7,12,13];//掉落装备异常时随机给的装备

export var heroMaxLv = 0;