import log = require('../util/log');
import DynObject = require('../util/dynamicobject/dyn_object')
import Time = require('../util/time');
import RedisMgr = require('../redis/redis_mgr');
import WorldDB = require('../database/impl/world_db');
import GameUtil = require('../util/game_util');

var roleRedis = RedisMgr.getInstance('roleRedis');


export interface BaseRoleRedisData {
    uid?:number;
    nickname?:string;
    headimgurl?:string;

    lastLoginTime?:number;
    lastAliveTime?:number;
    createTime?:number;

    dayShare?:number;
    shareAmount?:number;
    lastShareTime?:number;

    isAttention?:number;
    isAttentionAward?:number;

    yxb?:number;
    totalYxb?:number;
    gold?:number;
    totalRmbPay?:number;
    rmbPay?:number;
    isFirstPay?:number
    channel?:string;
}

export class BaseRole {
    token = '';
    isLoad = false;
    data:any = {};
    dirtyFields = {};
    isDirty = false;
    redisPrefix:string = '';
    redisKeyExpire:number = 3600;
    //recvAliveTime:number = 0;

    constructor(uid:number, token:string) {
        this.token = token;

        this.redisPrefix = 'role';
        //this.recvAliveTime = Time.realNow();

        this.data.uid = uid;
        this.data.nickname = '';
        this.data.headimgurl = '';

        this.data.lastLoginTime = Time.realNow();
        this.data.lastAliveTime = Time.realNow();
        this.data.createTime = 0;

        this.data.gold = 0;
        this.data.yxb = 0;
        this.data.totalYxb = 0;
        this.data.totalRmbPay = 0;
        this.data.rmbPay = 0;
        this.data.isFirstPay = 0;

        this.data.lastShareTime = 0;
        this.data.dayShare = 0;
        this.data.shareAmount = 0;
        this.data.isAttention = 0;
        this.data.isAttentionAward = 0;

        this.data.channel = '';
    }

    //public copyDataToBase(data:any){
    //    for(var key in data){
    //        this.data[key] = data[key];
    //    }
    //}

    public initData() {
        this.isLoad = true;
    }

    public get(name:string) {
        this.checkPlayerIsLoadAndThrowError();
        return this.data[name];
    }

    public set(name:string, val:any) {
        this.checkPlayerIsLoadAndThrowError();
        this.data[name] = val;
        this.dirtyFields[name] = val;
        this.isDirty = true;
    }

    public add(name:string, addVal:any, maxVal?:any) {
        var val = this.get(name);
        if (typeof val !== 'number') {
            log.sError('data %s is not number', name);
            return;
        }
        if (typeof maxVal === 'number'){
            val = maxVal - addVal >= val ? val + addVal : maxVal;
        }
        else{
            val += addVal;
        }
        this.set(name, val);
    }

    public save(callback:(err)=>void, onlyCache:boolean = false, bSaveAll:boolean = false):void {
        if (this.isDirty || bSaveAll) {
            this.checkPlayerIsLoadAndThrowError();
            var pckData = this.serialize();
            var checkFields = bSaveAll ? this.data : this.dirtyFields;
            var saveData:{[key: string]:any} = {};
            for (var obj in checkFields) {
                if (checkFields.hasOwnProperty(obj))
                    saveData[obj] = pckData[obj];
            }

            // 异步存储到redis
            roleRedis.hmset(this.getRedisKey(), saveData, this.redisKeyExpire, (err) => void {
                if(err) {
                    log.sError('redis update err=', err.stack);
                }
            });

            // 异步存储到db
            if (!onlyCache) {
                WorldDB.conn.execute('update player_info_' + this.getTableNum() + ' set ? where ?', [saveData, {uid: this.data.uid}], ()=> {
                })
            }

            this.isDirty = false;
            this.dirtyFields = {};

            callback(null);
        }
        else {
            return callback(null);
        }
    }

    public load(callback:(err, isExist:boolean)=>void):void {
        roleRedis.hmget(this.getRedisKey(), this.getDataFields(), (err, reply)=> {
            // 查询出错
            if (err || reply === null) {
                return callback(err, false);
            }
            // 缓存不存在，从db查询然后放到缓存
            else if (Object.keys(reply).length === 0) {
                WorldDB.conn.execute('select * from player_info_' + this.getTableNum() + ' where ?', {uid: this.data.uid}, (err, connection, result)=> {
                    if (err) {
                        return callback(err, false);
                    }

                    if (!result || result.length === 0) {
                        return callback(null, false);
                    }

                    roleRedis.hmset(this.getRedisKey(), result[0], this.redisKeyExpire, ()=> {
                    });
                    this.deserialize(result[0]);
                    return callback(null, true);
                })
            }
            // 缓存命中
            else {
                this.deserialize(reply);
                return callback(null, true);
            }
        });
    }

    public create(callback:(err:any)=>void):void {
        this.initData();
        this.data.createTime = Time.realNow();

        this.updateNick((err)=>{
            if(err){
                return callback(err);
            }
            var pckData = this.serialize();
            WorldDB.conn.execute('insert into player_info_' + this.getTableNum() + ' set ?', pckData, callback);
        });
    }

    public updateNick(callback:(err:any)=>void):void{
        if(this.token && this.token.length > 0){
            var accountTokenRedis = RedisMgr.getInstance('accountTokenRedis');
            accountTokenRedis.hmget(this.token, ['uid', 'headimgurl', 'nickname'], (error, reply) => {
                if (error) {
                    return callback(error);
                }
                else {
                    if(reply['headimgurl'] === null || typeof reply['headimgurl'] === 'undefined'){
                        this.data.headimgurl = '';
                    }
                    else{
                        this.data.headimgurl = reply['headimgurl'];
                    }

                    if(reply['nickname'] === null || typeof reply['nickname'] === 'undefined'){
                        this.data.nickname = '';
                    }
                    else{
                        this.data.nickname = reply['nickname'];
                    }

                    return callback(null);
                }
            });
        }
    }

    private checkPlayerIsLoadAndThrowError() {
        if (!this.isLoad) {
            throw new Error('player data is not loaded');
        }
    }

    private getRedisKey() {
        return this.redisPrefix + ':' + this.data.uid;
    }

    private getDataFields():string[] {
        var ret = [];
        for (var obj in this.data) {
            if (this.data.hasOwnProperty(obj)) {
                ret.push(obj.toString());
            }
        }
        return ret;
    }

    private getTableNum():number {
        return GameUtil.getTableNum(this.data.uid);
    }

    private deserialize(reply:{[key:string]:any}):void {
        //for (var obj in reply) {
        //    if (this.data.hasOwnProperty(obj)) {
        //        switch (typeof this.data) {
        //            case 'number' :
        //                this.data[obj] = parseInt(reply[obj]);
        //                break;
        //            case 'object' :
        //            case 'array' :
        //                this.data[obj] = JSON.parse(reply[obj]);
        //                break;
        //            case 'string' :
        //                this.data[obj] = reply[obj];
        //                break;
        //        }
        //    }
        //}
        this.data = GameUtil.deserialize(this.data, reply);
        this.isLoad = true;
    }

    private serialize():{[key:string]:any} {
        //var reply:{[key: string]:any} = {};
        //for (var obj in this.data) {
        //    if (this.data.hasOwnProperty(obj)) {
        //        switch (typeof this.data) {
        //            case 'number' :
        //            case 'string' :
        //                reply[obj] = this.data[obj];
        //                break;
        //            case 'object' :
        //            case 'array' :
        //                reply[obj] = JSON.stringify(this.data[obj]);
        //                break;
        //        }
        //    }
        //}
        //
        //return reply;
        return GameUtil.serialize(this.data);
    }

    // 获取data
    public getCopyData():any {
        return GameUtil.copyObject(this.data);
    }
}

//export = BaseRole;