import log = require('../util/log');
import Role = require('./role');
import Dispatcher = require('../http/dispatcher');
import ErrorCode = require('../util/error_code');

//class BaseController {
export function loadRole(uid, token, cb:(err, role:Role)=>void):void {
    var role = new Role(uid, token);
    role.load((err, isExist)=>{
        if(err || !isExist) {
            cb(ErrorCode.COMMON.ROLE_NOT_EXIST, null)
        }
        else {
            cb(ErrorCode.NO_ERROR, role);
        }
    });
}
//}

//export = BaseController;