import log = require('../util/log');
import DynObject = require('../util/dynamicobject/dyn_object');
import Time = require('../util/time');
import RedisMgr = require('../redis/redis_mgr');
import WorldDB = require('../database/impl/world_db');
import RoleBase = require('./role_base');
import ErrorCode = require('../util/error_code');
import GameData = require('./game_data');
import GameConst = require('./game_const');
import GameUtil = require('../util/game_util');
import MoltenApi = require('../molten/molten_api');

var roleRedis = RedisMgr.getInstance('roleRedis');

var configMgr = require('../config').configMgr;

interface weaponDBData{
    lv:number;
}

interface skillDBData{
    lv:number;
    lastTime:number;
}

interface soldierDBData{
    lv:number;
}

interface RoleRedisData extends RoleBase.BaseRoleRedisData {
    heroLv?:number;
    achiAwardBit?:string;
    killEnemy?:number;
    killBox?:number;
    soldierDie?:number;
    heroBorn?:number;
    weaponUp?:number;
    useSkill?:number;
    callPeri?:number;
    buyYxb?:number;

    lastItemDropTime?:number;
    dayDrop?:number;
    drawEffectCount?:number;
    drawEffectTime?:number;

    cdkeyAward?:string;
    awardMonthItemTime?:number;
    buyMonthItemTime?:number;

    highestfloorclear?:number;

    maxFloor?:number;
    sealFloor?:number;

    cheatNum?:number;
    lastCheatTime?:number;
    floorCheatNum?:number;
    floorLastCheatTime?:number;

    skillList?:{[ID:number]: skillDBData};
    soldierList?:{[ID:number] : soldierDBData};
    weaponList?:{[ID:number] : weaponDBData};
}

interface BattleRedisData{
    treasures?:any;
    battleField?:any;

    smHeroNum?:number; // 小勇士的个数
    lgHeroNum?:number; // 大勇士的个数

    //useDemonClickTime?:number;
    cheatNum?:number;
}

class Role extends RoleBase.BaseRole{
    battleData:BattleRedisData = {};
    callPeriTime:number = 0;//
    dropWeaponList = {}; // 记录掉落装备的列表

    constructor(uid:number, token:string) {
        //role
        super(uid, token);

        this.callPeriTime = 0;
        this.dropWeaponList = {};

        this.data.heroLv = 1;
        this.data.achiAwardBit = "";
        this.data.killEnemy = 0;
        this.data.killBox = 0;
        this.data.soldierDie = 0;
        this.data.heroBorn = 0;
        this.data.weaponUp = 0;
        this.data.useSkill = 0;
        this.data.callPeri = 0;
        this.data.buyYxb = 0;

        this.data.lastItemDropTime = 0;
        this.data.dayDrop = 0;
        this.data.drawEffectCount = 0;
        this.data.drawEffectTime = 0;

        this.data.cdkeyAward = '';
        this.data.awardMonthItemTime = 0;
        this.data.buyMonthItemTime = 0;

        this.data.highestfloorclear = 0;

        this.data.maxFloor = 0;
        this.data.sealFloor = 0;

        this.data.cheatNum = 0;
        this.data.lastCheatTime = 0;

        this.data.floorCheatNum = 0;
        this.data.floorLastCheatTime = 0;

        this.data.skillList = {};
        this.data.soldierList = {};
        this.data.weaponList = {};

        var skillLen =  Object.keys(GameConst.SKILL_DATA_MAXLEN).length;
        for(var i = 1; i <= skillLen ; i++)
        {
            this.data.skillList[i] = {lv : 0, lastTime : 0};
        }

        var heroAll =  configMgr.herodb.all();
        for (var sid in heroAll)
        {
            var soldierConfig = heroAll[sid];
            if(soldierConfig.type === 2 || soldierConfig.type === 4)
            {
                this.data.soldierList[sid] = {lv : 0};
            }
        }

        ////////TODO:battle
        this.data.useDemonClickTime = 0;
        this.battleData.cheatNum = 0;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 记录钻石日志
    public addGoldLog(uid, num, transId, reason, callback):void {
        var tableName = WorldDB.DIAMOND_LOG_TABLE_NAME;
        var now:string = (new Date().getTime() / 1000).toString();
        var time = parseInt(now, 10);
        var data = {
            uid : uid,
            transId : transId,
            addGold : num,
            time : time,
            reason : reason
        };


    }

    public addYxb(num):void {
        num = Number(num);
        this.add('yxb', num, Number.MAX_VALUE);
        this.add('totalYxb', num, Number.MAX_VALUE);
        //var data = [CONSTS.RANK_KEY_NAME, this.get('totalGold'), this.get('uid')];

        //// 添加排行
        //this.redisInstance.zadd(data, 0, function (err) {
        //if (err) {
        //logger.error('rank add failed, ' + err);
        //}
        //});
    }

    public reduceYxb(num):boolean {
        num = Number(num);
        var yxb = this.get('yxb');
        if (yxb < num) {
            return false;
        } else {
            yxb -= num;
            this.set('yxb', yxb);
            return true;
        }
    }

    // 添加钻石
    public addGold(num, reason, transId:string, callback):void {
        num = Number(num);
        if (num < 0) {
            //var err = new Error('addDiamond num must > 0');
            //throw err;
            log.sError('addGold num must > 0');
            return;
        }

        var gold = this.get('gold');
        gold += num;

        this.addGoldLog(this.get('uid'), num, transId, reason, function (err) {
            if (typeof callback === 'function') {
                return callback(err);
            }
        });

        this.set('gold', gold);
    }

    // 扣除钻石
    public reduceGold(num, reason):boolean {
        num = Number(num);
        if (num < 0) {
            var err = new Error('reduceGold num must > 0');
            throw err;
        }

        var gold = this.get('gold');
        if (gold < num) {
            return false;
        }
        gold -= num;

        this.addGoldLog(this.get('uid'), -num, '0', reason, function (err) {});
        this.set('gold', gold);

        return true;
    }

    public getProductInfo():any {
        GameData.setRole(this);
        var maxFloor = this.get('maxFloor');
        var heroLv = this.get('heroLv');
        var data = {
            shopYxb : GameData.getShopCoin(maxFloor),
            shopSuperYxb : GameData.getSuperShopCoin(maxFloor),
            demonAttack : GameData.getDemonAttack(maxFloor, heroLv)
        };
        return data;
    }

    public calcOffline():any {
        var now = Time.realNow();
        var lastAliveTime = this.get('lastAliveTime');
        if (lastAliveTime == 0) {
            return undefined;
        }

        var offlineTime = now - lastAliveTime;
        if (offlineTime <= 5 * 60) {
            return undefined;
        }

        if (offlineTime > 8 * 3600) {
            offlineTime = 8 * 3600;
        }

        var maxFloor = this.get('maxFloor');
        var bestFloor = Math.max(maxFloor - 1, 1);//floor 1 is minimum floor
        GameData.setRole(this);
        var offlineYxb:number = GameData.getOfflineCoin(offlineTime, bestFloor);

        this.addYxb(offlineYxb);

        log.sInfo('[OFFLINE] : offlineTime = %d, uid = %d, bestFloor = %d, offlineGold = %d',
            offlineTime, this.data.uid, bestFloor, offlineYxb);

        var data = {
            offlineTime : offlineTime,
            offlineYxb : offlineYxb
        };

        return data;
    }

    public isSkillCooldown(skillId):boolean {
        var skillList = this.get('skillList');
        var skillInfo = skillList[skillId];

        var skillConfig = configMgr.skilldb.get(skillId);
        var now = Time.realNow();

        if (typeof skillConfig === 'undefined') {
            return false;
        }

        if (skillInfo.lastTime /*+ skillConfig.cd*/ >= now) {
            return false;
        } else {
            return true;
        }
    }

    public useSkill(skillId):any {
        var skillList = this.get('skillList');
        var skillInfo = skillList[skillId];

        if (typeof skillInfo === 'undefined') {
            return ErrorCode.GAME.SKILL_NOT_EXIST;
        }

        if (skillInfo.lv == 0) {
            return ErrorCode.GAME.SKILL_EMPTY_LEVEL;
        }

        var skillConfig = this.getSkillInfoByLv(skillId, skillInfo.lv);
        if (typeof skillConfig === 'undefined') {
            return ErrorCode.GAME.SKILL_NOT_EXIST;
        }

        if (typeof skillConfig.time === 'undefined'
            || typeof skillConfig.cd === 'undefined') {
            return ErrorCode.GAME.CANNOT_USE_SKILL;
        }

        var now = Time.realNow();
        if (skillInfo.lastTime /*+ skillConfig.cd*/ >= now) {
            return ErrorCode.GAME.SKILL_NOT_COOL_DOWN;
        }

        skillInfo.lastTime = now + skillConfig.time + skillConfig.cd;

        this.set('skillList', skillList);
        this.add('useSkill', 1);

        return ErrorCode.NO_ERROR;
    }

    //public unlockFloor():any {
    //    var maxFloor = this.get('maxFloor');
    //    if(maxFloor % 10 != 0)
    //    {
    //        return ErrorCode.GAME.JUMP_CONDITION_NOT_FIT;
    //    }
    //
    //    var unlockYxb = this.getUnlockYxb(maxFloor);
    //    if(typeof unlockYxb === 'ErrorCode'){
    //        return unlockYxb;
    //    }
    //
    //    if (this.reduceYxb(unlockYxb) === false) {
    //        return ErrorCode.GAME.NOT_ENOUGH_YXB_SHOW;
    //    }
    //
    //    var nextUnlockYxb = this.getUnlockYxb(maxFloor + 10);
    //    if(typeof nextUnlockYxb === 'ErrorCode')
    //    {
    //        log.sWarn('%d had come through all floor %d', this.data.uid, maxFloor + 10);
    //    }
    //
    //    this.add('maxFloor', 1);
    //    this.set('highestfloorclear', 0);
    //    this.set('nextFloorYxb', nextUnlockYxb);
    //
    //    return ErrorCode.NO_ERROR;
    //}

    public getUnlockYxb(floor:number):number{
        var curFloor = Math.floor(floor);
        //var sealFloor = Math.floor(curFloor / 10) * 10;
        //var maxFloor = sealFloor + 10;
        var ID = curFloor / 10;

        var nextStoreyConf = configMgr.next_storeydb.get(ID);
        if(typeof nextStoreyConf !== 'undefined')
        {
            return nextStoreyConf.gold;
        }

        return 0;
    }

    public getSoldierInfoByLv(sid, lv):any {
        GameData.setRole(this);
        var skillList = this.get('skillList');
        var soldierInfo = GameData.getSoldierData(sid, lv);
        var soldierList = this.get('soldierList');
        //var soldier = soldierList[sid];

        var data = {
            maxhp : soldierInfo.hp,
            attack : soldierInfo.atk,
            speed : soldierInfo.speed,
            coin : soldierInfo.nextCoin,
            lv : lv,
            time : soldierInfo.time,
            pos : soldierInfo.pos
        };

        return data;
    }

    public getSoldierList():any {
        var soldierList = this.get('soldierList');

        //为了新增英雄的特殊处理,否则读取到该玩家的数据,会覆盖掉构造函数里的英雄数据
        var heroAll =  configMgr.herodb.all();
        for (var sid in heroAll)
        {
            var soldierConfig = heroAll[sid];
            if(soldierConfig.type === 4)
            {
                if(typeof soldierList[sid] === 'undefined'){
                    soldierList[sid] = {lv : 0};
                }
            }
        }

        var data = {};
        for (var sid in soldierList) {
            var soldier = soldierList[sid];

            data[sid] = [];
            data[sid].push(this.getSoldierInfoByLv(sid, soldier.lv));

            if(soldier.lv < GameConst.heroMaxLv)
            {
                data[sid].push(this.getSoldierInfoByLv(sid, soldier.lv + 1));
            }

            if (soldier.lv == 0) {
                break;
            }
        }
        this.set('soldierList', soldierList);
        return data;
    }

    public getOwnSoldierLength():number {
        var count = 0;
        var soldierList = this.get('soldierList');
        for (var sid in soldierList) {
            var soldier = soldierList[sid];
            if (soldier.lv == 0) {
                break;
            }
            count ++;
        }
        return count;
    }

    public equalSoldierList(oldSoldierList, newSoldierList):boolean {
        if (Object.keys(oldSoldierList).length !== Object.keys(newSoldierList).length) {
            return false;
        }

        for (var sid in oldSoldierList) {
            var oldList = oldSoldierList[sid];
            var newList = newSoldierList[sid];
            if (typeof newList === 'undefined') {
                return false;
            }

            if (oldList.length !== newList.length) {
                return false;
            }

            for (var i = 0; i < oldList.length; i++) {
                var oldInfo = oldList[i];
                var newInfo = newList[i];

                if (oldInfo.maxhp != newInfo.maxhp
                    || oldInfo.attack != newInfo.attack
                    || oldInfo.speed != newInfo.speed
                    || oldInfo.coin != newInfo.coin
                    || oldInfo.lv != newInfo.lv
                    || oldInfo.addTime != newInfo.addTime) {
                    return false;
                }
            }
        }
        return true;
    }

    public getSkillInfoByLv(sid, lv):any {
        GameData.setRole(this);
        sid = Number(sid);

        var skillList = this.get('skillList');
        var skillInfo = GameData.getSkillData(sid, lv);

        //var skillConfig = configMgr.skilldb.get(sid);
        var data:any = {};

        data.lv  = skillInfo.lv;
        data.yxb = skillInfo.yxb;
        if (skillInfo.maxLv) {
            data.maxLv = skillInfo.maxLv;
        }
        if (skillInfo.needFloor) {
            data.needFloor = skillInfo.needFloor;
        }
        if (skillInfo.needLv) {
            data.needLv = skillInfo.needLv;
        }
        //if (skillInfo.needSkillID) {
        //    data.needSkillID = skillInfo.needSkillID;
        //}
        if (skillInfo.count_1 != -1) {
            data.count_1 = skillInfo.count_1;
        }
        if (skillInfo.count_2 != -1) {
            data.count_2 = skillInfo.count_2;
        }
        if (skillInfo.time) {
            data.time = skillInfo.time;//持续 时间
        }
        if (skillInfo.cd) {
            data.cd = skillInfo.cd;//cd 时间
        }
        if (typeof skillInfo.time !== 'undefined'
            && typeof skillInfo.cd !== 'undefined') {
            data.lastTime = skillList[sid].lastTime;//下一次可以用的时间
        }
        return data;
    }

    public getSkillInfoById(sid:number) {
        var skillList = this.get('skillList');
        var skill = skillList[sid];
        var data = [];

        var skillData = this.getSkillInfoByLv(sid, skill.lv);
        data.push(skillData);
        if(skill.lv < skillData.maxLv)
        {
            data.push(this.getSkillInfoByLv(sid, skill.lv + 1));
        }

        return data;
    }

    public getSkillList() {
        var skillList = this.get('skillList');
        var data = {};
        GameData.setRole(this);
        for (var sid in skillList) {
            data[sid] = this.getSkillInfoById(sid);
        }
        return data;
    }

    public heroUp(soldierAtkSum):boolean {
        var heroLv = this.get('heroLv');
        GameData.setRole(this);

        var data = GameData.getHeroData(heroLv, soldierAtkSum);
        if (this.reduceYxb(data.nextCoin) === false) {
            return false;
        }
        this.add('heroLv', 1);

        return true;
    }

    public skillUp(skillId):any {
        var skillList = this.get('skillList');
        var skillInfo = skillList[skillId];
        if (typeof skillInfo === 'undefined') {
            return ErrorCode.GAME.SKILL_NOT_EXIST;
        }

        GameData.setRole(this);

        var skillData = GameData.getSkillData(skillId, skillInfo.lv);

        if (skillData.maxlv && skillInfo.lv >= skillData.maxlv) {
            return ErrorCode.GAME.SKILL_REACH_MAX_LEVEL;
        }

        var maxFloor = this.get('maxFloor');
        if (maxFloor < skillData.needFloor) {
            return ErrorCode.GAME.SKILL_CONDITION_NOT_FIT;
        }

        var heroLv = this.get('heroLv');
        if (heroLv < skillData.needLv) {
            return ErrorCode.GAME.SKILL_CONDITION_NOT_FIT;
        }

        if (this.reduceYxb(skillData.yxb) === false) {
            return ErrorCode.GAME.NOT_ENOUGH_YXB;
        }

        skillInfo.lv++;
        this.set('skillList', skillList);

        return ErrorCode.NO_ERROR;
    }

    public soldierUp(sid) {
        var soldierList = this.get('soldierList');
        var soldier = soldierList[sid];
        if (typeof soldier === 'undefined') {
            return ErrorCode.GAME.SOLDIER_NOT_EXIST;
        }
        var skillList = this.get('skillList');
        var weaponList = this.get('weaponList');
        GameData.setRole(this);
        var soldierInfo = GameData.getSoldierData(sid, soldier.lv);
        if(soldierInfo.gold > 0 && soldier.lv === 0){
            var gold = soldierInfo.gold;
            if(GameUtil.inActivity()){
                gold *= 0.8;
            }

            if (this.reduceGold(gold, GameConst.ADD_DIAMOND_REASON.SOLDIER_UP) === false) {
                return ErrorCode.GAME.NOT_ENOUGH_GOLD;
            }
        }
        else{
            if (this.reduceYxb(soldierInfo.nextCoin) === false) {
                return ErrorCode.GAME.NOT_ENOUGH_YXB;
            }
        }

        soldier.lv++;
        this.set('soldierList', soldierList);
        return ErrorCode.NO_ERROR;
    }

    public getHeroInfoByLv(lv:number):any {
        GameData.setRole(this);
        var soldierAtkSum = GameData.getSoldierAttackSum();
        var data = GameData.getHeroData(lv, soldierAtkSum);
        var heroInfo = {
            lv : data.lv,
            maxhp : data.hp,
            attack : data.atk,
            speed : data.speed,
            coin : data.nextCoin
        };

        return heroInfo;
    }

    public getHeroInfo():any {
        var heroLv = this.get('heroLv');
        //var heroMaxLen = Object.keys(configMgr.hero_basicdb.all()).length;
        var heroInfo = [];
        heroInfo.push(this.getHeroInfoByLv(heroLv));
        if(heroLv + 1 <= GameConst.heroMaxLv)
        {
            heroInfo.push(this.getHeroInfoByLv(heroLv + 1));
        }

        return heroInfo;
    }

    public equalHeroInfo(oldHeroInfo, newHeroInfo):boolean {
        if (oldHeroInfo.length !== newHeroInfo.length) {
            return false;
        }

        for (var i = 0; i < oldHeroInfo.length; i++) {
            var oldInfo = oldHeroInfo[i];
            var newInfo = newHeroInfo[i];
            if (oldInfo.lv != newInfo.lv
                || oldInfo.maxhp != newInfo.maxhp
                || oldInfo.attack != newInfo.attack
                || oldInfo.speed != newInfo.speed
                || oldInfo.coin != newInfo.coin) {
                return false;
            }
        }

        return true;
    }

    public dropWeapon(weaponId):any {
        var weaponList = this.get('weaponList');
        var weaponConfig = configMgr.itemdb.get(weaponId);
        if (typeof weaponConfig === 'undefined') {
            return;
        }

        if (typeof weaponList[weaponId] === 'undefined') {
            weaponList[weaponId] = 1;
        } else {
            if ((weaponConfig.max_lv && weaponList[weaponId] >= weaponConfig.max_lv)) {
                //var randItem:any = [4,5,6,7,12,13];
                var randNum = GameUtil.randInt(0, GameConst.randItem.length);
                weaponId = GameConst.randItem[randNum];
                return this.dropWeapon(weaponId);
            }
            weaponList[weaponId]++;
            this.add('weaponUp', 1);
        }
        this.dropWeaponList[weaponId] = weaponList[weaponId];
        this.set('weaponList', weaponList);
        return weaponId;
    }

    // 抽取一个装备
    public drawWeapon(isBattle:boolean):any {
        var randNum = GameUtil.randInt(1, 100);
        //var prob = 0;
        var all = configMgr.item_randomdb.all();

        for (var weaponId in all) {
            var itemRand = configMgr.item_randomdb.get(weaponId);

            if (randNum < itemRand.random) {
                if(isBattle && weaponId >= 4){
                    //var randItem:any = [4,5,6,7,12,13];
                    var randNum = GameUtil.randInt(0, GameConst.randItem.length);
                    var itemId = GameConst.randItem[randNum];
                    this.dropWeapon(itemId);
                    return itemId;
                }
                var itemArray = GameUtil.stringToArray(itemRand.item_id);
                var itemLen = itemArray.length;
                var randId = GameUtil.randInt(0, itemLen);
                var itemId = itemArray[randId];
                this.dropWeapon(itemId);
                return itemId;
            }
        }
        return undefined;
    }

    public getWeaponInfoById(weaponId):any {
        GameData.setRole(this);
        var weaponList = this.get('weaponList');
        var data = {
            lv : weaponList[weaponId],
            param : GameData.getWeaponPara(weaponId)
        };
        return data;
    }

    public getWeaponList():any {
        var weaponList = this.get('weaponList');
        var data = {};
        for (var weaponId in weaponList) {
            data[weaponId] = this.getWeaponInfoById(weaponId);
        }
        return data;
    }

// 获取装备掉落的同步信息
    public genDropWeaponSyncData():any {

        if (Object.keys(this.dropWeaponList).length === 0) {
            return {};
        }

        var weaponList = this.get('weaponList');
        var syncData:any = {};
        syncData.weaponList = {};

        var upList = {};
        for (var weaponId in this.dropWeaponList) {
            //var weaponConfig = WEAPON_JSON[weaponId];
            weaponId = Number(weaponId);
            var weaponConfig = configMgr.itemdb.get(weaponId);
            if (typeof weaponConfig === 'undefined') {
                continue;
            }

            var weaponData = this.getWeaponInfoById(weaponId);
            syncData.weaponList[weaponId] = weaponData;
            syncData.weaponList[weaponId].weaponId = weaponId;

            if (weaponConfig.up_hero) {
                upList['upHero'] = true;
            }

            if (weaponConfig.upSoldier) {
                upList['upSoldier'] = true;
                upList['upHero'] = true;
            }

            if (weaponConfig.up_skill) {
                if (!upList['upSkill']) {
                    upList['upSkill'] = {};
                }
                upList['upSkill'][weaponConfig.up_skill] = true;
            }

            if (weaponConfig.upMonster) {
                upList['upMonster'] = true;
            }
        }

        if (upList['upHero']) {
            syncData.heroInfo = this.getHeroInfo();
        }

        if (upList['upSoldier']) {
            syncData.soldierList = this.getSoldierList();
        }

        if (upList['upSkill']) {
            syncData.skillList = {};
            for (var skillId in upList['upSkill']) {
                syncData.skillList[skillId] = this.getSkillInfoById(skillId);
            }
        }

        if (upList['upMonster']) {
            //var battle = new Battle(this);
            //battle.genBattleField();
            //syncData.battleField = battle.getClientBattleField();
        }

        return syncData;
    }


// 同步最大层数排行
    public syncMaxFloorRank():void {
        var maxFloor = this.get('maxFloor');
        if (maxFloor < 5) {
            return ;
        }

        var timeDiff = ((new Date('2020-01-01 00:00:00').getTime()) - (new Date().getTime())) / 1000;
        var score = Math.floor(maxFloor * Math.pow(10, 10) + timeDiff);
        var data = [GameConst.RANK_KEY_NAME, score, this.get('uid')];

        // 添加排行
        roleRedis.zadd(data, 0, (err) =>{
            if (err) {
                log.sError('rank add failed, ' + err.stack);
            }
        });
    }

    // 领取成就
    public getAchPrize(achId):any{
        var achBitset = this.get('achiAwardBit');
        if (GameUtil.testBitset(achBitset, achId)) {
            return ErrorCode.GAME.ALREADY_GET_ACH;
        }

        var achConfig = configMgr.achievementdb.get(achId);
        if (typeof achConfig === 'undefined') {
            return ErrorCode.GAME.ACH_NOT_EXIST;
        }

        var value = achConfig.count;
        var type = achConfig.type;
        var reach = 0;

        if (type == 1) {
            reach = this.get('maxFloor');
        } else if (type == 2) {
            reach = this.get('killEnemy');
        } else if (type == 3) {
            reach = this.get('soldierDie');
        } else if (type == 4) {
            reach = this.get('heroBorn');
        } else if (type == 5) {
            reach = this.get('buyYxb');
        } else if (type == 6) {
            var soldierList = this.get('soldierList');
            for (var sid in soldierList) {
                if (soldierList[sid].lv > 0) {
                    reach++;
                }
            }
        } else if (type == 7) {
            reach = this.get('useSkill');
        } else if (type == 8) {
            reach = this.get('callPeri');
        } else if (type == 9) {
            reach = this.get('killBox');
        } else if (type == 10) {
            var weaponList = this.get('weaponList');
            reach = Object.keys(weaponList).length;
        } else if (type == 11) {
            reach = this.get('weaponUp');
        } else {
            return ErrorCode.GAME.ACH_NOT_EXIST;
        }

        if (achId != 1 && reach < value) {
            log.sError('[ACH NOT REACH] : user %d ach %d reach %d value %d', this.get('uid'), achId, reach, value);
            return ErrorCode.GAME.ACH_NOT_REACH;
        }

        achBitset = GameUtil.setBitset(achBitset, achId);
        this.set('achiAwardBit', achBitset);

        if (achConfig.currency_type == 1
            && !isNaN(achConfig.currency_num)) {
            this.addYxb(achConfig.currency_num);
        }

        if (achConfig.currency_type == 2
            && !isNaN(achConfig.currency_num)) {
            this.addGold(achConfig.currency_num,
                GameConst.ADD_DIAMOND_REASON.ACH, '0', function () {});
        }

        return ErrorCode.NO_ERROR;
    }

    // 钻石抽取宝箱
    public diamondDrawWeapon(fixWeapon:boolean) {
        var itemRandAll = configMgr.item_randomdb.all();

        var drawEffectCount = this.get('drawEffectCount');
        if(drawEffectCount > GameConst.BATTLE.MAX_DRAWWEAPON_EFFCOUNT)
            drawEffectCount = GameConst.BATTLE.MAX_DRAWWEAPON_EFFCOUNT;

        if(fixWeapon){
            this.set('drawEffectTime', Time.realNow() + 1800);
            this.add('drawEffectCount', 1, GameConst.BATTLE.MAX_DRAWWEAPON_EFFCOUNT);

            var randNum = GameUtil.randInt(0, 3);
            //var dropWeaponArr = [1, 2, 3];
            log.sInfo('player %d draw weapon first drop %d',
                this.get('uid'), GameConst.times12DrawWeapon[randNum]);
            return this.dropWeapon(GameConst.times12DrawWeapon[randNum]);
        }
        else{
            //var dropItemRand = [0, 0, 0, 0];

            GameConst.drawItemRand[0] = itemRandAll[1].random - drawEffectCount;
            GameConst.drawItemRand[1] = itemRandAll[2].random - drawEffectCount;
            GameConst.drawItemRand[2] = itemRandAll[4].random - Math.floor(drawEffectCount / 5);
            GameConst.drawItemRand[3] = itemRandAll[4].random;

            //log.sInfo('player %d dropItemRand[0] = %d dropItemRand[1] = %d dropItemRand[2] = %d dropItemRand[3] = %d',
            //    this.get('uid'), dropItemRand[0], dropItemRand[1], dropItemRand[2], dropItemRand[3]);

            this.set('drawEffectTime', Time.realNow() + 1800);
            this.add('drawEffectCount', 1, GameConst.BATTLE.MAX_DRAWWEAPON_EFFCOUNT);

            var randNum = GameUtil.randInt(1, 101);

            for(var i = 0; i < 4 ; i ++){
                if(randNum <= GameConst.drawItemRand[i]){
                    var itemRand = itemRandAll[i + 1];
                    var itemArray = GameUtil.stringToArray(itemRand.item_id);

                    var itemIndex = GameUtil.randInt(0, itemArray.length);
                    var weaponId = itemArray[itemIndex];

                    log.sInfo('player %d draw weapon %d randNum %d randId %d',
                        this.get('uid'), weaponId, randNum, i + 1);
                    return this.dropWeapon(weaponId);
                }
            }
        }

        log.sError('player %d draw nothing', this.get('uid'));
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO: Battle
    public getBattleFiled(floor:number):any{
        //var maxFloor = this.get('maxFloor');
        //var sealFloor = floor % 10 == 0 ? Math.floor(floor / 10) : Math.ceil(floor / 10);
        //sealFloor *= 10;
        var data:{[ID:number]: any}={};

        var coinUp = 1 + GameData.getWeaponPara(11) * 0.01;
        var hpDown = 1 - GameData.getWeaponPara(34) * 0.01;
        var atkDown = 1 - GameData.getWeaponPara(35) * 0.01;

        //var index = 1;
        var beginId = floor === 0 ? 1 : floor;
        for(var i = beginId; i < beginId + 10; i ++) {
            var battleFieldConf = configMgr.monster_lvdb.get(i);
            data[battleFieldConf.lv] = {};
            data[battleFieldConf.lv].monsterID = battleFieldConf.monsterID;
            data[battleFieldConf.lv].attack = Math.ceil(battleFieldConf.att * atkDown);
            data[battleFieldConf.lv].hp = Math.ceil(battleFieldConf.hp * hpDown);
            data[battleFieldConf.lv].yxb = Math.ceil(battleFieldConf.gold * coinUp);

            data[battleFieldConf.lv].bossID = battleFieldConf.bossID;
            data[battleFieldConf.lv].bossAttack = Math.ceil(battleFieldConf.bossatt * atkDown);
            data[battleFieldConf.lv].bossHp = Math.ceil(battleFieldConf.bosshp * hpDown);
            data[battleFieldConf.lv].bossYxb = Math.ceil(battleFieldConf.bossgold * coinUp);

            data[battleFieldConf.lv].mapID = battleFieldConf.mapID;
            data[battleFieldConf.lv].formation = battleFieldConf.formation;
            var bossSkill = GameData.getBossSkill(battleFieldConf.skill_list);
            var skillLen = Object.keys(bossSkill).length;
            if(skillLen > 0){
                data[battleFieldConf.lv].bossSkill = bossSkill;
            }
        }

        return data;
    }

    //// 获取技能数据
    //public getSkillData(skillId):any {
    //    var skillList = this.get('skillList');
    //    var skillInfo = skillList[skillId];
    //    var skillData = GameData.getSkillData(skillId, skillInfo.lv);
    //    return skillData;
    //}
    //
    //public useSkill(skillId): any {
    //    var skillData = this.getSkillData(skillId);
    //    if (typeof skillData === 'undefined'
    //        && typeof skillData.duration === 'undefined') {
    //        return ErrorCode.GAME.SKILL_NOT_EXIST;
    //    }
    //
    //    // 容错3次
    //    var times = Math.floor(skillData.duration / GameConst.BATTLE.BATTLE_ALIVE_INTERVAL) + 50;
    //    this.battleData.skillLeftTimes[skillId] = times;
    //
    //    log.sInfo('[USE SKILL] : user %d skillId %d', this.get('uid'), skillId);
    //    return ErrorCode.NO_ERROR;
    //}
    //
    //// 扣除技能的生效次数
    //public reduceSkillLeftTimes(): void {
    //    for (var skillId in this.battleData.skillLeftTimes) {
    //        this.battleData.skillLeftTimes[skillId]--;
    //        log.sInfo('[REDUCE SKILL LEFT TIMES] : user %d skillId %d left %d', this.get('uid'), skillId, this.battleData.skillLeftTimes[skillId]);
    //        if (this.battleData.skillLeftTimes[skillId] <= 0) {
    //            delete this.battleData.skillLeftTimes[skillId];
    //            log.sInfo('[SKILL DURATION OVER] : user %d skillId %d', this.get('uid'), skillId);
    //        }
    //    }
    //}
    //
    //// 技能是否在生效中
    //public isSkillInEffect(skillId): boolean {
    //    var leftTimes = Number(this.battleData.skillLeftTimes[skillId]);
    //    if (!isNaN(leftTimes) && leftTimes > 0) {
    //        return true;
    //    } else {
    //        return false;
    //    }
    //}

    public demonClick():boolean {
        var now = Time.realNow();
        var useDemonClickTime = this.data.useDemonClickTime;

        // 60秒冷却时间
        if (useDemonClickTime + 300 < now) {
            //var errstr = util.format('[INVALID] : demon click is not cool down, '
            //    + 'useTime = %d', useDemonClickTime);
            //this.setIsValid(false, errstr);
            log.sInfo('[INVALID] : demon click is not cool down, '
                + 'useTime = %d', useDemonClickTime);
            return false;
        }

        return true;
    }

    public useDemonClick() {
        var now = Time.realNow();
        this.data.useDemonClickTime = now;
    }

    public isDemonClickCoolDown() {
        var now = Time.realNow();
        var useDemonClickTime = this.data.useDemonClickTime;
        // 60秒冷却时间
        if (useDemonClickTime + 55 < now) {
            return true;
        } else {
            return false;
        }
    }

    public battleDropWeapon(firstTimes):void {
        var floor = Math.max(this.get('maxFloor'), 1);

        var bestFloor = floor % 10 == 0 ? floor : Math.floor(floor / 10) * 10 + 10;
        var prob = 2 + Math.min(Math.max(5 - (bestFloor - floor), 0), 5);
        if (firstTimes) {
            prob += 3;
        }
        if (prob < 0) {
            prob = 0;
        }

        log.sInfo('[WEAPON DROP PROB] : uid %d prob %d floor %d bestFloor %d',
            this.data.uid, prob, floor, bestFloor);

        var randNum = GameUtil.randInt(0, 101);
        var dayDrop = this.get('dayDrop');
        var dropIntervals = [0, 1800, 3600, 3600];
        var interval = dropIntervals[dayDrop];
        var now = Time.realNow();
        var lastItemDropTime = this.get('lastItemDropTime');
        if (floor > GameConst.BATTLE.MIN_DAYDROP_FLOOR && dayDrop < GameConst.BATTLE.MAX_DAYDROP_NUM && lastItemDropTime + interval < now) {
            if (prob > 0 && randNum < prob) {
                var weaponId = this.drawWeapon(true);

                log.sInfo('[DROP WEAPON] : user %d weapon %d', this.data.uid, weaponId);

                dayDrop++;
                this.set('dayDrop', dayDrop);
                this.set('lastItemDropTime', now);
            }
        }

        //// 掉落赠送的装备
        //var giftDrop = this.get('giftDrop');
        //if (giftDrop > 0 && floor >= 30) {
        //    var randNum = utils.rangedRand(0, 999);
        //    if (prob > 0 && randNum < prob) {
        //        var weaponId = this.drawWeapon();
        //        log.sInfo('[EXTRA DROP WEAPON] : user %d weapon %d giftDrop %d',
        //            this.get('uid'), weaponId, giftDrop);
        //        giftDrop--;
        //        this.set('giftDrop', giftDrop);
        //    }
        //}
        //return 0;
    }

    // 是否充值过
    public isCharged(transId, callback):void {
        var tableName = WorldDB.DIAMOND_LOG_TABLE_NAME;
        WorldDB.conn.execute('select * from ' + tableName + ' where ?', {transId: transId}, (err, connection, result)=> {
            if (err) {
                return callback(err);
            } else if (result.length === 0) {
                return callback(null, false);
            } else {
                return callback(null, true);
            }
        })
    }

    // 充值
    public charge(transId, num, reason, callback):void {
        var self = this;
        this.isCharged(transId, function (err, isCharged) {
            if (err) {
                return callback(err);
            }
            if (isCharged) {
                return callback(null);
            }
            self.addGold(num, reason, transId, function (err) {
                if (err) {
                    return callback(err);
                } else {
                    var rmbPay = self.get('rmbPay');
                    rmbPay += num;
                    self.set('rmbPay', rmbPay);

                    var totalPay = self.get('totalRmbPay');
                    totalPay += num;
                    self.set('totalRmbPay', totalPay);
                    return callback(null);
                }
            });
        });
    }

    public canAwardMonthGift():boolean{
        if(!this.isMonthGiftIn()) return false;

        var now = Time.realNow();
        var awardMonthItemTime = this.get('awardMonthItemTime');
        var nowDate = new Date(now * 1000);
        var awardDate = new Date(awardMonthItemTime * 1000);
        if(GameUtil.isSameDay(awardDate, nowDate)){
            log.sInfo('canAwardMonthGift is samedate %s %s',nowDate, awardDate);
            return false;
        }

        return true;
    }

    public awardMonthGift(callback:(err, data)=>void):void{
        if(this.canAwardMonthGift()){
            var buyMonthItemTime = this.get('buyMonthItemTime');
            var awardMonthItemTime = this.get('awardMonthItemTime');

            var now = Time.realNow();
            var nowDate = new Date(now * 1000);
            var buyDate = new Date(buyMonthItemTime * 1000);

            var data:any = {};
            if(GameUtil.isSameDay(nowDate, buyDate)){
                log.sInfo('[DROP MONTH WEAPON] : uid:%d first month award now = %d buyTime = %d weapon:%d', this.data.uid, now, buyMonthItemTime, 24);
                this.dropWeapon(24);//first pay song zhiyanzhandao
            }

            var monsterConf = configMgr.monster_lvdb.get(this.get('maxFloor'));
            var awardFirstMonthCoin = monsterConf.gold * 200;
            this.addYxb(awardFirstMonthCoin);
            data.addYxb = awardFirstMonthCoin;
            data.yxb = this.data.yxb;

            var weaponId = this.drawWeapon(false);

            log.sInfo('[DROP MONTH WEAPON] : uid:%d every month award weapon:%d', this.data.uid, weaponId);

            var syncWeaponData = this.genDropWeaponSyncData();
            for (var key in syncWeaponData) {
                data[key] = syncWeaponData[key];
            }

            this.set('awardMonthItemTime', now);
            data.awardMonthItemTime = now;

            return callback(ErrorCode.NO_ERROR, data);
        }
        return callback(ErrorCode.GAME.ALREADY_AWARD_MONTH_CARD, null);
    }

    public isMonthGiftIn():boolean {
        var buyMonthItemTime = this.get('buyMonthItemTime');

        if(buyMonthItemTime == 0){
            log.sInfo('canAwardMonthGift buyMonthItemTime == 0');
            return false;
        }

        var now = Time.realNow();
        var days = GameUtil.countDays(GameUtil.getLocalTime(buyMonthItemTime), GameUtil.getLocalTime(now));
        if(days >= 30 || days < 0){
            log.sInfo('canAwardMonthGift days >= 30 || days < 0 %d %d %d',now, buyMonthItemTime, days);
            return false;
        }

        return true;
    }

    public awardCDKeyGift(args, callback:(err, data)=>void):void{
        var self = this;
        MoltenApi.cdkeyAward(args, function (err, type) {
            if (err) {
                log.sError('awardCDKeyGift failed, err = ' + err
                    + ', args cdkey = ' + args.cdkeyAward);
                return callback(err, null);
            }

            var cdkeyAward = self.get('cdkeyAward');
            if (GameUtil.testBitset(cdkeyAward, type)) {
                return callback(ErrorCode.GAME.ALREADY_AWARD_CDKEY, null);
            }

            var data:any = {};
            var weaponId = 5;
            if(type === 2){
                weaponId = 24;
            }

            self.dropWeapon(weaponId);
            log.sInfo('[DROP CDKEY WEAPON] : uid:%d cdkey award weapon:%d', self.data.uid, weaponId);

            var syncWeaponData = self.genDropWeaponSyncData();
            for (var key in syncWeaponData) {
                data[key] = syncWeaponData[key];
            }

            cdkeyAward = GameUtil.setBitset(cdkeyAward, type);
            self.set('cdkeyAward', cdkeyAward);

            data.cdkeyType = type;
            data.cdkeyAward = cdkeyAward;

            return callback(ErrorCode.NO_ERROR, data);
        });
    }
}

export = Role;